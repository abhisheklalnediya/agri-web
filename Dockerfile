
FROM nginx:stable

COPY ./build /usr/share/nginx/html
WORKDIR /usr/share/nginx/html

COPY nginx/conf.d /etc/nginx/conf.d
COPY nginx/includes /etc/nginx/includes
COPY nginx/ssl     /etc/nginx/ssl
COPY nginx/nginx.conf /etc/nginx/nginx.conf

CMD nginx -g 'daemon off;'
