importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.2.0/workbox-sw.js');
importScripts('https://rawgit.com/abhiagri/workbox-bgsync/master/dist/sw.js');


self.__precacheManifest = [
].concat(self.__precacheManifest || []);

workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});


console.log('New SW initalized, waiting for activation');
console.log(`Caching ${self.__precacheManifest.length} files...`);

self.addEventListener('message', (event) => {
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
  }
});
self.skipWaiting();

const bgSync = bgSyncPlugin('queueName');


/**
 * ROUTES
 */

workbox.routing.registerRoute(
  new RegExp('.*\/explorer\/swagger.json'),
  workbox.strategies.networkFirst(),
);

workbox.routing.registerRoute(
  /\/api\/.*/,
  workbox.strategies.networkFirst({
    plugins: [bgSync],
  }),
  'GET',
);

workbox.routing.registerRoute(
  /\/api\/.*/,
  workbox.strategies.networkOnly({
    plugins: [bgSync],
  }),
  'POST',
);
