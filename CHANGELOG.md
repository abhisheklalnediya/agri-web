<a name="0.2.34"></a>
## <small>0.2.34 (2018-11-02)</small>

* AG-214,AG-215 Add User Form And Add Farm Validation ([076190f](https://bitbucket.org/agribuddy/agri-web/commits/076190f))
* AG-230-integrate-sentry ([82cc375](https://bitbucket.org/agribuddy/agri-web/commits/82cc375))



<a name="0.2.33"></a>
## <small>0.2.33 (2018-11-02)</small>

* bring back otp phone verification ([b7d7954](https://bitbucket.org/agribuddy/agri-web/commits/b7d7954))



<a name="0.2.32"></a>
## <small>0.2.32 (2018-11-01)</small>

* AG-213 FirstName And Last Name Maximum 100 Characters ([4aff917](https://bitbucket.org/agribuddy/agri-web/commits/4aff917))
* farm measurement style ([2e0bd48](https://bitbucket.org/agribuddy/agri-web/commits/2e0bd48))
* package changes ([f142393](https://bitbucket.org/agribuddy/agri-web/commits/f142393))



<a name="0.2.31"></a>
## <small>0.2.31 (2018-10-30)</small>




<a name="0.2.30"></a>
## <small>0.2.30 (2018-10-30)</small>

* show area for farm in acer and hectare ([1a0357f](https://bitbucket.org/agribuddy/agri-web/commits/1a0357f))



<a name="0.2.29"></a>
## <small>0.2.29 (2018-10-30)</small>

* AG-163 Measure farm in hectare ([841fbe6](https://bitbucket.org/agribuddy/agri-web/commits/841fbe6))
* Farms cache sync ([1f80742](https://bitbucket.org/agribuddy/agri-web/commits/1f80742))
* folde name ([a157a1b](https://bitbucket.org/agribuddy/agri-web/commits/a157a1b))
* foldername ([62e9a69](https://bitbucket.org/agribuddy/agri-web/commits/62e9a69))
* skip otp screen in welcome flow ([683a132](https://bitbucket.org/agribuddy/agri-web/commits/683a132))



<a name="0.2.28"></a>
## <small>0.2.28 (2018-10-26)</small>

* AG-202-otp-is-not-working ([ab71da7](https://bitbucket.org/agribuddy/agri-web/commits/ab71da7))
* do not delete myself ([94d5999](https://bitbucket.org/agribuddy/agri-web/commits/94d5999))
* foldername ([db4b692](https://bitbucket.org/agribuddy/agri-web/commits/db4b692))
* Localisation-strings-for-address-details-in-profile ([6983b0f](https://bitbucket.org/agribuddy/agri-web/commits/6983b0f))
* User Delete with conformation ([52455a3](https://bitbucket.org/agribuddy/agri-web/commits/52455a3))



<a name="0.2.27"></a>
## <small>0.2.27 (2018-10-25)</small>




<a name="0.2.26"></a>
## <small>0.2.26 (2018-10-25)</small>

* Map view in farm details page ([d1517ac](https://bitbucket.org/agribuddy/agri-web/commits/d1517ac))



<a name="0.2.25"></a>
## <small>0.2.25 (2018-10-25)</small>

* hide soil health card ([eb06ddd](https://bitbucket.org/agribuddy/agri-web/commits/eb06ddd))



<a name="0.2.24"></a>
## <small>0.2.24 (2018-10-25)</small>

* farm cache issue fix ([3f9f154](https://bitbucket.org/agribuddy/agri-web/commits/3f9f154))



<a name="0.2.23"></a>
## <small>0.2.23 (2018-10-25)</small>

* farm add message issue ([8bce2bd](https://bitbucket.org/agribuddy/agri-web/commits/8bce2bd))



<a name="0.2.22"></a>
## <small>0.2.22 (2018-10-25)</small>

* Adding-snackbar-and-button-for-adding-details-localisationstrings ([7a342bc](https://bitbucket.org/agribuddy/agri-web/commits/7a342bc))
* Localisation-strings-for-cropslist ([8dd9390](https://bitbucket.org/agribuddy/agri-web/commits/8dd9390))
* user list Edit/view label fix ([6004196](https://bitbucket.org/agribuddy/agri-web/commits/6004196))



<a name="0.2.21"></a>
## <small>0.2.21 (2018-10-24)</small>

* Farm meansure flow changed ([4531221](https://bitbucket.org/agribuddy/agri-web/commits/4531221))
* farm measure flow improved ([36f38bb](https://bitbucket.org/agribuddy/agri-web/commits/36f38bb))
* improved flow, remove a marker not working ([7455e8f](https://bitbucket.org/agribuddy/agri-web/commits/7455e8f))



<a name="0.2.20"></a>
## <small>0.2.20 (2018-10-24)</small>

* empty message for user list ([bdf87a1](https://bitbucket.org/agribuddy/agri-web/commits/bdf87a1))



<a name="0.2.19"></a>
## <small>0.2.19 (2018-10-24)</small>

* folder name issue ([6e94a4c](https://bitbucket.org/agribuddy/agri-web/commits/6e94a4c))



<a name="0.2.18"></a>
## <small>0.2.18 (2018-10-24)</small>

* profile pic issue, map zoom to 20 ([7906003](https://bitbucket.org/agribuddy/agri-web/commits/7906003))



<a name="0.2.17"></a>
## <small>0.2.17 (2018-10-24)</small>

* Adding localisation strings ([6f1e72c](https://bitbucket.org/agribuddy/agri-web/commits/6f1e72c))
* Adding-a-delete-button-for-RM ([904d0f1](https://bitbucket.org/agribuddy/agri-web/commits/904d0f1))
* clear farms on logout ([5cc7f49](https://bitbucket.org/agribuddy/agri-web/commits/5cc7f49))
* farms local db migration issue ([2bee59a](https://bitbucket.org/agribuddy/agri-web/commits/2bee59a))
* fetchPincode => fetchLocality ([122d98e](https://bitbucket.org/agribuddy/agri-web/commits/122d98e))
* pincode => Locality ([aa4490b](https://bitbucket.org/agribuddy/agri-web/commits/aa4490b))
* removed add farmer detil button and invite link for farmer ([ba53b37](https://bitbucket.org/agribuddy/agri-web/commits/ba53b37))



<a name="0.2.16"></a>
## <small>0.2.16 (2018-10-24)</small>

* Add Farm Form ([6e53063](https://bitbucket.org/agribuddy/agri-web/commits/6e53063))
* add farm in localdb ([0e965b6](https://bitbucket.org/agribuddy/agri-web/commits/0e965b6))
* add farm styling and bug fix ([e36add4](https://bitbucket.org/agribuddy/agri-web/commits/e36add4))
* AG-153-react-component-for-adding-farmer-with-routing ([6f840b2](https://bitbucket.org/agribuddy/agri-web/commits/6f840b2))
* AG-153-react-component-for-adding-farmers ([4510487](https://bitbucket.org/agribuddy/agri-web/commits/4510487))
* AG-165 farm photo upload from camera ([a1626b0](https://bitbucket.org/agribuddy/agri-web/commits/a1626b0))
* AG-167-mentor-home-page-shows-mentor-status ([ffde79a](https://bitbucket.org/agribuddy/agri-web/commits/ffde79a))
* AG-170 upload farm pictures and soil health card ([6f5fe53](https://bitbucket.org/agribuddy/agri-web/commits/6f5fe53))
* AG-172 bug fix ([ffbca61](https://bitbucket.org/agribuddy/agri-web/commits/ffbca61))
* AG-173-component-for-adding-profile-picture-with-localisation-strings ([bf51da8](https://bitbucket.org/agribuddy/agri-web/commits/bf51da8))
* AG-175-React-component-for-uploading-farmer-related-pictures ([3ded1f1](https://bitbucket.org/agribuddy/agri-web/commits/3ded1f1))
* AG-193-page-with-button-to-clear-the-cache ([9070497](https://bitbucket.org/agribuddy/agri-web/commits/9070497))
* clean Code, removed unwanted files ([ef0473b](https://bitbucket.org/agribuddy/agri-web/commits/ef0473b))
* CSS changes ([65b8100](https://bitbucket.org/agribuddy/agri-web/commits/65b8100))
* farm update ([c23b66d](https://bitbucket.org/agribuddy/agri-web/commits/c23b66d))
* farms module started ([62aead2](https://bitbucket.org/agribuddy/agri-web/commits/62aead2))
* floder name case issue ([a819902](https://bitbucket.org/agribuddy/agri-web/commits/a819902))
* package-lock.json ([f61c473](https://bitbucket.org/agribuddy/agri-web/commits/f61c473))
* React-component-to-show-mentor-status ([d0a8836](https://bitbucket.org/agribuddy/agri-web/commits/d0a8836))
* removed share link for supervisors ([041b256](https://bitbucket.org/agribuddy/agri-web/commits/041b256))
* request invite test ([8c6893d](https://bitbucket.org/agribuddy/agri-web/commits/8c6893d))
* skipped failing tests ([e4ae00b](https://bitbucket.org/agribuddy/agri-web/commits/e4ae00b))
* styling for fileupload ([6baee7f](https://bitbucket.org/agribuddy/agri-web/commits/6baee7f))
* tests ([432f971](https://bitbucket.org/agribuddy/agri-web/commits/432f971))
* unmerged changes ([3c0859b](https://bitbucket.org/agribuddy/agri-web/commits/3c0859b))
* Unmerged changes ([178489f](https://bitbucket.org/agribuddy/agri-web/commits/178489f))
* user/farm menu fix ([5a2c3a2](https://bitbucket.org/agribuddy/agri-web/commits/5a2c3a2))
* validation for add farm form ([1669aa6](https://bitbucket.org/agribuddy/agri-web/commits/1669aa6))



<a name="0.2.15"></a>
## <small>0.2.15 (2018-09-29)</small>

* auth side tweaks ([5ef965d](https://bitbucket.org/agribuddy/agri-web/commits/5ef965d))



<a name="0.2.14"></a>
## <small>0.2.14 (2018-09-29)</small>




<a name="0.2.13"></a>
## <small>0.2.13 (2018-09-29)</small>

* local db issue ([70fe521](https://bitbucket.org/agribuddy/agri-web/commits/70fe521))
* package lock ([48eb6d6](https://bitbucket.org/agribuddy/agri-web/commits/48eb6d6))



<a name="0.2.12"></a>
## <small>0.2.12 (2018-09-29)</small>




<a name="0.2.11"></a>
## <small>0.2.11 (2018-09-29)</small>

* user role issue debug ([9f224c0](https://bitbucket.org/agribuddy/agri-web/commits/9f224c0))



<a name="0.2.10"></a>
## <small>0.2.10 (2018-09-29)</small>

* merge issue fix ([6af6af4](https://bitbucket.org/agribuddy/agri-web/commits/6af6af4))



<a name="0.2.9"></a>
## <small>0.2.9 (2018-09-29)</small>

* AG-115-show-firstname-and-lastname-in-welcome-screen ([8664cc3](https://bitbucket.org/agribuddy/agri-web/commits/8664cc3))
* AG-122-Adding-bank-details-with-localisation-strings ([289d2f7](https://bitbucket.org/agribuddy/agri-web/commits/289d2f7))
* AG-134-mentor-and-buddy-profile ([30dbe65](https://bitbucket.org/agribuddy/agri-web/commits/30dbe65))
* AG-144-react-component-for-otp ([fab607b](https://bitbucket.org/agribuddy/agri-web/commits/fab607b))
* mereg issue ([1f1172e](https://bitbucket.org/agribuddy/agri-web/commits/1f1172e))
* resolving AG-128 conflicts ([f11646b](https://bitbucket.org/agribuddy/agri-web/commits/f11646b))
* Resolving bugs ([4e2b10d](https://bitbucket.org/agribuddy/agri-web/commits/4e2b10d))



<a name="0.2.8"></a>
## <small>0.2.8 (2018-09-29)</small>

* db for user list error ([4ec338f](https://bitbucket.org/agribuddy/agri-web/commits/4ec338f))



<a name="0.2.7"></a>
## <small>0.2.7 (2018-09-29)</small>

* keyboard adjust for username ([7792ef5](https://bitbucket.org/agribuddy/agri-web/commits/7792ef5))



<a name="0.2.6"></a>
## <small>0.2.6 (2018-09-29)</small>

* keyboard adjust for some fields ([fb87607](https://bitbucket.org/agribuddy/agri-web/commits/fb87607))



<a name="0.2.5"></a>
## <small>0.2.5 (2018-09-29)</small>

* swagger cache issuefix, network first ([ae90d13](https://bitbucket.org/agribuddy/agri-web/commits/ae90d13))



<a name="0.2.4"></a>
## <small>0.2.4 (2018-09-29)</small>

* removed network status badge ([a6bd54c](https://bitbucket.org/agribuddy/agri-web/commits/a6bd54c))



<a name="0.2.3"></a>
## <small>0.2.3 (2018-09-28)</small>

* input mask issuef ix ([bdac0dd](https://bitbucket.org/agribuddy/agri-web/commits/bdac0dd))



<a name="0.2.2"></a>
## <small>0.2.2 (2018-09-28)</small>

* nginx upload file size limit set ([8649061](https://bitbucket.org/agribuddy/agri-web/commits/8649061))
* profile pic upload progress bar and error handling ([50bbda7](https://bitbucket.org/agribuddy/agri-web/commits/50bbda7))



<a name="0.2.1"></a>
## <small>0.2.1 (2018-09-28)</small>

* custom install pwa removed ([99d8726](https://bitbucket.org/agribuddy/agri-web/commits/99d8726))



<a name="0.2.0"></a>
## 0.2.0 (2018-09-28)

* Adding localisation strings ([2cfaae5](https://bitbucket.org/agribuddy/agri-web/commits/2cfaae5))
* AG-109 update profile with pincode and address ([3097b7a](https://bitbucket.org/agribuddy/agri-web/commits/3097b7a))
* AG-117- Pincode is Mandator AG-116 Date of birth is mandatory ([19d5e6f](https://bitbucket.org/agribuddy/agri-web/commits/19d5e6f))
* AG-119 fields for address added, functionality pending ([7471092](https://bitbucket.org/agribuddy/agri-web/commits/7471092))
* AG-125 AG-141 AG-135 upload profile pic ([2e576cf](https://bitbucket.org/agribuddy/agri-web/commits/2e576cf))
* AG-132, AG-143 Buddy and Mentor can see his supervisees ([b59cb41](https://bitbucket.org/agribuddy/agri-web/commits/b59cb41))
* AG-133 call and sms button in user edit page ([9a0651d](https://bitbucket.org/agribuddy/agri-web/commits/9a0651d))
* AG-138 Edit issue for supervisors ([c464ae0](https://bitbucket.org/agribuddy/agri-web/commits/c464ae0))
* AG-138 I can see my supervisors ([1344a0d](https://bitbucket.org/agribuddy/agri-web/commits/1344a0d))
* commeted out the partially completed functionallity ([2750072](https://bitbucket.org/agribuddy/agri-web/commits/2750072))
* imporved profile validation and fixed date of birth bug in profile ([6b2843c](https://bitbucket.org/agribuddy/agri-web/commits/6b2843c))
* pincode Bug ([8452001](https://bitbucket.org/agribuddy/agri-web/commits/8452001))
* sw changes ([29051ef](https://bitbucket.org/agribuddy/agri-web/commits/29051ef))
* sw for update ([e7211be](https://bitbucket.org/agribuddy/agri-web/commits/e7211be))



<a name="0.1.5"></a>
## <small>0.1.5 (2018-09-14)</small>

* user name validation issue ([230b26f](https://bitbucket.org/agribuddy/agri-web/commits/230b26f))



<a name="0.1.4"></a>
## <small>0.1.4 (2018-09-14)</small>

* AG-78-react-component-for-change-password with localisation strings ([aa62c92](https://bitbucket.org/agribuddy/agri-web/commits/aa62c92))
* Header component with phone number masking ([cd3b4b1](https://bitbucket.org/agribuddy/agri-web/commits/cd3b4b1))
* phone format issue fixed ([324d9c7](https://bitbucket.org/agribuddy/agri-web/commits/324d9c7))
* removing unwanted package ([b202826](https://bitbucket.org/agribuddy/agri-web/commits/b202826))
* RequestInvite Container ([7203e95](https://bitbucket.org/agribuddy/agri-web/commits/7203e95))
* username validation for adduser ([8325225](https://bitbucket.org/agribuddy/agri-web/commits/8325225))
* Welcome flow issue fixed ([9e99d38](https://bitbucket.org/agribuddy/agri-web/commits/9e99d38))



<a name="0.1.3"></a>
## <small>0.1.3 (2018-09-14)</small>

* added enzyme tests, codecoverage increased ([aa17c3b](https://bitbucket.org/agribuddy/agri-web/commits/aa17c3b))
* Adding icon button for edit user ([3c5234f](https://bitbucket.org/agribuddy/agri-web/commits/3c5234f))
* Adding localisation strings and test component ([de71b50](https://bitbucket.org/agribuddy/agri-web/commits/de71b50))
* AG-24-react-component-for-set-password ([3174d26](https://bitbucket.org/agribuddy/agri-web/commits/3174d26))
* AG-61-add-choose-language-to-profile-page-with-localisation-strings ([85f3a64](https://bitbucket.org/agribuddy/agri-web/commits/85f3a64))
* AG-61-adding-choose-language-to-user-profile-page ([9ff9df2](https://bitbucket.org/agribuddy/agri-web/commits/9ff9df2))
* AG-63 PWA install prompt ([2aecbcb](https://bitbucket.org/agribuddy/agri-web/commits/2aecbcb))
* AG-64-styling-for-layout ([59180da](https://bitbucket.org/agribuddy/agri-web/commits/59180da))
* AG-67 Share option for invite link ([f4ec35d](https://bitbucket.org/agribuddy/agri-web/commits/f4ec35d))
* AG-70 improved with a progress bar ([737c1cc](https://bitbucket.org/agribuddy/agri-web/commits/737c1cc))
* AG-70 Shorten url using bitly ([6a284e2](https://bitbucket.org/agribuddy/agri-web/commits/6a284e2))
* AG-70 update with bitly for react ([d82bfaf](https://bitbucket.org/agribuddy/agri-web/commits/d82bfaf))
* AG-71 Update, case sensitive serach ([4cddae2](https://bitbucket.org/agribuddy/agri-web/commits/4cddae2))
* AG-72 AG-71 AG-80 AG-8 ([1c8ce74](https://bitbucket.org/agribuddy/agri-web/commits/1c8ce74))
* AG-72 user list search ([59eafcf](https://bitbucket.org/agribuddy/agri-web/commits/59eafcf))
* AG-73-user-menu-open-always with some  Test components ([6420c66](https://bitbucket.org/agribuddy/agri-web/commits/6420c66))
* AG-78-react-component-for-change-password ([cc14e71](https://bitbucket.org/agribuddy/agri-web/commits/cc14e71))
* AG-8 AG-24 react component for set password ([92a74ef](https://bitbucket.org/agribuddy/agri-web/commits/92a74ef))
* AG-8 improvement ([2eb213e](https://bitbucket.org/agribuddy/agri-web/commits/2eb213e))
* AG-82-react-component-for-user-list ([6889f01](https://bitbucket.org/agribuddy/agri-web/commits/6889f01))
* AG-82-user-list with ui fixes ([9092cd6](https://bitbucket.org/agribuddy/agri-web/commits/9092cd6))
* AG-88 notify user on update ([ab576ce](https://bitbucket.org/agribuddy/agri-web/commits/ab576ce))
* AG-89 page for invalid invite link ([b0225c4](https://bitbucket.org/agribuddy/agri-web/commits/b0225c4))
* AG-9 AG-99 profile input mask and validation ([90697c8](https://bitbucket.org/agribuddy/agri-web/commits/90697c8))
* AG-9 user can update his profile ([922a778](https://bitbucket.org/agribuddy/agri-web/commits/922a778))
* AG-96 Context and RXDB for user List ([8cce803](https://bitbucket.org/agribuddy/agri-web/commits/8cce803))
* AG-96 Context and RXDB, AG-72 user list ([1fb8d94](https://bitbucket.org/agribuddy/agri-web/commits/1fb8d94))
* clean code ([dcee4cc](https://bitbucket.org/agribuddy/agri-web/commits/dcee4cc))
* cleaned ui and code ([e97e15b](https://bitbucket.org/agribuddy/agri-web/commits/e97e15b))
* CSS changes ([8f77145](https://bitbucket.org/agribuddy/agri-web/commits/8f77145))
* filter default blank ([415b680](https://bitbucket.org/agribuddy/agri-web/commits/415b680))
* git merge isue ([4c57548](https://bitbucket.org/agribuddy/agri-web/commits/4c57548))
* invite flow complete ([3197a29](https://bitbucket.org/agribuddy/agri-web/commits/3197a29))
* Layout for Edit user and New user ([8fadce9](https://bitbucket.org/agribuddy/agri-web/commits/8fadce9))
* Localisation strings ([ec0c3c2](https://bitbucket.org/agribuddy/agri-web/commits/ec0c3c2))
* package-lock.json ([be4ee97](https://bitbucket.org/agribuddy/agri-web/commits/be4ee97))
* quesystring package is not compatible with react build ([50221d5](https://bitbucket.org/agribuddy/agri-web/commits/50221d5))
* removing unwanted package ([f567373](https://bitbucket.org/agribuddy/agri-web/commits/f567373))
* Routing for add user ([60aa6c0](https://bitbucket.org/agribuddy/agri-web/commits/60aa6c0))
* rxdb error fix ([7013324](https://bitbucket.org/agribuddy/agri-web/commits/7013324))
* Short url not working second time, bug fix ([c635bdf](https://bitbucket.org/agribuddy/agri-web/commits/c635bdf))
* styles ([18c2044](https://bitbucket.org/agribuddy/agri-web/commits/18c2044))
* test fix ([25bb739](https://bitbucket.org/agribuddy/agri-web/commits/25bb739))
* ui Changes for AG-72 ([8512deb](https://bitbucket.org/agribuddy/agri-web/commits/8512deb))
* user can edit first name, last name and gender ([ac8d91a](https://bitbucket.org/agribuddy/agri-web/commits/ac8d91a))



<a name="0.1.2"></a>
## <small>0.1.2 (2018-08-31)</small>

* bumped update dev server ([8fb1ced](https://bitbucket.org/agribuddy/agri-web/commits/8fb1ced))
* place holder issue fixed in login page ([615b714](https://bitbucket.org/agribuddy/agri-web/commits/615b714))



<a name="0.1.1"></a>
## <small>0.1.1 (2018-08-31)</small>

* Add User Component ([e26c369](https://bitbucket.org/agribuddy/agri-web/commits/e26c369))
* added version string in login page ([89174f7](https://bitbucket.org/agribuddy/agri-web/commits/89174f7))
* Adding-localisation-string-for-invite-link ([03d9482](https://bitbucket.org/agribuddy/agri-web/commits/03d9482))
* Adding-localisation-strings ([784bd7c](https://bitbucket.org/agribuddy/agri-web/commits/784bd7c))
* AG-15 - Login Error handling ([ae2fe9e](https://bitbucket.org/agribuddy/agri-web/commits/ae2fe9e))
* AG-15 Update snackbar component ([98d9ea9](https://bitbucket.org/agribuddy/agri-web/commits/98d9ea9))
* AG-2 - User Context improvement ([e8ce98d](https://bitbucket.org/agribuddy/agri-web/commits/e8ce98d))
* AG-21 Request and display invite link ([d594f73](https://bitbucket.org/agribuddy/agri-web/commits/d594f73))
* AG-21-get-invite-button ([9beac4f](https://bitbucket.org/agribuddy/agri-web/commits/9beac4f))
* AG-21-request-and-display-invite ([9d64a60](https://bitbucket.org/agribuddy/agri-web/commits/9d64a60))
* AG-26 language switcher on Loing page ([e7031da](https://bitbucket.org/agribuddy/agri-web/commits/e7031da))
* AG-28 disable failed test for drawer ([b9bf616](https://bitbucket.org/agribuddy/agri-web/commits/b9bf616))
* AG-28 Drawer component ([020fc0d](https://bitbucket.org/agribuddy/agri-web/commits/020fc0d))
* AG-28-react-component-for-dashboard ([07bf06f](https://bitbucket.org/agribuddy/agri-web/commits/07bf06f))
* AG-28-react-component-for-drawer ([26dc3f4](https://bitbucket.org/agribuddy/agri-web/commits/26dc3f4))
* AG-30 bitbucket-pipelines.yml edited online with Bitbucket ([f12c681](https://bitbucket.org/agribuddy/agri-web/commits/f12c681))
* AG-30 welcome.js is removed as test fails ([7efc921](https://bitbucket.org/agribuddy/agri-web/commits/7efc921))
* AG-33 Add user component update for new api ([461ac21](https://bitbucket.org/agribuddy/agri-web/commits/461ac21))
* AG-33 Add user Test fix ([0648eeb](https://bitbucket.org/agribuddy/agri-web/commits/0648eeb))
* AG-33 Component for add user ([9d95f7b](https://bitbucket.org/agribuddy/agri-web/commits/9d95f7b))
* AG-33 React componet and module for add user ([0ad57a6](https://bitbucket.org/agribuddy/agri-web/commits/0ad57a6))
* AG-34 Context actions for add user ([63a2073](https://bitbucket.org/agribuddy/agri-web/commits/63a2073))
* AG-34 Context for user List ([d264f97](https://bitbucket.org/agribuddy/agri-web/commits/d264f97))
* AG-37 AG-38, Check Account context actions ([382dc0a](https://bitbucket.org/agribuddy/agri-web/commits/382dc0a))
* AG-37 validate account new api ([fb12727](https://bitbucket.org/agribuddy/agri-web/commits/fb12727))
* AG-38 Spinner for account check ([91c625a](https://bitbucket.org/agribuddy/agri-web/commits/91c625a))
* AG-39 AG-46 Language select componet on Login page, Functional ([85bef49](https://bitbucket.org/agribuddy/agri-web/commits/85bef49))
* AG-39 Context for localisation ([62c3783](https://bitbucket.org/agribuddy/agri-web/commits/62c3783))
* AG-40 - coverage for basic components ([77b8ff2](https://bitbucket.org/agribuddy/agri-web/commits/77b8ff2))
* AG-40 Select Component for Final Form ([2130610](https://bitbucket.org/agribuddy/agri-web/commits/2130610))
* AG-40 Test for Text field ([5d6d5dc](https://bitbucket.org/agribuddy/agri-web/commits/5d6d5dc))
* AG-40-SnackBar, Text field update ([e4a38b1](https://bitbucket.org/agribuddy/agri-web/commits/e4a38b1))
* AG-42-react-context-and-actions-for-logi ([91464bc](https://bitbucket.org/agribuddy/agri-web/commits/91464bc))
* AG-43 - logout user api integration ([e64175a](https://bitbucket.org/agribuddy/agri-web/commits/e64175a))
* AG-44 fixed using timeout?? ([930e762](https://bitbucket.org/agribuddy/agri-web/commits/930e762))
* AG-44 Still not fixed, fails some times ([889ec3c](https://bitbucket.org/agribuddy/agri-web/commits/889ec3c))
* AG-44 Validate account failes some times ([0a7adc3](https://bitbucket.org/agribuddy/agri-web/commits/0a7adc3))
* AG-45-adding-localisation-strings-dashboard-login ([40f3271](https://bitbucket.org/agribuddy/agri-web/commits/40f3271))
* AG-45-adding-localisation-strings-for-add-user ([88b0de1](https://bitbucket.org/agribuddy/agri-web/commits/88b0de1))
* AG-47 Docker and Nginx ([e8a4469](https://bitbucket.org/agribuddy/agri-web/commits/e8a4469))
* AG-47 Service worker and offline ([e73c5f6](https://bitbucket.org/agribuddy/agri-web/commits/e73c5f6))
* AG-49 invitelink for added user ([fdd9fbb](https://bitbucket.org/agribuddy/agri-web/commits/fdd9fbb))
* AG-5 improvement ([cfd277c](https://bitbucket.org/agribuddy/agri-web/commits/cfd277c))
* AG-50 component to show locale strings ([e2b82bd](https://bitbucket.org/agribuddy/agri-web/commits/e2b82bd))
* AG-50 improvement ([95eb656](https://bitbucket.org/agribuddy/agri-web/commits/95eb656))
* AG-51 Drawer close when clicked out side or on menu ([cab8c27](https://bitbucket.org/agribuddy/agri-web/commits/cab8c27))
* AG-54 Login improvement ([5fd5709](https://bitbucket.org/agribuddy/agri-web/commits/5fd5709))
* AG-7 user can access dashboard usign the invite link ([7c7ba36](https://bitbucket.org/agribuddy/agri-web/commits/7c7ba36))
* AG52 AG-53 Locale issues ([5363e73](https://bitbucket.org/agribuddy/agri-web/commits/5363e73))
* api / swagger url in config file ([6008850](https://bitbucket.org/agribuddy/agri-web/commits/6008850))
* ChangePassword screen with routing ([100a08c](https://bitbucket.org/agribuddy/agri-web/commits/100a08c))
* clean code ([a79c28b](https://bitbucket.org/agribuddy/agri-web/commits/a79c28b))
* code clean ([79ba267](https://bitbucket.org/agribuddy/agri-web/commits/79ba267))
* code clean ([dbddba0](https://bitbucket.org/agribuddy/agri-web/commits/dbddba0))
* Context for app,  state of app,  like if the swagger client is ready? ([e7fb3fe](https://bitbucket.org/agribuddy/agri-web/commits/e7fb3fe))
* context in drawer ([c7ae5b3](https://bitbucket.org/agribuddy/agri-web/commits/c7ae5b3))
* Default values set for user context. ([8bb09e8](https://bitbucket.org/agribuddy/agri-web/commits/8bb09e8))
* fire base login, story board, user component on drawer. ([5c5b5ea](https://bitbucket.org/agribuddy/agri-web/commits/5c5b5ea))
* firebase Login ([fbdfd8d](https://bitbucket.org/agribuddy/agri-web/commits/fbdfd8d))
* first name and last in drawer ([239d339](https://bitbucket.org/agribuddy/agri-web/commits/239d339))
* Initial Bitbucket Pipelines configuration ([e1a1431](https://bitbucket.org/agribuddy/agri-web/commits/e1a1431))
* intial, layout is loading ([cbeed76](https://bitbucket.org/agribuddy/agri-web/commits/cbeed76))
* Layout in progress ([d692d6e](https://bitbucket.org/agribuddy/agri-web/commits/d692d6e))
* localisation-for-user-types ([2d372ce](https://bitbucket.org/agribuddy/agri-web/commits/2d372ce))
* login style tweak ([a1d1a24](https://bitbucket.org/agribuddy/agri-web/commits/a1d1a24))
* login/user role improved ([8f7313e](https://bitbucket.org/agribuddy/agri-web/commits/8f7313e))
* package-lock.json ([2844ad9](https://bitbucket.org/agribuddy/agri-web/commits/2844ad9))
* package-locks ([ebdf92f](https://bitbucket.org/agribuddy/agri-web/commits/ebdf92f))
* profile save/persist bug fix, removed unwanted files ([9ac117c](https://bitbucket.org/agribuddy/agri-web/commits/9ac117c))
* profle page and save ([8ad17ed](https://bitbucket.org/agribuddy/agri-web/commits/8ad17ed))
* React Context, Login and Nav ([f86fb57](https://bitbucket.org/agribuddy/agri-web/commits/f86fb57))
* removed old code ([253bb27](https://bitbucket.org/agribuddy/agri-web/commits/253bb27))
* removed redis actions ([48cedbb](https://bitbucket.org/agribuddy/agri-web/commits/48cedbb))
* reomved conlosle log ([b1f5e8e](https://bitbucket.org/agribuddy/agri-web/commits/b1f5e8e))
* rxdb and other changes ([4100566](https://bitbucket.org/agribuddy/agri-web/commits/4100566))
* rxdb error handled ([a027f9c](https://bitbucket.org/agribuddy/agri-web/commits/a027f9c))
* service worker updated with skip waiting ([83e8e94](https://bitbucket.org/agribuddy/agri-web/commits/83e8e94))
* side menu started ([7faead9](https://bitbucket.org/agribuddy/agri-web/commits/7faead9))
* Swagger Client integrated ([fb4db99](https://bitbucket.org/agribuddy/agri-web/commits/fb4db99))
* swagger is no longer part of app context, !cyclic dependency ([8ff2ff1](https://bitbucket.org/agribuddy/agri-web/commits/8ff2ff1))
* tests working, removed redux ([fae6096](https://bitbucket.org/agribuddy/agri-web/commits/fae6096))
* translations for drawer ([c86bf56](https://bitbucket.org/agribuddy/agri-web/commits/c86bf56))
* ui changes ([dcdea8b](https://bitbucket.org/agribuddy/agri-web/commits/dcdea8b))
* UI Changes ([5fda563](https://bitbucket.org/agribuddy/agri-web/commits/5fda563))
* undo AG-30 welcome.js is removed as test fails ([c558105](https://bitbucket.org/agribuddy/agri-web/commits/c558105))
* version is shown in every page ([1267145](https://bitbucket.org/agribuddy/agri-web/commits/1267145))



