module.exports = {
  'globDirectory': 'build/',
  'globPatterns': [
    '**/*.{js,png,json,xml,ico,html,webp,css,svg,jpg,eot,woff2,ttf,woff}',
  ],
  'swDest': 'sw.js',
};
