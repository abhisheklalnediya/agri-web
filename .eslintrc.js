module.exports = {
    extends: 'airbnb',
    rules: {
        indent: ['error', 2],
        'react/jsx-indent': ['error', 2],
        'react/jsx-indent-props': ['error', 2],
        'quote-props': ['error', 'as-needed', { unnecessary: false }],
        "react/destructuring-assignment": [0, { "extensions": [".jsx"] }],
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        'no-underscore-dangle': ['error', { allowAfterThis: false }],
        "react/prop-types": [
            "enabled",
            { "ignore": "ignore", "customValidators": "customValidator" }
          ],
          'no-plusplus': 'off',
          "react/prefer-stateless-function": "off",
        'react/forbid-prop-types': 0,
        'linebreak-style': 0,
        'jsx-a11y/anchor-is-valid': [
            'error',
            {
                components: ['Link'],
                specialLink: ['hrefLeft', 'hrefRight'],
                aspects: ['invalidHref', 'preferButton'],
            },
        ],
        'prefer-promise-reject-errors': "off",
        'max-len': [2, 250, 4],
    },
    "settings": {
      "import/resolver": {
        "node": {
          "moduleDirectory": ["node_modules", "src/"]
        }
      }
    },
    "env": {
        "browser": true,
        "node": true,
        "jest": true,
    }
};
