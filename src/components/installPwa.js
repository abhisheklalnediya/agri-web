import React from 'react';
// import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { Button, Snackbar, SnackbarContent } from '@material-ui/core';
import classNames from 'classnames';
import AppContext from 'AppContext';

import styles from './installPwa.style';


class InstallButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showBtn: false,
    };
    this.install = this.install.bind(this);
  }

  componentDidMount() {
    let deferredPrompt;
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      deferredPrompt = e;
      this.setState({
        showBtn: deferredPrompt,
      });
      return false;
    });
  }

  install() {
    const deferredPrompt = this.state.showBtn;
    deferredPrompt.prompt();
    deferredPrompt.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === 'accepted') {
          console.log('User accepted the A2HS prompt');
        } else {
          console.log('User dismissed the A2HS prompt');
        }
        this.setState({
          showBtn: false,
        });
      });
  }

  render() {
    const { classes } = this.props;
    const { showBtn } = this.state;
    return (
      showBtn
        && (
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open
            autoHideDuration={3000}
          >
            <SnackbarContent
              className={classes.info}
              message={(
                <span id="client-snackbar" className={classes.message}>
                  <AppContext.icons.info className={classNames(classes.icon, classes.iconVariant)} />
                  You can install agribuddy app.
                </span>
              )}
              action={[
                <Button
                  key="install"
                  aria-label="Close"
                  color="inherit"
                  className={classes.close}
                  onClick={this.install}
                >
                  <AppContext.icons.download className={classes.icon} />
                  Install
                </Button>,
              ]}
            />
          </Snackbar>
        )
    );
  }
}

export default withStyles(styles)(InstallButton);
