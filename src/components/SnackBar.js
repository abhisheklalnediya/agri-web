import React from 'react';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from '@material-ui/core/styles';
import styles from './SnackBar.style';

export default withStyles(styles)(({ classes, type, ...props }) => (
  <SnackbarContent className={classes[type || 'success']} {...props} />
));
