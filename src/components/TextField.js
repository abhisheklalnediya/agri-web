import React from 'react';
import {
  FormControl, FormHelperText, InputLabel, Input, InputAdornment,
} from '@material-ui/core';
import MaskedInput from 'react-text-mask';
import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe';

const Mask = {
  aadhar: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
    />
    );
  },
  phone: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
    />
    );
  },
  pan: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/[A-Z,a-z]/, /[A-Z,a-z]/, /[A-Z,a-z]/, /[A-Z,a-z]/, /[A-Z,a-z]/, /\d/, /\d/, /\d/, /\d/, /[A-Z,a-z]/]}
    />
    );
  },
  ifsc: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/[A-Z,a-z]/, /[A-Z,a-z]/, /[A-Z,a-z]/, /[A-Z,a-z]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}

    />
    );
  },
  accountnumber: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
    />
    );
  },
  pincode: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}

    />
    );
  },
  date: (props) => {
    const { inputRef, ...other } = props;
    const pipe = createAutoCorrectedDatePipe('dd-mm-yyyy', { minYear: 1900, maxYear: 2018 });
    return (<MaskedInput
      {...other}
      ref={inputRef}
      pipe={pipe}
      mask={[/\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}

    />
    );
  },
  otp: (props) => {
    const { inputRef, ...other } = props;
    return (<MaskedInput
      {...other}
      ref={inputRef}
      mask={[/\d/, /\d/, /\d/, /\d/]}
    />
    );
  },
};

export default ({
  input: {
    name, onChange, value, ...restInput
  },
  meta,
  fullWidth,
  label,
  ...rest
}) => {
  const props = { ...rest };
  if (rest.mask) {
    props.inputComponent = Mask[rest.mask];
  }
  // const helperText = (meta.touched && meta.error) || meta.submitError;
  // if (helperText) {
  //   props.helperText = helperText;
  // }
  return (
    <FormControl fullWidth={fullWidth !== false}>
      <InputLabel>{label}</InputLabel>
      <Input
        fullWidth={fullWidth !== false}
        name={name}
        error={!!(((meta.touched && meta.error) || (meta.submitError && meta.submitError.length > 0)))}
        inputProps={restInput}
        startAdornment={rest.mask && rest.mask === 'phone' && <InputAdornment position="start">+91-</InputAdornment>}
        onChange={onChange}
        value={value}
        {...props}
      />
      {(meta.submitError || (meta.error)) && <FormHelperText>{meta.submitError || meta.error}</FormHelperText>}
    </FormControl>
  );
};
