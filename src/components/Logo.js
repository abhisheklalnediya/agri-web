import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  logo: {
    width: 270,
    marginBottom: 20,
  },
});


export default withStyles(styles)(({ classes }) => (
  <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
));
