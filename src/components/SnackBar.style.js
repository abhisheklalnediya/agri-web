export default theme => ({
  success: {
    backgroundColor: theme.palette.primary.dark,
  },
  info: {
    backgroundColor: theme.palette.secondary.dark,
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
});
