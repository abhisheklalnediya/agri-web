import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';

export default ({
  input: {
    checked, name, onChange, ...restInput
  },
  ...rest
}) => (
  <Checkbox
    {...rest}
    name={name}
    inputprops={restInput}
    onChange={onChange}
    checked={!!checked}
  />
);
