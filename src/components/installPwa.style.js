import blue from '@material-ui/core/colors/blue';

export default theme => ({
  installBtn: {
    position: 'fixed',
    margin: '10px',
    right: 0,
    bottom: 0,
  },
  info: {
    backgroundColor: blue[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});
