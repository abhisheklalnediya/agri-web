import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import amber from '@material-ui/core/colors/amber';
import green from '@material-ui/core/colors/green';
import blue from '@material-ui/core/colors/blue';
import WarningIcon from '@material-ui/icons/Warning';
import _ from 'lodash';

const styles = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: blue[700],
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
  },
});

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

class SimpleSnackbar extends React.Component {
  constructor(props) {
    super(props);
    const { messages } = props;
    this.state = {
      messages,
    };
    this.handleClose = this.handleClose.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      messages: nextProps.messages,
    });
  }

  handleClose(id) {
    const { handleClose } = this.props;
    if (_.isFunction(handleClose)) {
      handleClose(id);
    }
  }

  renderButtons(messages) {
    const {
      id, buttons,
    } = messages;
    const {
      classes,
    } = this.props;

    const onClose = () => this.handleClose(id);
    if (_.isArray(buttons)) {
      return buttons.map(b => (
        <IconButton
          key={Date.now()}
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={b.action}
        >
          <b.icon className={classes.icon} />
        </IconButton>
      ));
    }

    return ([
      <IconButton
        key="close"
        aria-label="Close"
        color="inherit"
        className={classes.close}
        onClick={onClose}
      >
        <CloseIcon className={classes.icon} />
      </IconButton>,
    ]);
  }

  renderMessage(messages) {
    const {
      message, variant, id, hide,
    } = messages;
    const {
      classes, className,
    } = this.props;
    const varOrInfo = variant || 'info';
    const onClose = () => this.handleClose(id);
    const Icon = variantIcon[varOrInfo];
    return (
      <Snackbar
        key={id}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open
        autoHideDuration={hide === null ? null : 3000}
        onClose={onClose}
      >
        <SnackbarContent
          className={classNames(classes[varOrInfo], className, classes.content)}
          message={(
            <span id="client-snackbar" className={classes.message}>
              <Icon className={classNames(classes.icon, classes.iconVariant)} />
              {message}
            </span>
          )}
          action={[
            ...this.renderButtons(messages),
          ]}
        />
      </Snackbar>
    );
  }

  render() {
    const { messages } = this.state;
    if (_.isArray(messages) && messages.length) {
      return this.renderMessage(messages[0]);
    }
    if (_.isObject(messages) && messages.message) {
      return this.renderMessage(messages);
    }
    return false;
  }
}

SimpleSnackbar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleSnackbar);
