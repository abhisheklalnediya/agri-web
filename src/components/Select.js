import React from 'react';
import {
  FormControl, FormHelperText, InputLabel, Select, MenuItem, Chip,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  sectionViewProfile: {
    '& .heading': {
      marginBottom: 20,
      marginTop: 10,
    },
  },
  nodata: {
    padding: 20,
  },
  paper: {
    display: 'block',
    margin: 10,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    // minWidth: 120,
    // maxWidth: 300,
    width: '100%',
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 4,
  },
  measureContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
});


export default withStyles(styles)(({
  input: {
    name, onChange, value, ...restInput
  },
  menuItems,
  meta,
  fullWidth,
  formControlProps,
  label,
  multiple,
  classes,
  ...rest
}) => {
  const items = (menuItems && menuItems.length) ? menuItems : [];
  const helperText = (meta.touched && meta.error) || meta.submitError;

  const mutipleRender = {};
  if (multiple) {
    mutipleRender.renderValue = selected => (
      <div className={classes.chips}>
        {selected.map(v => (
          <Chip key={v} label={v} className={classes.chip} />
        ))}
      </div>
    );
  }

  return (
    <FormControl
      {...formControlProps}
      style={{
        minWidth: '100%',
      }}
    >
      <InputLabel htmlFor="age-simple">
        {label}
      </InputLabel>
      <Select
        fullWidth
        name={name}
        value={value || (multiple ? [] : value)}
        multiple={!!multiple}
        onChange={onChange}
        inputProps={restInput}
        {...rest}
        {...mutipleRender}
      >
        {items.map(category => (
          <MenuItem key={category.value} value={category.value}>
            {category.title}
          </MenuItem>
        ))}
      </Select>
      {helperText && (
        <FormHelperText>
          {helperText}
        </FormHelperText>
      )}
    </FormControl>
  );
});
