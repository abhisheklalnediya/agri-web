import React from 'react';
import ReactDOM from 'react-dom';
import TestComponent from '../Select';


it('renders without crashing', () => {
  const div = document.createElement('div');
  const props = {
    input: {
      name: 'name',
      value: 'Brr',
      onChange: () => console.log('On Change Fired'),
    },
    menuitems: [
      { value: 'a', title: 'a' },
      { value: 'b', title: 'b' },
      { value: 1, title: '1' },
      { value: 2, title: 2 },
    ],
    meta: {
      touched: true,
      error: true,
    },
  };
  ReactDOM.render(
    <div>
      <TestComponent {...props} />
    </div>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});
