import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AppCtx from 'AppContext';
import TestComponent from '../notification';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing noprops', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <TestComponent />,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

xit('renders without crashing', () => {
  const mockCallBack = jest.fn();
  const msg = {
  };
  msg.message = 'Agribuddy ';
  msg.buttons = [{
    icon: AppCtx.icons.refresh,
    action: mockCallBack,
  }];

  const button = shallow((<TestComponent messages={msg} />));
  console.log(button.html(), button.find('button'));
  button.find('button').simulate('click');
  expect(mockCallBack.mock.calls.length).toEqual(1);
});
