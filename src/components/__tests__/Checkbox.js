import React from 'react';
import ReactDOM from 'react-dom';
import TestComponent from '../Checkbox';


it('renders without crashing', () => {
  const div = document.createElement('div');
  const props = {
    input: {
      checked: true,
      name: 'name',
      onChange: () => console.log('On Change Fired'),
      someting: true,
    },
  };
  ReactDOM.render(
    <TestComponent {...props} />,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  const props = {
    input: {
      checked: false,
      name: 'name',
      onChange: () => console.log('On Change Fired'),
      someting: true,
    },
  };
  ReactDOM.render(
    <TestComponent {...props} />,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});
