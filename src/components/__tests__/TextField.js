import React from 'react';
import ReactDOM from 'react-dom';
import TestComponent from '../TextField';


it('renders without crashing', () => {
  const div = document.createElement('div');
  const props = {
    input: {
      name: 'name',
      value: 'Brr',
      onChange: () => console.log('On Change Fired'),
    },
    meta: {
      touched: true,
      error: 'Error Text',
    },
  };
  ReactDOM.render(
    <div>
      <TestComponent {...props} />
    </div>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  const props = {
    input: {
      name: 'name',
      onChange: () => console.log('On Change Fired'),
    },
    meta: {
      touched: false,
      error: 'Error Text',
    },
  };
  ReactDOM.render(
    <div>
      <TestComponent {...props} />
    </div>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});
