import React from 'react';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default ({
  input: {
    checked, value, name, onChange, ...restInput
  },
  meta,
  label,
  ...rest
}) => (
  <FormControlLabel
    control={(
      <Radio
        {...rest}
        name={name}
        inputprops={restInput}
        onChange={onChange}
        checked={!!checked}
        value={value}
      />
    )}
    label={label}
  />
);
