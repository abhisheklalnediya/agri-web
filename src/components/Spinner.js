import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import orange from '@material-ui/core/colors/orange';

export default props => <CircularProgress style={{ color: orange[50] }} thickness={7} {...props} />;
