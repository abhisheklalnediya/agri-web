
export default theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.primary,
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    },
    a: {
      textDecoration: 'none',
    },
    'input:-webkit-autofill': {
      /* border: 2px solid; */
      '-webkit-box-shadow': '0 0 0 30px #ffffff8f inset',
    },
    '.emptyMessage': {
      marginTop: 10,
    },
  },
  version: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    marginBottom: 0,
    fontSize: '70%',
    color: '#666',
  },
});
