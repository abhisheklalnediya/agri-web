import React, { Component } from 'react';
import { Router, Switch } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import modulesList from 'modulesList';
import AppCtx from 'AppContext';
import Routes from './Routes';
import styles from './App.styles';


const {
  Context,
  history,
} = AppCtx;

class App extends Component {
  constructor(props) {
    super(props);
    this.moduleList = modulesList;
    this.routes = [];
    this.state = {
    };
  }

  componentWillMount() {
  }

  componentDidMount() {
  }

  render() {
    const { classes } = this.props;
    return (
      <Context.app.Provider>
        <Context.locale.Provider>
          <Context.user.Provider>
            <div className={classes.mainContainer}>
              <Router history={history}>
                {
                  <Switch>
                    <Routes />
                  </Switch>
                }
              </Router>
              <Typography className={classes.version} variant="caption">
                {AppCtx.version}
              </Typography>
            </div>
          </Context.user.Provider>
        </Context.locale.Provider>
      </Context.app.Provider>

    );
  }
}
export default withStyles(styles)(App);
