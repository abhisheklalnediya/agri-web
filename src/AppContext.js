import { AuthProvider, AuthConsumer } from './context/user';
import { UserListProvider, UserListConsumer } from './context/usersList';
import { AppProvider, AppConsumer } from './context/app';
import { LocaleProvider, LocaleConsumer, LocaleStrings } from './context/locale';
import { FarmProvider, FarmConsumer } from './context/farms';
import { version } from '../package.json';
import * as formUtils from './util/formUtils';
import components from './components';
import history from './util/history';
import icons from './theme/icons';
import Logger from './util/eventLogger';
import { ga } from './util/withTracker';
import urlShortner from './util/urlShortner';
import * as farmUtils from './util/farmUtils';

const appCtx = {
  Logger,
  formUtils,
  farmUtils,
  components,
  locale: LocaleStrings,
  Context: {
    user: { Provider: AuthProvider, Consumer: AuthConsumer },
    userList: { Provider: UserListProvider, Consumer: UserListConsumer },
    app: { Provider: AppProvider, Consumer: AppConsumer },
    locale: { Provider: LocaleProvider, Consumer: LocaleConsumer },
    farm: { Provider: FarmProvider, Consumer: FarmConsumer },

  },
  history,
  icons,
  version,
  ga,
  urlShortner,
};
window.appCtx = appCtx;
export default appCtx;
