import User from './modules/user';
import Layout from './modules/layout';
import Login from './modules/login';
import UserList from './modules/userList';
import Farms from './modules/farms';

export default {
  Layout,
  Login,
  User,
  UserList,
  Farms,
};
