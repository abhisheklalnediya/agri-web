import { BitlyClient } from 'bitly-react';

const bitly = new BitlyClient('e287663c6236c54fefe6c6e0b3ef4944baf662f9', {});

export default url => bitly.shorten(url);
