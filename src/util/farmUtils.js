import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import styles from './farmStyles';

export function calculateArea(path) {
  const { google } = window;
  if (google && path && path.length > 2) {
    const latLngs = path.map(x => new google.maps.LatLng(x.lat, x.lng));
    const area = google.maps.geometry.spherical.computeArea(latLngs);
    return area;
  }
  return null;
}

export function calculateDistanceBetween(cord1, cord2) {
  const { google } = window;
  if (google) {
    const latLng1 = new google.maps.LatLng(cord1.lat, cord1.lng);
    const latLng2 = new google.maps.LatLng(cord2.lat, cord2.lng);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(latLng1, latLng2);
    return distance;
  }
  return null;
}

export const RenderMeasure = withStyles(styles)(({ path, classes }) => {
  const area = calculateArea(path);
  if (area !== null) {
    return (
      <React.Fragment>
        <Typography className={classes.area}>
          {'Area:'}
        </Typography>
        <Typography className={classes.areaValue}>
          {`${(area / 10000).toFixed(2)} Hectare  |  ${(area / 4046.856).toFixed(2)} Acre`}
        </Typography>
      </React.Fragment>
    );
  }
  return null;
});
