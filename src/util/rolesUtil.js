import { ROLES } from '../constants/roles';

export const processRoles = (roles) => {

  let rolesNumber = 0x0000;
  roles.forEach(roleString => {
    rolesNumber = rolesNumber + ROLES[roleString] || 0x0000
  });

  return rolesNumber;
};

export const isAdmin = (roleNumber) => {
  return (roleNumber & ROLES.ADMIN) > 0;
}

export const inRoles = (roles, roleNumber) => {
  let result = false;
  roles.forEach(role => {
    result = result || (roleNumber & role) > 0;
  });
  return result;
}