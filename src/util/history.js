import createHistory from 'history/createBrowserHistory';
import { ga } from './withTracker';

const history = createHistory();
history.listen((location) => {
  ga.pageview(location.pathname);
});
export default history;
