import { ga } from './withTracker';

const EventLogger = {
  log: (message) => {
    console.log(message)
    ga.event({
      category: 'InfoLogs',
      action: 'Message',
      label: message,
    });
  },
  error: (code, err) => {
    console.log(code)
    ga.event({
      category: 'Error',
      action: `E-${code}`,
      label: err.message,
    });
  },
};

export default EventLogger;
