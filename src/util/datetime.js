import moment from 'moment';


export function formatDate(timestamp) {
  return moment(timestamp).format('LL');
}

export function formatTime(timestamp) {
  return moment(timestamp).format('LT');
}
