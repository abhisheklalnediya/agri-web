export default () => ({
    sectionViewProfile: {
      '& .heading': {
        marginBottom: 20,
        marginTop: 10,
      },
    },
    area: {
      fontWeight: 600,
    },
    areaValue: {
      fontWeight: 500,
    },
    cordinatesList: {
      fontWeight: 400,
      overflow: 'hidden',
      '& >div': {
        float: 'left',
        padding: '0px 0px 0px 16px',
        background: 'rgba(255, 152, 0, 0.18)',
        margin: 5,
        borderRadius: 50,
      },
    },
    removeCord: {
      height: 37,
      width: 37,
    },
    nodata: {
      padding: 20,
    },
    paper: {
      display: 'block',
      margin: 10,
    },
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    formControl: {
      // minWidth: 120,
      // maxWidth: 300,
      width: '100%',
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 4,
    },
    measureContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  });
  