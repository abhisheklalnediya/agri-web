import React from 'react';
import { LocaleStrings } from '../context/locale';

export const phoneFormatter = (number) => {
  if (!number) return '';
  // NNN-NNN-NNNN
  const textPattern = /^[a-z]+/ig;
  if (textPattern.test(number)) { return number; }

  const splitter = /.{1,3}/g;
  const subnumber = number.substring(0, 10);
  const formated = subnumber.substring(0, 7).match(splitter).join('-') + subnumber.substring(7);
  return `${formated}`;
};


const passwordRegex = value => ((/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.{8,})/.test(value)) ? undefined : <LocaleStrings text="Password Rule" />);

const getVal = value => (/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?\d]/.test(value) ? <LocaleStrings text="Special Characters And Numbers Are Not Allowed" /> : undefined);

/**
 * Remove dashes added by the formatter. We want to store phones as plain numbers
 */
export const phoneParser = number => (number ? number.replace(/-/g, '') : '');

export const required = value => (value == null || value === '' ? <LocaleStrings text="Required" /> : undefined);

export const dateFormatter = date => (date || '');
export const dateParser = date => (date || '');
export const nameFormatter = value => (value == null ? <LocaleStrings text="Required" />
  : getVal(value));

export const passwordFormatter = value => (value == null ? <LocaleStrings text="Required" />
  : passwordRegex(value));
