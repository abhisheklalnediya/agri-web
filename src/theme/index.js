import { createMuiTheme } from '@material-ui/core/styles';
import {
  orange, deepOrange, red,
} from '@material-ui/core/colors';

const theme = createMuiTheme({
  'palette': {
    'background': {
      'default': '#EDEDED',
      'appDefault': '#EDEDED',
    },
    'common': {
      'primary': orange['500'],
      'black': '#000',
      'info': 'blue',
      'fullWhite': 'rgba(255, 255, 255, 1)',
      'color': 'red',
    },
    'primary': {
      ...orange,
    },
    'secondary': {
      ...deepOrange,
    },
    'error': red,
  },
  typography: {
    body1: {
      display: 'flex',
      alignItems: 'center',
    },
  },
});

export default theme;
