import close from '@material-ui/icons/Clear';
import download from '@material-ui/icons/GetApp';
import edit from '@material-ui/icons/Create';
import info from '@material-ui/icons/Info';
import mail from '@material-ui/icons/Mail';
import plus from '@material-ui/icons/Add';
import refresh from '@material-ui/icons/Refresh';
import view from '@material-ui/icons/RemoveRedEye';

import collapse from '@material-ui/icons/ExpandLess';
import expand from '@material-ui/icons/ExpandMore';

// import buddy from '@material-ui/icons/RecordVoiceOver';
// import farmer from '@material-ui/icons/Face';
// import mentor from '@material-ui/icons/Accessibility';
import addUser from '@material-ui/icons/PersonAdd';
import admin from '@material-ui/icons/SupervisorAccount';
import buddy from '@material-ui/icons/ChildCare';
import farmer from '@material-ui/icons/Mood';
import mentor from '@material-ui/icons/Person';
import users from '@material-ui/icons/People';
import share from '@material-ui/icons/Share';
import phone from '@material-ui/icons/Phone';
import sms from '@material-ui/icons/Sms';
import gpsFixed from '@material-ui/icons/GpsFixed';
import loading from '@material-ui/icons/HourglassEmpty';
import record from '@material-ui/icons/FiberManualRecord';
import farm from './wheat';

export default {
  add: plus,
  close,
  clear: close,
  download,
  edit,
  info,
  mail,
  plus,
  refresh,
  view,
  share,
  phone,
  sms,
  record,
  gpsFixed,
  // menu
  loading,
  expand,
  collapse,

  // Users
  addUser,
  admin,
  buddy,
  farmer,
  mentor,
  users,
  farm,
};
