export const root = {
  width: '100%',
  marginTop: 0,
  overflowX: 'auto',
  boxShadow: 'none',
  position: 'relative',
  padding: 10,
  background: 'none',
};

export const headingSection = {
  display: 'flex',
  padding: '10px 0px',
  alignItems: 'center',
  '& .heading-text': {
    flexGrow: 1,
  },
};
export const heading = {
  display: 'flex',
  alignItems: 'center',
  fontWeight: 500,
  fontSize: '1.2rem',
  '& svg': {
    marginRight: '5px',
  },
};

export const cardTitle = {
  display: 'flex',
  fontWeight: 500,
  fontSize: '1rem',
};

export const card = {
  marginBottom: 10,
};

export const cardContent = {
  paddingBottom: 5,
};

export const cardAction = {
  textAlign: 'right',
  display: 'block',
};
