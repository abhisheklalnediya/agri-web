const API_URL = '';
const SWAGGER_URL = `${API_URL}/explorer/swagger.json`;

export default {
  API_URL,
  SWAGGER_URL,
};
