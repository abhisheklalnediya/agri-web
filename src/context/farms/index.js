import React from 'react';
import DB from '../../db';
import {
  addFarm, editFarm, getFarms, uploadFarmPic,
} from './actions';

const FarmContext = React.createContext({
});

function getD(ud) {
  return {
    id: ud.get('id'),
    name: ud.get('name'),
    farmerId: ud.get('farmerId'),
    crops: ud.get('crops'),
    cordinates: ud.get('cordinates'),
    farmPhotos: ud.get('farmPhotos') || [],
    area: ud.get('area'),
  };
}

class FarmProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      farmList: null,
    };
    this.dbsubscription = null;
    this.addFarm = this.addFarm.bind(this);
    this.editFarm = this.editFarm.bind(this);
    this.setFarmList = this.setFarmList.bind(this);
    this.uploadFarmPic = this.uploadFarmPic.bind(this);
  }

  componentDidMount() {
    DB.then((db) => {
      this.dbsubscription = db.farms.$.subscribe(this.setFarmList);
      getFarms(db);
      this.setState({ db }, () => {
        this.setFarmList();
      });
    });
  }

  componentWillUnmount() {
    if (this.dbsubscription) this.dbsubscription.unsubscribe();
  }

  setFarmList() {
    const { db } = this.state;
    db.farms.find().exec().then((fd) => {
      this.setState({
        farmList: [
          ...fd.map(getD),
        ],
      });
    });
  }

  addFarm(v) {
    const { db } = this.state;
    return addFarm(db, v);
  }

  editFarm(v) {
    const { db } = this.state;
    return editFarm(db, v);
  }

  uploadFarmPic(...v) {
    const { db } = this.state;
    return uploadFarmPic(db, ...v);
  }

  render() {
    return (
      <FarmContext.Provider
        value={{
          farmList: this.state.farmList,
          addFarm: this.addFarm,
          editFarm: this.editFarm,
          uploadFarmPic: this.uploadFarmPic,
        }}
      >
        {this.props.children}
      </FarmContext.Provider>
    );
  }
}
const FarmConsumer = FarmContext.Consumer;

export { FarmProvider, FarmConsumer };
