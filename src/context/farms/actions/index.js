import Swagger from 'swagger-client';
import { getClient } from '../../apiClient';

export function getFarms(db) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then((c) => {
      c.farm.farm_find().then(({ body }) => {
        Promise.all(body.map(f => db.farms.upsert({ ...f }))).then(() => {
          const ids = body.map(x => x.id);
          db.farms.find({ id: { $nin: ids } }).remove().then(() => resolve(null));
        }, (e) => {
          console.log(e);
          reject({ FORM_ERROR: 'asd' });
        });
        // resolve(null);
      });
    }, () => {
      reject({ FORM_ERROR: 'asd' });
    });
  });
}

export function addFarm(db, farm) {
  const {
    farmerId, name, crops, cordinates, area,
  } = farm;

  const data = {
    farmerId, name, crops, cordinates, area,
  };
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then((c) => {
      c.farm.farm_create({ data }).then(({ body }) => {
        db.farms.insert({ ...data, id: body.id }).then(() => {
          resolve(null);
        }, () => reject({ FORM_ERROR: 'asd' }));
        // resolve(null);
      });
    }, () => {
      reject({ FORM_ERROR: 'asd' });
    });
  });
}

export function editFarm(db, farm) {
  const {
    name, crops, id,
  } = farm;

  const data = {
    name, crops,
  };
  const client = getClient();
  return new Promise((resolve, reject) => {
    db.farms.findOne({ id }).exec().then((x) => {
      x.update({
        $set: {
          ...data,
        },
      });
      client.then((c) => {
        c.farm.farm_prototype_patchAttributes({ id, data }).then(({ body }) => {
          resolve(null);
        }, () => reject({ FORM_ERROR: 'asd' }));
        // resolve(null);
      });
    }, (x) => {
      reject({ FORM_ERROR: 'asd' });
    });
  });
}

export const uploadFarmPic = (db, id, fileList) => {
  console.log(id);
  if (fileList && fileList.length === 0) return Promise.resolve([]);
  const client = getClient();
  const formData = new FormData();
  const filesToUpload = fileList.filter(x => x.photo);
  const oldFiles = fileList.filter(x => !x.photo);
  const container = `${id}-${(new Date()).getTime()}`;
  return new Promise((resolve, reject) => {
    client.then((c) => {
      Promise.all(filesToUpload.map((fileEntry, index) => fetch(fileEntry.photo)
        .then(res => res.blob())
        .then((blob) => {
          formData.append(`file${index}`, blob, fileEntry.name);
        }))).then(() => {
        const request = {
          url: `/api/documents/${container}/upload`,
          method: 'POST',
          body: formData,
          headers: {
            'content-type': 'multipart/form-data',
          },
        };
        Swagger.http(request)
          .then(({ body }) => {
            if (body && body.result && body.result.length) {
              c.farm.farm_prototype_patchAttributes({ id, data: { farmPhotos: [...oldFiles, ...body.result.map(x => ({ container, ...x }))] } })
                .then((patchRes) => {
                  db.farms.findOne({ id }).exec().then((x) => {
                    x.update({
                      $set: {
                        farmPhotos: patchRes.body.farmPhotos,
                      },
                    });
                    resolve(patchRes.body.farmPhotos);
                  });
                }, () => reject(new Error('Login error:body not in json')));
            } else reject(new Error('Login error:body not in json'));
          }, (err) => {
            reject(err);
          });
      });
    });
  });
};


export const a = 1;
