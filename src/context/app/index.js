import React from 'react';
import { getApi } from './actions';

const AppContext = React.createContext({
});

class AppProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      appReady: false,
      update: null,
      updating: false,
      drawerMenu: [],
      dialogs: [],
    };
    this.setDrawerMenu = this.setDrawerMenu.bind(this);
    this.conformationBox = this.conformationBox.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  componentDidMount() {
    // Define all app preload checks here
    getApi().then(() => {
      setTimeout(() => this.setState({
        appReady: true,
      }), 500);
    });
  }

  setDrawerMenu(menuItems) {
    const { drawerMenu } = this.state;
    this.setState({ drawerMenu: [...drawerMenu, ...menuItems] });
  }

  conformationBox({ title, body, buttons }) {
    const id = (new Date()).getTime();
    const dialogData = {
      body,
      title,
      buttons,
      id,
      close: this.closeDialog,
    };
    if (!buttons) {
      dialogData.onOk = this.closeDialog;
    }
    this.setState(state => ({ dialogs: [...state.dialogs, dialogData] }));
  }

  closeDialog(id) {
    this.setState(state => ({
      dialogs: [...state.dialogs.filter(x => (x.id !== id))],
    }));
  }

  render() {
    return (
      <AppContext.Provider
        value={{
          appReady: this.state.appReady,
          update: this.state.update,
          updating: this.state.updating,
          drawerMenu: this.state.drawerMenu,
          dialogs: this.state.dialogs,

          setDrawerMenu: this.setDrawerMenu,
          conformationBox: this.conformationBox,

        }}
      >
        {this.props.children}
      </AppContext.Provider>
    );
  }
}
const AppConsumer = AppContext.Consumer;

export { AppProvider, AppConsumer };
