import Swagger from 'swagger-client';
import CONFIG from 'config';

const swaggerUrl = CONFIG.SWAGGER_URL;

function responseInterceptor(res) {
  if (res.status === 401 && !(res.url.endsWith('/api/users/logout') || res.url.endsWith('/api/users/status'))) {
    // AppCxt.history.push('/logout');
  }
  return res;
}

const CLIENT = new Promise((resolve) => {
  Swagger(swaggerUrl,
    {
      requestInterceptor: (req) => {
        const reqWithAuth = {
          ...req,
        };
        return reqWithAuth;
      },
      responseInterceptor,
    })
    .then((client) => {
      resolve(client.apis);
    });
});

export const getClient = () => CLIENT;

export const a = 1;
