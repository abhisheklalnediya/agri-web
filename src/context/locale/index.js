import React from 'react';
import ReactDOMServer from 'react-dom/server';
import _ from 'lodash';
import LocalizedStrings from 'react-localization';
import localeStrings from './locale-strings.json';
import DB from '../../db';

const LANG = 'en';

const LocaleContext = React.createContext({});
class LocaleProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      lang: LANG,
      strings: new LocalizedStrings(localeStrings, () => LANG),
    };
    this.setLanguage = this.setLanguage.bind(this);
  }


  componentDidMount() {
    DB.then((db) => {
      this.setState({ db });
      db.app.findOne().exec().then((appDoc) => {
        if (appDoc) {
          const lang = appDoc.get('lang');
          this.setState({
            appDoc,
          }, () => this.setLanguage(lang));
        } else {
          db.app.insert({
            lang: LANG,
          }).then((doc) => {
            this.setState({ appDoc: doc }, () => this.setLanguage(LANG));
          });
        }
      });
    });
  }

  setLanguage(lang) {
    const { strings } = this.state;
    strings.setLanguage(lang);
    this.setState({
      lang,
      strings,
    }, () => {
      this.state.appDoc.update({
        $set: { lang },
      });
    });
  }

  render() {
    return (
      <LocaleContext.Provider
        value={{
          lang: this.state.lang,
          strings: this.state.strings,

          setLanguage: this.setLanguage,

        }}
      >
        {this.props.children}
      </LocaleContext.Provider>
    );
  }
}
const LocaleConsumer = LocaleContext.Consumer;

const LocaleStrings = (props) => {
  const text = _.isObject(props) ? props.text : props;
  return ReactDOMServer.renderToString(
    <LocaleConsumer>
      {locale => ((locale && locale.strings && locale.strings[text]) || text)}
    </LocaleConsumer>,
  );
};

export { LocaleProvider, LocaleConsumer, LocaleStrings };
