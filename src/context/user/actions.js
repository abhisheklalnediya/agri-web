import Swagger from 'swagger-client';
import { getClient } from '../apiClient';

export function login({ username, password }) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.user.user_login({ credentials: { username, password } }).then(({ body }) => {
      resolve(body);
    }, (e) => {
      reject(e);
    }));
  });
}

export function setAddressFromPin({ pincode, ...rest }) {
  // console.log(pincode);
  // 518003
  const client = getClient();
  return new Promise((resolve, reject) => {
    if (this.state.pincode === pincode) {
      resolve();
    } else {
      client.then((c) => {
        c.locality.locality_find({ filter: JSON.stringify({ where: { pincode } }) }).then(({ body }) => {
          if (body && body.length) {
            const [selectedPin] = body;
            const {
              district, country, state, postoffice, id,
            } = selectedPin;
            this.setState({
              ...rest,
              postoffice,
              district,
              country,
              state,
              localityId: id,
              pincode,
            });
            resolve(null);
          } else {
            this.setState({
              postoffice: '',
              district: '',
              country: '',
              state: '',
              localityId: '',
              ...rest,
              pincode,
            });
            reject({ pincode: 'Invalid' });
          }
        });
      });
    }
  });
}

export function setPassword({ password }) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.user.user_setPassword({ newPassword: password }, {
      requestContentType: 'application/x-www-form-urlencoded',
    }).then(({ body }) => {
      resolve(body);
    }, (e) => {
      reject(e);
    }));
  });
}

export function checkLogin() {
  const client = getClient();
  console.log(client);
  return client.then(c => c.user.user_getStatus());
}

export function fetchProfile(id) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.user.user_prototype___get__profile({ id }).then(({ body }) => {
      resolve(body);
    }, (e) => {
      reject(e);
    }));
  });
}
export function fetchLocality(id) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.locality.locality_findById({ id }).then(({ body }) => {
      resolve(body);
    }, (e) => {
      reject(e);
    }));
  });
}

export function verifyPhone(id) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.user.user_prototype_verifyPhone({ id }).then(() => {
      resolve(null);
    }, (e) => {
      reject(e);
    }));
  });
}

export function confirmPhone(id, token) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.user.user_prototype_confirmPhone({ id, token }, { requestContentType: 'application/x-www-form-urlencoded' }).then(({ body }) => {
      resolve(body);
    }, (e) => {
      reject(e);
    }));
  });
}

export function editUser(db, user) {
  const {
    username, firstname, lastname, email, gender, role, agriId,
  } = user;
  const client = getClient();
  const data = {
    email,
    username,
    // password: Math.random().toString(36).substring(7),
    roles: [role.toUpperCase()],
    firstname,
    lastname,
    gender,


  };
  return new Promise((resolve, reject) => {
    db.users.upsert({ ...data }).then(() => {
      if (!navigator.onLine) resolve(null);
      /* eslint no-underscore-dangle: ["error", { "allow": ["user_prototype_patchAttributes__patch_users__id_"] }] */
      client.then(c => c.user.user_prototype_patchAttributes__patch_users__id_({ id: agriId, data }).then(({ body }) => {
        resolve(body);
      }, (e) => {
        reject(e.response.body.error.details.messages);
      }));
    }, () => {
      reject({ username: 'Already exists', FORM_ERROR: 'asd' });
    });
  });
}

export const uploadProfilePic = (agriId, fileList) => {
  if (fileList && fileList.length === 0) return Promise.resolve([]);
  const client = getClient();

  const formData = new FormData();

  fileList.forEach((fileEntry, index) => {
    formData.append(`file${index}`, fileEntry);
  });
  const container = `${agriId}-${(new Date()).getTime()}`;
  const request = {
    url: `/api/documents/${container}/upload`,
    method: 'POST',
    body: formData,
    headers: {
      'content-type': 'multipart/form-data',
    },
  };
  return new Promise((resolve, reject) => {
    client.then((c) => {
      Swagger.http(request)
        .then(({ body }) => {
          if (body && body.result && body.result.length) {
            c.user.user_prototype_patchAttributes__patch_users__id_({ id: agriId, data: { profilePic: { container, ...body.result[0] } } })
              .then(patchRes => resolve(patchRes.body.profilePic), () => reject(new Error('Login error:body not in json')));
          } else reject(new Error('Login error:body not in json'));
        }, (err) => {
          reject(err);
        });
    });
  });
};


export function updateProfile(user, create) {
  const {
    address, agriId, dateOfBirth, email, firstname, gender,
    housename, lastname, locality, nationalId, panNumber, localityId,
    bankIFSC, bankAccountNo,
  } = user;
  const profileData = {
    address,
    housename,
    locality,
    nationalId,
    panNumber,
    localityId,
    bankIFSC,
    bankAccountNo,
  };
  if (dateOfBirth) profileData.dateOfBirth = dateOfBirth;
  const accountData = {
    email,
    firstname,
    lastname,
    gender,
  };
  const client = getClient();

  return new Promise((resolve, reject) => {
    client.then((c) => {
      const createOrUpdate = create ? c.user.user_prototype___create__profile : c.user.user_prototype___update__profile;
      Promise.all([
        createOrUpdate({ id: agriId, data: profileData }),
        c.user.user_prototype_patchAttributes__patch_users__id_({ id: agriId, data: accountData }),
      ]).then((x) => {
        const rbody = x.map(({ body }) => body);
        resolve(rbody.reduce((a, b) => ({ ...a, ...b })));
      }, (e) => {
        reject(e);
      });
    });
  });
}


export function logout() {
  const client = getClient();
  return client.then(c => c.user.user_logout());
}

export const a = 1;
