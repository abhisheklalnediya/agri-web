import React from 'react';
import moment from 'moment';
import * as Sentry from '@sentry/browser';
import GoogleAnalytics from 'react-ga';
import DB from '../../db';
import {
  checkLogin, fetchLocality, fetchProfile, login, logout, setAddressFromPin,
  setPassword, updateProfile, uploadProfilePic, verifyPhone, confirmPhone,
} from './actions';

const CHECK_INTERVAL = 1000 * 60 * 60 * 4; // 4HRs
const AuthContext = React.createContext({
  isAuth: false,
  login: () => false,
  logout: () => false,
});

class AuthProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      isAuth: null,
      isError: false,
      isSubmitting: false,
      username: '',
      firstname: '',
      lastname: '',
      gender: '',
      roles: [],
      forcePasswordReset: false,
      profile: null,
    };
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.updateProfile = this.updateProfile.bind(this);
    this.verifyPhone = this.verifyPhone.bind(this);
    this.confirmPhone = this.confirmPhone.bind(this);
    this.setPassword = setPassword.bind(this);
    this.checkLogin = this.checkLogin.bind(this);
    this.fetchProfile = this.fetchProfile.bind(this);
    this.checkAccount = this.checkAccount.bind(this);
    this.setAddressFromPin = setAddressFromPin.bind(this);
    this.uploadProfilePic = this.uploadProfilePic.bind(this);
  }


  componentDidMount() {
    // Login Successfull
    // 1. Read DB
    // 2. Check Account
    // 3. Set State
    DB.then((db) => {
      this.setState({ db });
      db.auth.findOne().exec().then((ud) => {
        if (ud) {
          this.checkLogin(ud);
        } else {
          this.checkLogin();
        }
      });
    });
  }


  login(creds) {
    const { db } = this.state;
    if (db) {
      db.users.find().remove();
      db.farms.find().remove();
    }
    return login(creds).then((body) => {
      this.state.db.auth.insert({
        username: creds.username,
        agriId: body.userId,
      }).then((ud) => {
        // this.setState({ isAuth: true });
        this.checkLogin(ud);
      });
    });
  }

  logout() {
    const { db } = this.state;
    const { auth, users, farms } = db;
    return Promise.all([
      auth.find().remove(),
      users.find().remove(),
      farms.find().remove(),
    ]).then(() => {
      this.setState({ isAuth: false });
      logout(this.state.username);
    });
  }

  uploadProfilePic(fileList) {
    return uploadProfilePic(this.state.agriId, fileList).then(profilePic => this.setState({ profilePic }));
  }

  fetchProfile(id) {
    return fetchProfile(id).then((profile) => {
      const dateOfBirth = profile.dateOfBirth ? moment(profile.dateOfBirth).format('DD-MM-YYYY') : '';
      this.setState({ ...profile, profile: true, dateOfBirth });
      if (profile.localityId) {
        fetchLocality(profile.localityId).then((pinData) => {
          const {
            postoffice,
            district,
            country,
            state,
            pincode,
          } = pinData;
          this.setState({
            postoffice,
            district,
            country,
            state,
            pincode,
          });
        });
      }
    },
    () => this.setState({ profile: 404 }));
  }

  updateProfile(v) {
    const { agriId, profile } = this.state;
    const profileData = { ...v, agriId };
    if (profileData.dateOfBirth) profileData.dateOfBirth = moment(profileData.dateOfBirth, 'DD-MM-YYYY').toDate();
    return new Promise((resolve, reject) => updateProfile({ ...profileData, agriId }, profile === 404).then((data) => {
      const dateOfBirth = data.dateOfBirth ? moment(data.dateOfBirth).format('DD-MM-YYYY') : '';
      this.setState({ profile: true, ...data, dateOfBirth });
      resolve(null);
    }, (e) => {
      reject(e);
    }));
  }

  checkAccount() {
    const { ud } = this.state;
    this.checkLogin(ud);
  }


  checkLogin(ud) {
    if (ud && ud.deleted) return;
    checkLogin().then(({ body }) => {
      const {
        agriId,
        username,
        firstname,
        lastname,
        gender,
        roles,
        forcePasswordReset,
        profilePic,
      } = body;
      const data = {
        agriId,
        username,
        firstname,
        lastname,
        gender,
        roles,
        forcePasswordReset,
        profilePic,
      };
      this.state.db.auth.upsert(data).then((nud) => {
        this.setState({ ud: nud, ...body, isAuth: true });
      });
      setTimeout(() => { this.checkLogin(ud); }, CHECK_INTERVAL); // Check login after x seconds
      this.fetchProfile(body.agriId);
    }, () => {
      // User is not valid
      // 1. Remove from db
      // 2. Set State
      if (ud) {
        ud.remove().then(() => {
          const { db } = this.state;
          this.setState({ isAuth: false });
          if (db) {
            db.users.find().remove();
            db.farms.find().remove();
          }
        });
      } else {
        const { db } = this.state;
        this.setState({ isAuth: false });
        if (db) {
          db.users.find().remove();
          db.farms.find().remove();
        }
      }
    });
  }

  verifyPhone() {
    const { agriId } = this.state;
    return verifyPhone(agriId);
  }

  confirmPhone({ token }) {
    const { agriId } = this.state;
    return confirmPhone(agriId, token);
  }

  render() {
    const { agriId, username, roles } = this.state;

    Sentry.configureScope((scope) => {
      scope.setUser({ id: agriId, username, roles });
      // scope.clear();
    });
    GoogleAnalytics.set({ userId: agriId });

    return (
      <AuthContext.Provider
        value={{
          ...this.state,
          login: this.login,
          logout: this.logout,
          loginFB: this.loginFB,
          verifyPhone: this.verifyPhone,
          confirmPhone: this.confirmPhone,
          updateProfile: this.updateProfile,
          setPassword: this.setPassword,
          checkAccount: this.checkAccount,
          setAddressFromPin: this.setAddressFromPin,
          uploadProfilePic: this.uploadProfilePic,
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}
const AuthConsumer = AuthContext.Consumer;

export { AuthProvider, AuthConsumer };
