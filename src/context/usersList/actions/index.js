import { getClient } from '../../apiClient';

export function getInviteCode(agriId) {
  const client = getClient();
  return client.then(c => c.invite.invite_create({ data: { agriId } }));
}

export function addUser(db, user) {
  const {
    username, firstname, lastname, email, gender, role,
  } = user;
  const client = getClient();
  const data = {
    email,
    username: username.replace(/-/g, '').replace('+91', ''),
    password: Math.random().toString(36).substring(7),
    roles: [role.toUpperCase()],
    firstname,
    lastname,
    gender,

  };
  return new Promise((resolve, reject) => {
    db.users.insert({ ...data }).then((ud) => {
      if (!navigator.onLine) resolve(null);
      client.then(c => c.user.user_create({ data }).then(({ body }) => {
        ud.set('agriId', body.agriId);
        ud.save();
        resolve(body);
      }, (e) => {
        reject(e.response.body.error.details.messages);
      }));
    }, () => {
      reject({ username: 'Already exists', FORM_ERROR: 'asd' });
    });
  });
}

export function deleteUser(db, agriId) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then(c => c.user.user_deleteById({ id: agriId }).then(() => {
      db.users.find({ agriId }).remove().then(() => {
        resolve(null);
      });
    }, (e) => {
      reject(e.response.body.error.details.messages);
    }));
  });
}

export function editUser(db, user) {
  const {
    username, firstname, lastname, email, gender, role, agriId,
  } = user;
  const client = getClient();
  const data = {
    email,
    username,
    // password: Math.random().toString(36).substring(7),
    roles: [role.toUpperCase()],
    firstname,
    lastname,
    gender,


  };
  return new Promise((resolve, reject) => {
    db.users.upsert({ ...data }).then(() => {
      if (!navigator.onLine) resolve(null);
      /* eslint no-underscore-dangle: ["error", { "allow": ["user_prototype_patchAttributes__patch_users__id_"] }] */
      client.then(c => c.user.user_prototype_patchAttributes__patch_users__id_({ id: agriId, data }).then(({ body }) => {
        resolve(body);
      }, (e) => {
        reject(e.response.body.error.details.messages);
      }));
    }, (e) => {
      reject({ username: 'Already exists', FORM_ERROR: 'asd' });
    });
  });
}

export function getUserList(db) {
  const client = getClient();
  return new Promise((resolve, reject) => {
    client.then((c) => {
      db.auth.findOne().exec().then((auth) => {
        if (auth.get('roles').includes('ADMIN')) {
          const find = c.user.user_find;
          find().then(({ body }) => {
            resolve(body);
          }, (e) => {
            reject(e);
          });
        } else {
          Promise.all([
            c.user.user_findSubTree(),
            c.user.user_findParents(),
          ]).then(([subtree, parents]) => {
            resolve([...subtree.body, ...parents.body.map(pu => ({ ...pu, roles: [...pu.roles, 'SUPERVISOR'] }))]);
          }, (e) => {
            reject(e);
          });
        }
      });
    });
  });
}

export function getUser(db, agriId) {
  // const client = getClient();
  return new Promise((resolve, reject) => {
    db.users.find({ agriId: { $eq: agriId } }).exec().then(() => {
    });
  });
}

export const a = 1;
