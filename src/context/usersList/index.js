import React from 'react';
import DB from '../../db';
import {
  addUser, editUser, getUserList, getInviteCode, deleteUser,
} from './actions';

const UserListContext = React.createContext({
});

function getUD(ud) {
  return {
    username: ud.get('username'),
    agriId: ud.get('agriId'),
    firstname: ud.get('firstname'),
    lastname: ud.get('lastname'),
    email: ud.get('email'),
    roles: ud.get('roles'),
    gender: ud.get('gender'),
  };
}

class UserListProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      userList: [],
      filter: '',
      db: null,
      selectedUser: null,
      inviteLink: null,
    };
    this.dbsubscription = null;
    this.addUser = this.addUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.editUser = this.editUser.bind(this);
    this.getInviteCode = this.getInviteCode.bind(this);
    this.syncUserList = this.syncUserList.bind(this);
    this.setUserListState = this.setUserListState.bind(this);
    this.selectUser = this.selectUser.bind(this);
    this.filter = this.filter.bind(this);
  }

  componentDidMount() {
    DB.then((db) => {
      this.dbsubscription = db.users.$.subscribe(this.setUserListState);
      this.setState({ db }, () => {
        this.syncUserList();
        this.setUserListState();
      });
    });
    window.addEventListener('online', () => setTimeout(this.setUserListState, 4000));
  }

  componentWillUnmount() {
    if (this.dbsubscription) this.dbsubscription.unsubscribe();
  }

  setUserListState() {
    const { db, selectedUser, filter } = this.state;
    let query = {};
    if (filter) {
      const regexp = new RegExp(`.*${filter}.*`, 'i');
      query = {
        $or: [
          { username: { $regex: regexp } },
          { email: { $regex: regexp } },
          { firstname: { $regex: regexp } },
          { lastname: { $regex: regexp } },
        ],
      };
    }
    db.users.find(query).exec().then((usd) => {
      this.setState({
        userList: [...usd.map((u) => {
          const uJson = getUD(u);
          uJson.selected = uJson.agriId === selectedUser;
          return uJson;
        })],
      });
    }, e => console.log(e));
  }

  getInviteCode() {
    return getInviteCode(this.state.selectedUser);
  }

  selectUser(agriId) {
    this.setState({
      selectedUser: agriId,
    }, () => {
      if (this.state.db) this.setUserListState();
    });
  }

  syncUserList() {
    const { db } = this.state;
    if (!db) return;
    getUserList(db).then((ul) => {
      const usernames = ul.map(u => u.username);
      Promise.all(ul.map(u => db.users.upsert({ ...u })))
        .then(() => db.users.find({ agriId: { $exists: true }, username: { $nin: usernames } }).remove().then(this.setUserListState));
    });
  }

  addUser(v) {
    const { db } = this.state;
    return addUser(db, v).then((res) => {
      this.setState({
        selectedUser: res.agriId,
      }, this.setUserListState);
    });
  }

  deleteUser(agriId) {
    const { db } = this.state;
    return deleteUser(db, agriId).then(() => {
      this.setState({
      }, this.setUserListState);
    });
  }

  editUser(v) {
    const { userList, db } = this.state;
    return editUser(db, v).then((res) => {
      const updateList = userList.map(u => ((u.agriId === v.agriId) ? { ...u, ...res, selected: true } : u));
      this.setState({
        userList: [...updateList],
        selectedUser: res.agriId,
      });
    });
  }

  filter(filter) {
    this.setState({ filter }, this.setUserListState);
  }

  render() {
    return (
      <UserListContext.Provider
        value={{
          userList: this.state.userList,
          selectedUser: this.state.selectedUser,
          inviteLink: this.state.inviteLink,

          addUser: this.addUser,
          deleteUser: this.deleteUser,
          editUser: this.editUser,
          getInviteCode: this.getInviteCode,
          updateUserList: this.syncUserList,
          selectUser: this.selectUser,
          filter: this.filter,
        }}
      >
        {this.props.children}
      </UserListContext.Provider>
    );
  }
}
const UserListConsumer = UserListContext.Consumer;

export { UserListProvider, UserListConsumer };
