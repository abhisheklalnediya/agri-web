import React from 'react';
import { Route, Switch } from 'react-router-dom';
import modulesList from 'modulesList';
import AppCtx from 'AppContext';

const {
  Context,
} = AppCtx;

const LayoutContainer = modulesList.Layout.Container;
const Update = modulesList.Layout.Components.update;
const { CheckAccount, Login } = modulesList.Login.Containers;
const { UserContainer, SetPassword, Welcome } = modulesList.User.Containers;
const UserListContainer = modulesList.UserList.Containers;
const FarmsContainers = modulesList.Farms.Containers;

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  componentDidMount() {
    const { setDrawerMenu } = this.props;
    setDrawerMenu([
      ...modulesList.UserList.Menu,
      ...modulesList.Farms.Menu,
    ]);
  }

  render() {
    return null;
  }
}

export default ({ menuItems, location }) => (
  <Context.app.Consumer>
    {
      app => (
        <React.Fragment>
          <Menu {...app} />
          {app.update && <Update {...app} />}
          {
            (!app.appReady && <Route component={CheckAccount} />)
            || ((location.pathname === '/logout') && <Login />)
            || (
              <Context.user.Consumer>
                {
                  auth => (
                    ((auth.isAuth == null || (auth.isAuth && auth.profile == null)) && <Route component={CheckAccount} />)
                    || (auth.isAuth && auth.profile === 404 && auth.roles.indexOf('ADMIN') === -1 && (<Welcome />)) // Welcome Flow
                    || (auth.isAuth && auth.forcePasswordReset && <SetPassword />)
                    || (auth.isAuth && (
                      <Context.userList.Provider>
                        <LayoutContainer menuItems={menuItems}>
                          <Switch>
                            <Route
                              path="/users"
                              component={UserListContainer}
                            />
                            <Route
                              path="/user"
                              component={UserContainer}
                            />
                            <Route
                              path="/farms"
                              component={FarmsContainers}
                            />
                            <Route
                              path="/"
                              component={UserContainer}
                            />
                          </Switch>
                        </LayoutContainer>
                      </Context.userList.Provider>
                    )) || <Route component={Login} />
                  )
                }
              </Context.user.Consumer>
            )
          }
        </React.Fragment>
      )
    }
  </Context.app.Consumer>

);
