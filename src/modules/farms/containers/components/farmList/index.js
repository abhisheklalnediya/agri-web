import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import Create from '@material-ui/icons/Create';
import IconButton from '@material-ui/core/IconButton';
// import {
//   Search as SearchIcon,
// } from '@material-ui/icons';
import AppCxt from 'AppContext';
import {
  Card, CardContent, Paper, Typography, Grid,
  // TextField,
} from '@material-ui/core';
import styles from './styles';

class FarmList extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    const {
      classes, farm, userList,
    } = this.props;
    const farmerDetails = (agriId) => {
      const farmer = _.find(userList.userList, { agriId });
      return farmer ? `${farmer.firstname} ${farmer.lastname}(${farmer.username})` : 'Unknown';
    };
    const farmList = _.sortBy(farm.farmList, 'name');
    console.log(farmList);
    return (
      <Paper className={classes.paper}>
        <Card className={classes.base}>
          <CardContent>
            <Typography variant="headline" component="h3">
              {AppCxt.locale('Farms List')}
            </Typography>
            {/* <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
                <SearchIcon />
              </Grid>
              <Grid item>
                <TextField onChange={(e) => { farm.filter(e.target.value); }} label="Search" />
              </Grid>
            </Grid> */}
            <Grid container spacing={8}>
              {farmList.map(f => (
                <Grid item className={classes.usercard} key={`${f.id}-${f.name}`}>
                  <Card>
                    <CardContent className={classes.content}>
                      <Grid container wrap="nowrap" spacing={8}>
                        <Grid item>
                          <AppCxt.icons.farm style={{ fontSize: 50, color: '#666' }} />
                        </Grid>
                        <Grid item style={{ flexGrow: 1 }}>
                          <Typography variant="headline" component="h3">
                            {`${f.name}`}
                          </Typography>
                          <Typography variant="subheading" component="h3">
                            Area: {`${(f.area / 10000).toFixed(2)} Hectare  |  ${(f.area / 4046.856).toFixed(2)} Acre`}
                          </Typography>
                          <Typography variant="subheading" component="h3">
                            {`${f.crops && f.crops.join(', ')}`}
                          </Typography>
                          <Typography variant="subheading" component="h3">
                            {`${farmerDetails(f.farmerId)}`}
                          </Typography>
                        </Grid>
                        {f.id && (
                          <Grid item>
                            <Link to={`/farms/${f.id}`}>
                              <IconButton>
                                <Create />
                              </IconButton>
                            </Link>
                          </Grid>
                        )}
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
            { farm.farmList === null
            && (
              <Grid className="emptyMessage" container spacing={8}>
                <Grid item>
                  <AppCxt.icons.loading />
                </Grid>
              </Grid>
            )}
            { farm.farmList && (!farm.farmList.length)
            && (
              <Grid className="emptyMessage" container spacing={8}>
                <Grid item>
                  <AppCxt.icons.clear />
                </Grid>
                <Grid item>
                  <Typography variant="subheading" component="h3">{AppCxt.locale('NoFarms')}</Typography>
                </Grid>
              </Grid>
            )}
          </CardContent>
        </Card>
      </Paper>
    );
  }
}

export default withStyles(styles)(FarmList);
