export default theme => ({
  nodata: {
    padding: 20,
  },
  paper: {
    display: 'block',
    margin: 10,
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  heading: {
    textTransform: 'capitalize',
    marginBottom: 30,
  },
  button: {
    marginLeft: 10,
    marginTop: 10,
  },
  table: {
    width: '100%',
  },
  username: {
    textTransform: 'capitalize',
    fontWeight: 'bold',
  },
  content: {
    background: '#f0f0f0',
  },
  usercard: {
    width: '100%',
  },
  tableheading: {
    textTransform: 'capitalize',
    fontWeight: 'bold',
  },
  userSearch: {
    marginTop: 10,
    display: 'block',
    height: '40',
    marginBottom: 15,
    width: '100%',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 'auto',
      marginBottom: 22,
      marginLeft: -12,
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    boxSizing: 'border-box',
    width: '100%',
    borderRadius: theme.shape.borderRadius,
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: 6,
    paddingLeft: 55,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
});
