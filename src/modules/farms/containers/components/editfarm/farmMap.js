import React from 'react';

import {
  compose, withProps, withState, withHandlers,
} from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Polygon,
} from 'react-google-maps';

const MapWithControlledZoom = compose(
  withProps({
    googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSY6mP1IiLFVXmH-GPIUBFhvpJHoYU9KU',
    loadingElement: <div style={{ width: '100%', height: '100%' }} />,
    containerElement: <div style={{ height: '50vh', width: '100%', background: '#DDD' }} />,
    mapElement: <div style={{ width: '100%', height: '100%' }} />,
  }),
  withState('zoom', 'onZoomChange', 20),
  withHandlers(() => {
    const refs = {
      map: undefined,
    };

    return {
      onMapMounted: () => (ref) => {
        refs.map = ref;
      },
      onZoomChanged: ({ onZoomChange }) => () => {
        onZoomChange(refs.map.getZoom());
      },
    };
  }),
  withScriptjs,
  withGoogleMap,
)((props) => {
  const {
    path, center, zoom, marker,
  } = props;
  return (
    <GoogleMap
      defaultCenter={center}
      zoom={zoom}
      center={center}
      ref={props.onMapMounted}
      onZoomChanged={props.onZoomChanged}
      options={{
        // styles: mapStyles,
        // mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false,
      }}
    >
      {marker && (
        <Marker
          position={marker}
          icon="/static/images/mapPin.png"
        />
      )}
      {path.map((p, i) => (
        <Marker
          label={`${i + 1}`}
          key={`${i + 1}`}
          position={p}
          icon="/static/images/mapPinFix.png"
        />
      ))}
      <Polygon
        path={path}
        geodesic
        options={{
          strokeColor: '#ff2527',
          strokeOpacity: 0.75,
          strokeWeight: 2,
        }}
      />

    </GoogleMap>

  );
});
export default MapWithControlledZoom;
