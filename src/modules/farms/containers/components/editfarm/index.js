import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Typography, FormLabel, Grid,
  Paper, Card,
} from '@material-ui/core';

import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import _ from 'lodash';

import styles from './styles';
import cropsList from '../cropsList';
import Photos from './photos';
import Map from './farmMap';

const { TextField, Radio, Select } = AppCtx.components;
const { RenderMeasure } = AppCtx.farmUtils;

const farmerprofileForm = ({
  handleSubmit,
  invalid,
  classes,
  submitSucceeded,
  submitFailed,
  values,
}) => (
  <React.Fragment>
    <form onSubmit={handleSubmit} className={classes.formViewProfile}>
      {/* <Field name="id" component="input" type="hidden" placeholder="" required /> */}
      <Typography variant="subheading" component="h3" className="heading">
        <AppCtx.locale text="Basic Details" />
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12}>
          <Field name="name" label={<AppCtx.locale text="FarmName" />} component={TextField} type="text" placeholder="" required />
        </Grid>
        <Grid item xs={12}>
          <Field name="crops" label={<AppCtx.locale text="Crops" />} component={Select} menuItems={cropsList.map((crop, index) => ({ value: <AppCtx.locale text={crop.value} />, title: <AppCtx.locale text={crop.title} /> }))} multiple type="text" placeholder="" required />
        </Grid>
      </Grid>
      {submitSucceeded && navigator.onLine && (
        <Grid container alignItems="flex-end" direction="row" spacing={8}>
          <Grid item xs={12}>
            <AppCtx.components.SnackBar message={AppCtx.locale('Farm Updated')} type="success" />
          </Grid>
        </Grid>
      )}
      {submitFailed && (
        <Grid container alignItems="flex-end" direction="row" spacing={8}>
          <Grid item xs={12}>
            <AppCtx.components.SnackBar message={AppCtx.locale('Farm Update Failed')} type="error" />
          </Grid>
        </Grid>
      )}
      <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
        <Grid item>
          <Button type="submit" variant="raised" color="primary" disabled={invalid}>
            <AppCtx.locale text="update" />
          </Button>
        </Grid>
      </Grid>

      <Typography variant="subheading" component="h3" className="heading">
        <AppCtx.locale text="Farmer Details" />
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12} sm={6}>
          <Field
            name="farmerDetails.username"
            label={<AppCtx.locale text="mobileNumber" />}
            component={TextField}
            type="text"
            placeholder=""
            required
            mask={(values && values.farmerDetails && !Number.isNaN(Number(values.farmerDetails.username))) ? 'phone' : ''}
            disabled
          />
          <Field name="farmerId" component="input" type="hidden" placeholder="" required />
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
              <Field name="farmerDetails.firstname" label={<AppCtx.locale text="firstname" />} component={TextField} type="text" placeholder="" disabled />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="farmerDetails.lastname" label={<AppCtx.locale text="lastname" />} component={TextField} type="text" placeholder="" disabled />
            </Grid>
          </Grid>
        </Grid>
      </Grid>

    </form>

  </React.Fragment>
);

const FarmerProfile = (props) => {
  const {
    classes,
    farm,
    farmId,
    userList,
  } = props;
  const selectedFarm = _.find(farm.farmList, { id: farmId });
  const farmer = selectedFarm && _.find(userList.userList, { agriId: selectedFarm.farmerId });
  const values = {
    ...selectedFarm,
    farmerDetails: farmer,
  };
  let mapData = null;
  if (selectedFarm && selectedFarm.cordinates) {
    const { cordinates } = selectedFarm;
    mapData = {
      path: selectedFarm.cordinates,
      zoom: 18,
      center: cordinates[0],
      marker: null,
    };
  }
  return (
    <section className={classes.sectionViewProfile}>
      <Paper className={classes.paper}>
        <Card className={classes.nodata}>
          <Typography variant="headline" component="h3" className="heading">
            <AppCtx.locale text="Farm" />
          </Typography>
          <Form
            onSubmit={farm.editFarm}
            render={farmerprofileForm}
            classes={classes}
            initialValues={values}
          />
          <Photos farm={farm} selectedFarm={selectedFarm} />
          {mapData
          && (
            <React.Fragment>
              <Typography variant="subheading" component="h3" className="heading">
                <AppCtx.locale text="Map" />
              </Typography>
              <RenderMeasure path={mapData.path} />
              <Grid container spacing={16} className={classes.photostatus}>
                <Grid item xs={12} className={classes.status}>
                  <Map {...mapData} />
                </Grid>
              </Grid>
            </React.Fragment>
          )
          }
        </Card>
      </Paper>
    </section>
  );
};

export default withStyles(styles)(FarmerProfile);
