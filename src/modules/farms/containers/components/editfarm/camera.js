import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  CircularProgress, Typography, Button,
} from '@material-ui/core';
import Camera, { FACING_MODES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import AppCtx from 'AppContext';
import styles from './styles';

class Photos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      error: false,
      showCamera: false,
      progress: false,
      cameraPhotos: 0,
      showMinCameraPhotosError: false,
    };
    this.renderPreview = this.renderPreview.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload() {
    if (!(this.state.cameraPhotos >= 2)) {
      // alert("Please upload atleast 2 photos with camera");
      this.setState({ showMinCameraPhotosError: true });
      return;
    }
    this.setState({ showMinCameraPhotosError: false });
    const { selectedFarm, onUpload } = this.props;
    const { photos } = this.state;
    const oldPhotos = selectedFarm ? selectedFarm.farmPhotos : [];
    this.setState({
      progress: true,
      showCamera: false,
    }, () => onUpload(selectedFarm.id, [...oldPhotos, ...photos]).then(() => this.setState({
      progress: false,
      photos: [],
    })));
  }

  renderPreview() {
    const { photos } = this.state;
    const { classes, selectedFarm } = this.props;
    const oldPhotos = selectedFarm ? selectedFarm.farmPhotos : [];
    const photosToPreview = [...oldPhotos, ...photos];
    if (!photosToPreview.length) {
      return (
        <Typography variant="subheading" component="h4" align="center">
          <AppCtx.locale text="Turn on camera and upload photos" />
        </Typography>
      );
    }

    return photosToPreview.map((p) => {
      const url = p.container ? encodeURI(`/api/documents/${p.container}/download/${p.filename}`) : p.photo;
      const style = { backgroundImage: `url(${url})` };
      if (p.container) {
        style.backgroundColor = '#ff9800';
      }
      return (
        <div className={classes.preview} style={style} />
      );
    });
  }


  render() {
    const { classes } = this.props;
    const {
      photos, progress, error, showCamera, showMinCameraPhotosError,
    } = this.state;

    const onDrop = (photo) => {
      this.setState(state => ({ photos: [...state.photos, { name: `${new Date().getTime()}.jpg`, photo }], cameraPhotos: state.cameraPhotos + 1 }));
      console.log(this.state.cameraPhotos);
    };

    return (
      <div className={classes.dropZone}>
        {!error && showCamera && (
          <Camera
            idealFacingMode={FACING_MODES.ENVIRONMENT}
            onTakePhoto={onDrop}
            onCameraError={() => this.setState({ error: true })}
          />
        )}

        {!error && !showCamera && (
          <div className={classes.buttonContainer}><Button variant="raised" onClick={() => this.setState({ showCamera: true })}><AppCtx.locale text="Open Camera" /></Button></div>
        )}
        {error && <Typography><AppCtx.locale text="Camera Error" /></Typography>}

        {showMinCameraPhotosError && <AppCtx.components.SnackBar type="error" message={AppCtx.locale('upload error message')} />}

        <div className={classes.previewContainer}>{this.renderPreview()}</div>

        <div className={classes.buttonContainer}><Button variant="raised" color="primary" disabled={!photos.length} onClick={this.handleUpload}><AppCtx.locale text="upload" /></Button></div>

        {progress && <div className={classes.buttonContainer}><CircularProgress className={classes.progress} /></div>}
      </div>
    );
  }
}
export default withStyles(styles)(Photos);
