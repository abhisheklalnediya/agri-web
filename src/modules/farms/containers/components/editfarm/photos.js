import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography, Grid,

} from '@material-ui/core';
import AppCtx from 'AppContext';
import styles from './styles';
import Camera from './camera';


const FarmPhotos = (props) => {
  const {
    classes,
    farm,
    selectedFarm,
  } = props;

  return (
    <React.Fragment>
      <Typography variant="subheading" component="h3" className="heading">
        <AppCtx.locale text="Photos" />
      </Typography>
      <Grid container spacing={16} className={classes.photostatus}>
        <Grid item xs={12} md={6} className={classes.status}>
          <Typography variant="body1">
            <AppCtx.locale text="Farm Photos" />
          </Typography>
          <Camera name="idcard" selectedFarm={selectedFarm} onUpload={farm.uploadFarmPic} />
        </Grid>
        {/* <Grid item xs={12} md={6} className={classes.status}>
          <Typography variant="body1">
            <AppCtx.locale text="Soil Health Card Photos" />
          </Typography>
          <Camera name="idcard" selectedFarm={selectedFarm} onUpload={farm.uploadSHCPic} />
        </Grid> */}
      </Grid>
    </React.Fragment>
  );
};

export default withStyles(styles)(FarmPhotos);
