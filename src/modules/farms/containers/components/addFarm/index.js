import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Paper, Step, StepContent, StepLabel,
  Stepper, Typography,
} from '@material-ui/core';
import _ from 'lodash';
import AppCtx from 'AppContext';
import { Link } from 'react-router-dom';
import SelectFarmer from './selectFarmer';
import FarmBasicDetails from './farmDetails';
import FarmMeasure from './farmMeasure';

const { calculateArea } = AppCtx.farmUtils;

const styles = theme => ({
  root: {
    // width: '90%',
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  actionsContainer: {
    marginBottom: theme.spacing.unit * 2,
  },
  resetContainer: {
    padding: theme.spacing.unit * 3,
  },
});

const initalState = {
  activeStep: 0,
  selectedFarmer: null,
  error: null,
  name: '',
  crops: [],
  cordinates: [],
  completed: false,
};
function getSteps() {
  // return ['Select Farmer', 'Basic Farm Details', 'Farm Measurement'];
  return [<AppCtx.locale text="SelectFarmer" />,
    <AppCtx.locale text="BasicFarmDetails" />,
    <AppCtx.locale text="FarmMeasurement" />];
}

class VerticalLinearStepper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initalState,
    };

    this.handleNext = this.handleNext.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.getStepContent = this.getStepContent.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


  getStepContent(step) {
    const { userList } = this.props;
    const {
      selectedFarmer, error, crops, name, cordinates, area
    } = this.state;
    const farmerList = _.sortBy(userList.userList, 'agriId').filter(u => u.roles.includes('FARMER'));
    switch (step) {
    case 0:
      return <SelectFarmer error={error} value={selectedFarmer} onChange={f => this.setState({ selectedFarmer: f })} farmerList={farmerList} />;
    case 1:
      return <FarmBasicDetails name={name} crops={crops} error={error} onChange={this.handleChange} />;
    case 2:
      return <FarmMeasure area={area} cordinates={cordinates} error={error} onChange={this.handleChange} />;
    default:
      return 'Unknown step';
    }
  }

  handleChange(v) {
    this.setState({ ...v });
  }

  handleNext() {
    const {
      activeStep, crops, name, selectedFarmer, cordinates,
    } = this.state;
    switch (activeStep) {
    case 0:
      this.setState(state => ({
        error: {
          ...state.error,
          farmer: !selectedFarmer,
        },
      }), () => {
        const { state } = this;
        if (!state.error.farmer) this.nextStep();
      });
      break;
    case 1:
      this.setState(state => ({
        error: {
          ...state.error,
          name: !name,
          crops: !crops.length,
        },
      }), () => {
        const { state } = this;
        if (!state.error.name && !state.error.crops) this.nextStep();
      });
      break;
    case 2:
      this.setState(state => ({
        error: {
          ...state.error,
          cordinates: cordinates.length < 3,
        },
      }), () => {
        const { state } = this;
        if (!state.error.cordinates) {
          this.nextStep();
          this.addFarm();
        }
      });
      break;
    default:
      this.nextStep();
    }
  }

  addFarm() {
    const { addFarm } = this.props.farm;
    const {
      selectedFarmer, name, crops, cordinates,
    } = this.state;
    const area = calculateArea(cordinates);
    addFarm({
      farmerId: parseInt(selectedFarmer, 10), name, crops, cordinates, area,
    }).then(() => {
      this.setState({
        completed: true,
      });
    });
  }

  nextStep() {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  }

  handleBack() {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  }

  handleReset() {
    this.setState({
      ...initalState,
    });
  }

  render() {
    const { classes, farm } = this.props;
    const steps = getSteps();
    const { activeStep, completed } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent>
                {this.getStepContent(index)}
                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={activeStep === 0}
                      onClick={this.handleBack}
                      className={classes.button}
                    >
                      <AppCtx.locale text="back" />
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleNext}
                      className={classes.button}
                    >
                      {activeStep === steps.length - 1 ? <AppCtx.locale text="finish" /> : <AppCtx.locale text="next" />}
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            { ((completed
            && (
              <Typography>
                <AppCtx.components.SnackBar type="success" message={AppCtx.locale('Farm Added')} />
              </Typography>)) || '...')}
            <Button onClick={this.handleReset} className={classes.button}>
              {AppCtx.locale('Clear')}
            </Button>
          </Paper>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(VerticalLinearStepper);
