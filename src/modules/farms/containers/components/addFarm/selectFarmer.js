import React, { Component } from 'react';
import {
  FormControl, FormControlLabel, FormLabel,
  FormHelperText,
  Grid, Radio, RadioGroup, Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import AppCtx from 'AppContext';
import styles from './styles';

class ChooseFarmer extends Component {
  constructor(props) {
    super(props);
    this.state = { };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onChange(e.target.value);
  }

  render() {
    const { classes, farmerList, error } = this.props;
    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={6}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel error={(error && error.farmer)} component="legend"><AppCtx.locale text="Farmers" /></FormLabel>
              <RadioGroup
                aria-label="Farmers"
                name="farmer"
                className={classes.group}
                value={this.props.value}
                onChange={this.handleChange}
              >
                {farmerList.map(fu => <FormControlLabel key={fu.agriId} value={String(fu.agriId)} control={<Radio />} label={`${fu.firstname} ${fu.lastname} (${fu.username})`} />)}
              </RadioGroup>
              {error && error.farmer && <FormHelperText error={(error && error.farmer)}><AppCtx.locale text="Required" /></FormHelperText>}
            </FormControl>
            { farmerList && !farmerList.length && <Typography>{<AppCtx.locale text="NoFarmers" />}</Typography> }
          </Grid>
        </Grid>

      </div>
    );
  }
}


export default withStyles(styles)(ChooseFarmer);
