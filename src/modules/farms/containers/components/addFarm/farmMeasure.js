import React, { Component } from 'react';
import {
  Button, CircularProgress, Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import AppCtx from 'AppContext';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

import styles from './styles';
import FarmMap from './farmMap';

const  { RenderMeasure, calculateDistanceBetween } = AppCtx.farmUtils;

class Farmer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      support: false,
      progress: false,
      center: { lat: 20.5937, lng: 78.9629 },
      centerSet: false, // eslint-disable-line
      cordinates: [],
      // zoom: 15,
      zoom: 20,
    };
    this.getCorrds = this.getCorrds.bind(this);
    this.removeCord = this.removeCord.bind(this);
    this.renderCordsList = this.renderCordsList.bind(this);
  }

  componentDidMount() {
    if (navigator.geolocation) {
      this.setState({ support: true });
      this.watchPosition = navigator.geolocation.watchPosition((p) => {
      // navigator.geolocation.getCurrentPosition((p) => {
        const marker = { lat: p.coords.latitude, lng: p.coords.longitude };
        this.setState(state => ({
          center: (!state.centerSet || calculateDistanceBetween(state.center, marker) > 12) ? marker : state.center,
          centerSet: true,
          marker,
        }));
      }, null, { enableHighAccuracy: true });
    }
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchPosition);
  }

  getCorrds() {
    const { onChange } = this.props;
    const { marker } = this.state;
    const postition = marker;
    this.setState(state => (({
      progress: true,
      center: postition,
      marker: postition,
      cordinates: [...state.cordinates, postition],
    })), () => {
      const { cordinates } = this.state;
      onChange({ cordinates });
      this.setState({
        progress: false,
      });
    });
  }

  removeCord(i) {
    const { onChange } = this.props;
    const { cordinates } = this.state;
    const nCord = [...cordinates];
    nCord.splice(i, 1);
    this.setState({
      cordinates: nCord,
    }, () => {
      onChange({ cordinates: this.state.cordinates });
    });
    this.forceUpdate();
  }

  renderCordsList() {
    const { classes } = this.props;
    const { cordinates } = this.state;
    return (
      <div className={classes.cordinatesList}>
        {cordinates.map((x, i) => (
          <div key={`${i + 1}`}>
            <span>{i + 1}</span>
            <IconButton onClick={() => this.removeCord(i)} aria-label="Delete" color="secondary" className={classes.removeCord}>
              <DeleteIcon />
            </IconButton>
          </div>
        ))}
      </div>
    );
  }

  render() {
    const {
      support, progress, center, zoom, marker, cordinates,
    } = this.state;
    const { classes, error } = this.props;
    return (
      <React.Fragment>
        <Typography variant="subheading" gutterBottom>{<AppCtx.locale text="MeasureFarm" />}</Typography>
        <Typography color="primary" variant="body1" gutterBottom>Walk around the farm and record all the corners</Typography>
        <div className={classes.measureContainer}>
          {!support && 'Your phone does not support Measurments'}
          {progress && <CircularProgress className={classes.progress} />}
          <FarmMap path={cordinates} marker={marker} center={center} zoom={zoom} />
          <Button style={{ marginTop: 5 }} variant="extendedFab" disabled={progress} color="secondary" onClick={this.getCorrds}><AppCtx.icons.record /></Button>
          {error && error.cordinates && <Typography color="error" variant="body1" gutterBottom>{<AppCtx.locale text="addboundaries" />}</Typography>}
        </div>
        <RenderMeasure path={cordinates} />
        {this.renderCordsList()}
      </React.Fragment>
    );
  }
}


export default withStyles(styles)(Farmer);
