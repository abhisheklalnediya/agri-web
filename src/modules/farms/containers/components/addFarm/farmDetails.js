import React, { Component } from 'react';
import {
  FormControl, FormLabel, Grid,
  TextField, InputLabel,
  Select, Input, Chip, MenuItem,
  FormHelperText,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import AppCtx from 'AppContext';
import styles from './styles';
import cropsList from '../cropsList';

class ChooseFarmer extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const value = {};
    value[e.target.name] = e.target.value;
    this.props.onChange(value);
  }

  render() {
    const {
      classes, error, crops, name,
    } = this.props;
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
      PaperProps: {
        style: {
          maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
          width: 250,
        },
      },
    };

    return (
      <div>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">{<AppCtx.locale text="FarmName" />}</FormLabel>
              <TextField
                id="standard-name"
                label={<AppCtx.locale text="Name" />}
                name="name"
                className={classes.textField}
                value={name}
                onChange={this.handleChange}
                margin="normal"
                error={(error && error.name)}
                helperText={error && error.name && <AppCtx.locale text="Required" />}
                inputProps={{ maxLength: '100' }}
              />
              <FormControl className={classes.formControl}>
                <InputLabel error={(error && error.crops)} htmlFor="select-multiple-chip">{<AppCtx.locale text="Crops" />}</InputLabel>
                <Select
                  fullWidth
                  multiple
                  required
                  name="crops"
                  value={crops}
                  onChange={this.handleChange}
                  input={<Input id="select-multiple-chip" />}
                  renderValue={selected => (
                    <div className={classes.chips}>
                      {selected.map(value => (
                        <Chip key={value} label={<AppCtx.locale text={value} />} className={classes.chip} />
                      ))}
                    </div>
                  )}
                  MenuProps={MenuProps}
                  error={(error && error.crops)}
                >
                  {cropsList.map(crop => (
                    <MenuItem
                      key={crop.value}
                      value={crop.value}
                      style={{
                        fontWeight: crops.indexOf(crop) === -1 ? 100 : 500,
                      }}
                    >
                      {<AppCtx.locale text={crop.title} />}
                    </MenuItem>
                  ))}
                </Select>
                {error && error.crops && <FormHelperText error={(error && error.crops)}><AppCtx.locale text="Required" /></FormHelperText>}
              </FormControl>
            </FormControl>
          </Grid>
        </Grid>
      </div>
    );
  }
}


export default withStyles(styles)(ChooseFarmer);
