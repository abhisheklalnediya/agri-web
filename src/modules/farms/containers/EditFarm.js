import React from 'react';
import AppCtx from 'AppContext';
import EditFarm from './components/editfarm';

export default ({ match }) => (
  <AppCtx.Context.user.Consumer>
    { auth => (
      <AppCtx.Context.userList.Consumer>
        { userList => (
          <AppCtx.Context.farm.Consumer>
            { farm => (
              <EditFarm farmId={match.params.farmId} user={auth} userList={userList} farm={farm} />
            )}
          </AppCtx.Context.farm.Consumer>
        )}
      </AppCtx.Context.userList.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
