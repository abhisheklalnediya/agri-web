import React from 'react';
import AppCtx from 'AppContext';
import FarmList from './components/farmList';

export default ({ match }) => (
  <AppCtx.Context.user.Consumer>
    { auth => (
      <AppCtx.Context.userList.Consumer>
        { userList => (
          <AppCtx.Context.farm.Consumer>
            { farm => (
              <FarmList filter={match.params.filter} user={auth} userList={userList} farm={farm} />
            )}
          </AppCtx.Context.farm.Consumer>
        )}
      </AppCtx.Context.userList.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
