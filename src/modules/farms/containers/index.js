import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AppCtx from 'AppContext';
import AddFarm from './AddFarm';
import FarmList from './FarmList';
import EditFarm from './EditFarm';

function ProfileContainer({ match }) {
  return (
    <AppCtx.Context.farm.Provider>
      <Switch>
        <Route
          path={`${match.url}/new`}
          exact
          render={rprops => (
            <AddFarm {...rprops} />
          )}
        />
        <Route
          path={`${match.url}/`}
          exact
          render={rprops => (
            <FarmList {...rprops} />
          )}
        />
        <Route
          path={`${match.url}/:farmId`}
          exact
          render={rprops => (
            <EditFarm {...rprops} />
          )}
        />
      </Switch>
    </AppCtx.Context.farm.Provider>
  );
}

export default ProfileContainer;
