import React from 'react';
import AppCtx from 'AppContext';
import AddFarm from './components/addFarm';

export default () => (
  <AppCtx.Context.userList.Consumer>
{ userList => (
  <AppCtx.Context.locale.Consumer>
    {({ strings }) => (
      <AppCtx.Context.farm.Consumer>
        { farm => <AddFarm farm={farm} userList={userList} strings={strings} /> }
      </AppCtx.Context.farm.Consumer>
    )}
  </AppCtx.Context.locale.Consumer>
)}
</AppCtx.Context.userList.Consumer>
);


