import AppCxt from 'AppContext';

export default [
  {
    p: 1, // for ordering
    title: 'Farms',
    link: null,
    permission: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR', 'BUDDY'],
    icon: AppCxt.icons.farm,
    sub: [
      {
        p: 0,
        title: 'Add Farm',
        link: '/farms/new',
        permission: ['BUDDY'],
      },
      {
        p: 0,
        title: 'Farms',
        link: '/farms',
        permission: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR', 'BUDDY'],
      },
    ],
  },
];
