import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import TestComponent from '../containers/components/login';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const locale = {
    strings: {},
    lang: 'en',
    setLanguage: () => {},
  };
  const auth = {
    login: () => {},
  };
  ReactDOM.render(
    <MemoryRouter>
      <TestComponent auth={auth} locale={locale} />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});
