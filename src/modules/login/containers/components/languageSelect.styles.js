export default () => ({
  langSelect: {
    float: 'right',
    width: '100%',
    display: 'flex',
    position: 'absolute',
    maxWidth: '120px',
    right: '10px',
    top: '10px',
  },
});
