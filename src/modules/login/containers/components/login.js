import React from 'react';
import {
  Button, Typography, Paper,
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import styles from './login.styles';
import LanguageSelect from './languageSelect';

const { phoneFormatter, phoneParser, required } = AppCtx.formUtils;
const { TextField } = AppCtx.components;

const loginForm = ({
  handleSubmit,
  submitting,
  classes,
  hasSubmitErrors,
  dirtySinceLastSubmit,
}) => (

  <form className={classes.loginForm} onSubmit={handleSubmit}>
    <div>
      <Field
        name="username"
        component={TextField}
        label={<AppCtx.locale text="mobileNumber" />}
        validate={required}
        format={phoneFormatter}
        parse={phoneParser}
        type="text"
        disabled={submitting}
      />
    </div>
    <div>
      <Field
        name="password"
        component={TextField}
        type="password"
        label={<AppCtx.locale text="password" />}
        validate={required}
        disabled={submitting}
      />
    </div>
    { !submitting && !dirtySinceLastSubmit && hasSubmitErrors && (
      <AppCtx.components.SnackBar type="error" message={AppCtx.locale('errormessage')} />
    )}
    <div className="actions">
      <Link to="/forgotpassword">
        <Button type="button" color="primary" className="linkButton">
          <AppCtx.locale text="forgotpassword" />
        </Button>
      </Link>
      <div className="buttons">
        <Button disabled={submitting} type="submit" color="primary" variant="raised">
          <AppCtx.locale text="login" />
        </Button>
      </div>
    </div>
  </form>
);

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  componentDidMount() {
    const { auth } = this.props;
    const { history } = AppCtx;
    if (history.location.pathname === '/logout') {
      auth.logout().then(() => history.push('/'), () => history.push('/'));
    }
  }

  render() {
    const {
      locale,
      auth,
      classes,
    } = this.props;
    return (
      <div className="login-container">
        <LanguageSelect locale={locale} />
        <div className={classes.container}>
          <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
          <Paper className={classes.paper}>
            <Typography variant="headline" component="h3" className="heading">
              <AppCtx.locale text="login" />
            </Typography>
            <Form
              onSubmit={auth.login}
              validate={(values) => {
                const errors = {};
                if (!values.username) {
                  errors.username = <AppCtx.locale text="Required" />;
                }
                if (!values.password) {
                  errors.password = <AppCtx.locale text="Required" />;
                }
                return errors;
              }}
              initialValues={auth}
              render={loginForm}
              classes={classes}
              isSubmitting={auth.isSubmitting}
            />
          </Paper>
        </div>

      </div>
    );
  }
}

export default withStyles(styles)(Login);
