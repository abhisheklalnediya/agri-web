import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppCxt from 'AppContext';

const styles = () => ({
  container: {
    display: 'flex',
    flexGrow: 1,
    alignItems: 'center',
    height: '100vh',
    justifyContent: 'center',
  },
});

export default withStyles(styles)(props => (
  <div className={props.classes.container}>
    <AppCxt.components.Spinner />
  </div>
));
