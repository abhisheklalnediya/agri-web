import React from 'react';
import {
  Select, MenuItem,
} from '@material-ui/core';

import { withStyles } from '@material-ui/core/styles';
import styles from './languageSelect.styles';

const langItems = [
  { title: 'Hindi', value: 'hi' },
  { title: 'English', value: 'en' },
];

const LangForm = ({
  locale,
  classes,
}) => {
  function handleChange(event) {
    locale.setLanguage(event.target.value);
  }
  return (
    <Select
      className={classes.langSelect}
      value={locale.lang}
      onChange={handleChange}
    >
      {langItems.map(category => (
        <MenuItem key={category.value} value={category.value}>
          {category.title}
        </MenuItem>
      ))}
    </Select>
  );
};
export default withStyles(styles)(LangForm);
