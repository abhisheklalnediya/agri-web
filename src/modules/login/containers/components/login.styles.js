export default theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 540,
    height: window.innerHeight,
    background: theme.palette.common.primary,
    flexDirection: 'column',
  },
  logo: {
    width: 270,
    marginBottom: 20,
  },
  links: {
    position: 'absolute',
    top: 0,
    right: 0,
    '& .link2Button': {
      '&:hover': {
        color: theme.palette.common.color,
      },
      textTransform: 'initial',
      textDecoration: 'underline',
    },
  },
  paper: {
    position: 'relative',
    padding: 20,
    overflow: 'auto',
    [theme.breakpoints.up('md')]: {
      minWidth: 300,
      maxWidth: '50%',
    },
    [theme.breakpoints.down('md')]: {
      boxSizing: 'border-box',
      width: '90%',
      minHeight: 250,
    },
    '& .heading': {
      marginBottom: 20,
      '& img': {
        width: '100%',
        height: 'auto',
        marginBottom: 10,
      },
    },
  },
  captcha: {
    display: 'flex',
    flexGrow: 1,
    '& >div': {
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'column',
      flexGrow: 1,
    },
  },
  snackbar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.error.dark,
  },
  loginForm: {
    '& >div': {
      marginBottom: 10,
    },
    '& >div >div': {
      display: 'flex',
    },

    '& .actions': {
      display: 'flex',
      flexDirection: 'column-reverse',
      // alignItems: 'center',
      '& .linkButton': {
        '&:hover': {
          color: theme.palette.common.color,
        },
        textTransform: 'initial',
        textDecoration: 'underline',
      },
      '& .buttons': {
        flexGrow: 1,
        alignItems: 'center',
        height: 48,
        // justifyContent: 'flex-end',
        // marginRight: -20,
      },
      '& button': {
        // marginRight: 20,
        // marginLeft: -10,
        paddingRight: 12,
        paddingLeft: 12,
        display: 'flex',
        flexGrow: 1,
      },
    },
  },
});
