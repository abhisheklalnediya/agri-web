
import React from 'react';
import AppCtx from 'AppContext';
import LoginForm from './components/login';

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <AppCtx.Context.user.Consumer>
        { auth => (
          <AppCtx.Context.locale.Consumer>
            {locale => (
              <LoginForm auth={auth} locale={locale} />
            )}
          </AppCtx.Context.locale.Consumer>
        )}
      </AppCtx.Context.user.Consumer>

    );
  }
}
export default LoginContainer;
