import React from 'react';
import AppCtx from 'AppContext';
import CheckAccount from './components/checkAccount';

export default () => (
  <AppCtx.Context.user.Consumer>
    { auth => <CheckAccount user={auth} /> }
  </AppCtx.Context.user.Consumer>
);
