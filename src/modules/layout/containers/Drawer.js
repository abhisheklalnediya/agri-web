import React from 'react';
// import { withStyles } from '@material-ui/core/styles';
// import compose from 'recompose/compose';

import AppCtx from 'AppContext';

import DrawerComponent from './components/drawer';

const Drawer = props => (
  <AppCtx.Context.user.Consumer>
    { user => (
      <AppCtx.Context.app.Consumer>
        { app => (
          <DrawerComponent user={user} app={app} {...props} />
        )}
      </AppCtx.Context.app.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
export default Drawer;
