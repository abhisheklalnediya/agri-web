const drawerWidth = 200;
const styles = theme => ({
  root: {
    // width: '100%',
    height: window.innerHeight,
    marginTop: 0,
    zIndex: 1,
    overflow: 'hidden',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    // width: '100%',
    height: '100%',
  },
  appBar: {
    position: 'absolute',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 4,
    marginRight: 0,
    color: theme.palette.common.fullWhite,
  },
  whiteText: {
    color: theme.palette.common.fullWhite,
    '& a': {
      color: theme.palette.common.fullWhite,
    },
  },
  hide: {
    display: 'none',
  },
  flexGrow: {
    flexGrow: 1,
  },
  badge: {
    '& span': {
      right: 0,
      margin: '0 15px',
      width: 'auto',
      borderRadius: '10px',
      padding: '10px',
      fontWeight: 600,
    },
  },
});

export default styles;
