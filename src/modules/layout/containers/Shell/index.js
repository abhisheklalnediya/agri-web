import React from 'react';
import classNames from 'classnames';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import compose from 'recompose/compose';

import {
  AppBar, Avatar, Button,
  Hidden, IconButton,
  Toolbar, Typography, withWidth,
} from '@material-ui/core';

import AppCtx from 'AppContext';
import Dialog from '../components/dialog';
import styles from './styles';
import Drawer from '../Drawer';
import UserMenu from '../UserMenu';

function letterAvatar(user) {
  if (user.first_name) {
    return `${user.first_name.substring(0, 1)}${user.last_name.substring(0, 1)}`;
  }
  return user.username.substring(0, 2).toUpperCase();
}

class Shell extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      xsMenuOpen: false,
      xsMenuAnchorEl: null,
    };
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleDrawerOpen() {
    this.setState({
      open: true,
    });
  }

  handleDrawerClose() {
    this.setState({
      open: false,
    });
  }

  handleClick(event) {
    this.setState({
      xsMenuOpen: true,
      xsMenuAnchorEl: event.currentTarget,
    });
  }

  handleClose() {
    this.setState({
      xsMenuOpen: false,
    });
  }

  render() {
    const {
      classes,
      fullName,
      user,
      app,
    } = this.props;

    const {
      open,
    } = this.state;
    const { xsMenuAnchorEl, xsMenuOpen } = this.state;
    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <AppBar className={classNames(classes.appBar, open && classes.appBarShift)}>
            <Toolbar disableGutters={!open}>
              <IconButton color="primary" aria-label="open drawer" onClick={this.handleDrawerOpen} className={classNames(classes.menuButton, open && classes.hide)}>
                <MenuIcon />
              </IconButton>
              <Typography
                variant="title"
                color="inherit"
                noWrap
                className={classNames(classes.flexGrow, classes.whiteText)}
              >
                <Link to="/">
                      Agribuddy
                </Link>
              </Typography>
              {/* <Link to="/requests">
                    <Badge className={classes.badge} color={online ? 'primary' : 'secondary'} badgeContent={online ? <AppCtx.locale text="Online" /> : <AppCtx.locale text="Offline" />}><span hidden /></Badge>
                  </Link> */}
              <Hidden mdUp>
                <div>
                  <IconButton aria-owns={xsMenuOpen ? 'xs-menu' : null} aria-haspopup="true" onClick={this.handleClick} className={classes.whiteText}>
                    <Avatar alt={fullName} aria-owns={xsMenuOpen ? 'xs-menu' : null} aria-haspopup="true" onClick={this.handleClick} color="accent" className={classes.whiteText}>
                      {letterAvatar(user)}
                    </Avatar>
                  </IconButton>
                  <UserMenu {...this.props} xsMenuAnchorEl={xsMenuAnchorEl} xsMenuOpen={xsMenuOpen} handleClose={this.handleClose} />
                </div>
              </Hidden>
              <Hidden smDown>
                <div>
                  <Avatar alt={fullName} aria-owns={xsMenuOpen ? 'xs-menu' : null} aria-haspopup="true" onClick={this.handleClick} color="accent" className={classes.whiteText}>
                    {letterAvatar(user)}
                  </Avatar>
                  <UserMenu {...this.props} xsMenuAnchorEl={xsMenuAnchorEl} xsMenuOpen={xsMenuOpen} handleClose={this.handleClose} />
                </div>
                <Button mini type="default" className={classes.whiteText}>
                  <AppCtx.locale text="Help" />
                </Button>
              </Hidden>
            </Toolbar>
          </AppBar>
          <Drawer user={user} open={open} handleDrawerClose={this.handleDrawerClose} />
          { this.props.children }
          { app.dialogs.map(x => <Dialog {...x} />) }
        </div>
      </div>
    );
  }
}

export default compose(withStyles(styles), withWidth())(Shell);
