

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import { Typography } from '@material-ui/core';

// Dialog module
// onOK fn to show 'ok' button
// buttons to add custom buttons
// button : {
//   text: 'Yes',
//   onClick: () => {
//     onDelete(userID);
//   },
//   buttonProps: {
//     variant: 'raised',
//     color: 'primary',
//   },
// },
class ConfirmationDialogRaw extends React.Component {
  handleOk() {
    const { onOk } = this.props;
    onOk();
  }

  renderButtons() {
    const { buttons, close, id } = this.props;
    if (buttons && buttons.length) {
      return buttons.map(b => (
        <Button key={b.text} onClick={() => b.onClick().then(() => close(id))} {...b.buttonProps}>
          {b.text}
        </Button>
      ));
    }
    return null;
  }

  render() {
    const {
      onOk, buttons, body, title, ...other
    } = this.props;

    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        // maxWidth="xs"
        onEntering={this.handleEntering}
        open
        {...other}
      >
        <DialogTitle id="confirmation-dialog-title">
          {title || body}
        </DialogTitle>
        {title && body && (
          <DialogContent>
            <Typography variant="body2" gutterBottom>
              {body}
            </Typography>
          </DialogContent>
        )}
        <DialogActions>
          {this.renderButtons()}
          { onOk
            && (
              <Button variant="raised" onClick={this.handleOk} color="primary">
                Ok
              </Button>
            )
          }
        </DialogActions>
      </Dialog>
    );
  }
}

ConfirmationDialogRaw.propTypes = {
  body: PropTypes.string.isRequired,
  title: PropTypes.string,
  buttons: PropTypes.array,
  onOk: PropTypes.func,
};

ConfirmationDialogRaw.defaultProps = {
  title: null,
  buttons: null,
  onOk: null,
};

const styles = () => ({
  root: {
  },
  paper: {
    width: '80%',
    maxHeight: 435,
  },
});

ConfirmationDialogRaw.propTypes = {
};

export default withStyles(styles)(ConfirmationDialogRaw);
