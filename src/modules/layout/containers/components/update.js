import React from 'react';
import AppCtx from 'AppContext';

export default ({ update, updating }) => {
  const updateMsg = {
    hide: null,
  };
  updateMsg.message = updating ? 'Updating...' : 'Agribuddy update is available, Please save any changes and click update.';
  updateMsg.buttons = updating ? [] : [{
    icon: AppCtx.icons.refresh,
    action: () => {
      update();
    },
  }];
  return (
    <AppCtx.components.Notification messages={updateMsg} />
  );
};
