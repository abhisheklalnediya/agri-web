import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  List, ListItem, ListItemIcon, ListItemText, Collapse,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AppCtx from 'AppContext';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
});

function Item({ item, onClick }) {
  const Icon = item.icon;
  return (
    <ListItem component={props => <Link onClick={onClick} to={item.link} {...props} />}>
      { Icon && (
        <ListItemIcon>
          <Icon />
        </ListItemIcon>
      )}
      <ListItemText inset primary={<AppCtx.locale text={item.title} />} />
    </ListItem>
  );
}

class Group extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({ open: !state.open }));
  }

  render() {
    const { item, onClick, user } = this.props;
    const Icon = item.icon;
    const rolesSet = new Set([...user.roles, 'ALL']);
    const newSub = item.sub.filter(x => x.permission.filter(y => rolesSet.has(y)).length);
    return (
      <React.Fragment>
        <ListItem button onClick={this.handleClick}>
          { Icon && (
            <ListItemIcon>
              <Icon />
            </ListItemIcon>
          )}
          <ListItemText inset primary={AppCtx.locale(item.title)} />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>

        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {newSub.map(subItem => <Item key={`${Math.random()}${item.title}`} item={subItem} onClick={onClick} />)}
          </List>
        </Collapse>
      </React.Fragment>
    );
  }
}

function Menu(props) {
  const { app } = props;
  const rolesSet = new Set([...props.user.roles, 'ALL']);
  const newMenuList = app.drawerMenu.filter(x => x.permission.filter(y => rolesSet.has(y)).length);
  return (
    <List component="nav">
      {newMenuList.map(item => (
        item.sub ? <Group key={`${Math.random()}${item.title}`} item={item} {...props} /> : <Item key={`${Math.random()}${item.title}`} item={item} {...props} />
      ))}
    </List>
  );
}

export default withStyles(styles)(Menu);
