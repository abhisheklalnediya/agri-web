import * as colors from '@material-ui/core/colors';

const drawerWidth = 240;

const styles = theme => ({
  drawerPaper: {
    height: '100%',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    background: colors.blueGrey['900'],
    '& *': {
      color: theme.palette.common.fullWhite,
    },
    '& a:hover': {
      backgroundColor: 'rgba(255,255,255,1)',
      '& *': {
        color: colors.blueGrey['900'],
      },
    },
  },
  drawerPaperClose: {
    [theme.breakpoints.up('xs')]: {
      width: 60,
      left: -1,
    },
    [theme.breakpoints.down('xs')]: {
      width: 0,
    },
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerInner: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'block',
    // alignItems: 'center',
    // justifyContent: 'flex-end',
    padding: '8px',
    ...theme.mixins.toolbar,
  },
  chevron: {
    float: 'right',
  },
});

export default styles;
