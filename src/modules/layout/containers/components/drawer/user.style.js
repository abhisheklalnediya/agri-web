
const styles = () => ({
  content: {
    display: 'flex',
    flexDirection: 'row',

  },
  cover: {
    display: 'flex',
    minWidth: '60px',
    height: '60px',
    marginRight: 5,
    borderRadius: '50%',
  },
});

export default styles;
