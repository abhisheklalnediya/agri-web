import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

import {
  CardMedia,
  Typography, withWidth,
} from '@material-ui/core';

import { getUserType } from '../../../../userList/userTypes';
import styles from './user.style';

const User = (props) => {
  const { classes, user } = props;
  return (

    <div className={classes.content}>
      <CardMedia
        className={classes.cover}
        image={(user.profilePic && encodeURI(`/api/documents/${user.profilePic.container}/download/${user.profilePic.filename}`)) || '/static/images/user-dummy.jpg'}
      />
      <div>
        <Typography variant="subheading" color="textSecondary">
          {`${user.firstname} ${user.lastname}`}
        </Typography>
        <Typography variant="body1" color="textSecondary">
          { getUserType(user.roles) }
        </Typography>
      </div>
    </div>

  );
};
export default compose(withStyles(styles), withWidth())(User);
