import React from 'react';
import classNames from 'classnames';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { withStyles } from '@material-ui/core/styles';
import compose from 'recompose/compose';

import {
  Divider, Drawer, IconButton,
  withWidth,
} from '@material-ui/core';

import DrawerMenu from './drawerMenu';

import styles from './styles';
import User from './user';

const AgriDrawer = (props) => {
  const {
    classes, handleDrawerClose, open, user, app,
  } = props;
  return (
    <Drawer classes={{ paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose) }} onClose={handleDrawerClose} open={open}>
      <div className={classes.drawerInner}>
        <div className={classes.drawerHeader}>
          <IconButton className={classes.chevron} onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
          <User user={user} />
        </div>
        <Divider />
        <DrawerMenu app={app} user={user} onClick={handleDrawerClose} />
        <Divider />
      </div>
    </Drawer>
  );
};

export default compose(withStyles(styles), withWidth())(AgriDrawer);
