import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppCtx from 'AppContext';
import Shell from './Shell';

const styles = theme => ({
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.appDefault,
    [theme.breakpoints.down('sm')]: {
      // padding: 10,
    },
    [theme.breakpoints.up('md')]: {
      // padding: 12,
    },
    height: 'calc(100% - 56px)',
    marginTop: 56,
    overflowX: 'hidden',
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      marginTop: 64,
    },
  },
});

class LayoutContainer extends Component {
  render() {
    const { classes, menuItems } = this.props;
    return (
      <AppCtx.Context.app.Consumer>
        { app => (
          <AppCtx.Context.user.Consumer>
            { user => (
              <Shell app={app} user={user} menuItems={menuItems}>
                <div className={classes.content}>
                  { this.props.children }
                </div>
              </Shell>
            )}
          </AppCtx.Context.user.Consumer>
        )}
      </AppCtx.Context.app.Consumer>
    );
  }
}

export default withStyles(styles, {
  withTheme: true,
})(LayoutContainer);
