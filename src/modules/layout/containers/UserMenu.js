import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import compose from 'recompose/compose';

import AppCtx from 'AppContext';
import {
  Menu, MenuItem, withWidth,
} from '@material-ui/core';
import styles from './Shell/styles';

const MenuContainer = (props) => {
  const {
    handleClose, xsMenuOpen, xsMenuAnchorEl,
  } = props;
  const { history } = AppCtx;
  return (
    <AppCtx.Context.user.Consumer>
      { user => (
        <Menu
          id="xs-menu"
          anchorEl={xsMenuAnchorEl}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={xsMenuOpen}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose} component={rprops => <Link to="/user/profile" {...rprops} />}>
            <AppCtx.locale text="ViewProfile" />
          </MenuItem>
          <MenuItem onClick={handleClose} component={rprops => <Link to="/user/changepassword" {...rprops} />}>
            <AppCtx.locale text="ChangePassword" />
          </MenuItem>
          <MenuItem onClick={() => user.logout().then(() => history.push('/'), () => history.push('/'))}>
            <AppCtx.locale text="Logout" />
          </MenuItem>
          <MenuItem>
            <AppCtx.locale text="Version" />
            {AppCtx.version}
          </MenuItem>
        </Menu>
      )}
    </AppCtx.Context.user.Consumer>
  );
};
export default compose(withStyles(styles), withWidth())(MenuContainer);
