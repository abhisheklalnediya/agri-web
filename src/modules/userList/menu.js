import AppCxt from 'AppContext';

export default [
  {
    p: 1, // for ordering
    title: 'Users',
    link: null,
    permission: ['ALL'],
    icon: AppCxt.icons.users,
    sub: [
      {
        p: 0,
        title: 'Add User',
        link: '/users/new',
        permission: ['ALL'],
      },
      {
        p: 0,
        title: 'Users',
        link: '/users',
        permission: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
      },
      {
        p: 0,
        title: 'Supervisors',
        link: '/users/filter/supervisors',
        permission: ['BUDDY', 'MENTOR'],
      },
      {
        p: 0,
        title: 'Buddies',
        link: '/users/filter/buddies',
        permission: ['COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR'],
      },
      {
        p: 0,
        title: 'Farmers',
        link: '/users/filter/farmers',
        permission: ['COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR', 'BUDDY'],
      },
    ],
  },
];
