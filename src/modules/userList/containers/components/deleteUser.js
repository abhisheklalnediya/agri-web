import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AppCtx from 'AppContext';


const styles = () => ({
  deletebutton: {
    marginLeft: 5,
  },
});

class DeleteUser extends React.Component {
  constructor(props) {
    super(props);
    this.deleteUser = this.deleteUser.bind(this);
  }

  deleteUser() {
   console.log("abcd");
  }

  render() {
    const { classes } = this.props;
    return (
      <Button type="button" variant="raised" color="primary" className={classes.deletebutton}>
        <AppCtx.locale text="delete" />
      </Button>
    );
  }
}
export default withStyles(styles)(DeleteUser);
