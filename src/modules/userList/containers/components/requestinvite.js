import React from 'react';
import Button from '@material-ui/core/Button';
import AppCtx from 'AppContext';
import InviteLink from './inviteLink';

class RequestInvite extends React.Component {
  constructor(props) {
    super(props);
    this.state = { inviteLink: null };
    this.getInviteLink = this.getInviteLink.bind(this);
  }

  getInviteLink() {
    const { onRequest } = this.props;
    onRequest().then(({ body }) => this.setState({
      inviteLink: `${window.location.protocol}//${window.location.host}/invite/${body.code}`,
    }));
  }

  render() {
    const { inviteLink } = this.state;
    return (
      (inviteLink && <InviteLink inviteLink={inviteLink} />)
      || (
        <Button type="button" variant="raised" color="primary" onClick={this.getInviteLink}>
          <AppCtx.locale text="Invite" />
        </Button>
      )
    );
  }
}
export default (RequestInvite);
