export default () => ({
  sectionViewProfile: {
    '& .heading': {
      marginBottom: 20,
      marginTop: 10,
    },
  },
  nodata: {
    padding: 20,
  },
  paper: {
    display: 'block',
    margin: 10,
  },
});
