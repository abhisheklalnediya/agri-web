import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Typography, FormLabel, Grid,
  FormGroup, FormControl, Paper, Card,
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import styles from './styles';
import { getCreatableUserTypes } from '../../../userTypes';
import RequestInvite from '../requestinvite';
// import DeleteUser from '../deleteUser';
import AddFarmerProfile from '../addFarmerProfile';

const { required, nameFormatter } = AppCtx.formUtils;
const { TextField, Radio, Select } = AppCtx.components;

const profileForm = ({
  handleSubmit,
  pristine,
  classes,
  form,
  owner,
  submitSucceeded,
  submitting,
  agriId,
  getInviteCode,
  inviteLink,
  submitErrors,
  values,
}) => (
  <section className={classes.sectionViewProfile}>
    <Paper className={classes.paper}>
      <Card className={classes.nodata}>
        <Typography variant="headline" component="h3" className="heading">
          {AppCtx.locale('newuser')}
        </Typography>
        <form onSubmit={handleSubmit} className={classes.formViewProfile}>
          <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
              <Field name="role" menuItems={getCreatableUserTypes(owner.roles)} label={getCreatableUserTypes(owner.roles).length <= 1 ? getCreatableUserTypes(owner.roles)[0].title : AppCtx.locale('role')} component={Select} type="text" placeholder="" validate={required} disabled={(getCreatableUserTypes(owner.roles).length) <= 1} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="username" label={AppCtx.locale('mobileNumber')} component={TextField} type="tel" placeholder="" validate={required} mask="phone" />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="firstname" label={AppCtx.locale('firstname')} component={TextField} type="text" placeholder="" validate={nameFormatter} inputProps={{ maxLength: '100' }} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="lastname" label={AppCtx.locale('lastname')} component={TextField} type="text" placeholder="" validate={nameFormatter} inputProps={{ maxLength: '100' }} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">
                  {AppCtx.locale('gender')}
                </FormLabel>
                <FormGroup row>
                  <Field
                    name="gender"
                    component={Radio}
                    type="radio"
                    value="male"
                    label={AppCtx.locale('male')}
                    required
                  />
                  <Field
                    name="gender"
                    component={Radio}
                    type="radio"
                    value="female"
                    label={AppCtx.locale('female')}
                  />
                </FormGroup>
              </FormControl>
            </Grid>
            { (submitSucceeded && values.role !== 'FARMER') && (
              <AppCtx.Context.user.Consumer>
                { user => ((!user.roles.includes('BUDDY') && submitSucceeded && agriId) && (
                  <Grid item xs={12} sm={6}>
                    <RequestInvite onRequest={getInviteCode} inviteLink={inviteLink} />
                  </Grid>
                ))
                }
              </AppCtx.Context.user.Consumer>
            )}
            { (submitSucceeded && values.role === 'FARMER') && (
              <Grid item xs={12} sm={6}>
                <AddFarmerProfile />
              </Grid>)
            }
          </Grid>
          {submitSucceeded && navigator.onLine && (
            <Grid container alignItems="flex-end" direction="row" spacing={8}>
              <Grid item xs={12}>
                <AppCtx.components.SnackBar message={AppCtx.locale('User Added')} type="success" />
              </Grid>
            </Grid>
          )}
          {submitSucceeded && !navigator.onLine && (
            <Grid container alignItems="flex-end" direction="row" spacing={8}>
              <Grid item xs={12}>
                <AppCtx.components.SnackBar message={AppCtx.locale('User Added Offline')} type="success" />
              </Grid>
            </Grid>
          )}
          {submitErrors && submitErrors.FORM_ERROR && (
            <Grid container alignItems="flex-end" direction="row" spacing={8}>
              <Grid item xs={12}>
                <AppCtx.components.SnackBar message={AppCtx.locale('Failed to add user')} type="error" />
              </Grid>
            </Grid>
          )}
          <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
            <Grid item>
              <Button type="submit" variant="raised" color="primary" disabled={pristine || submitting || submitSucceeded}>
                {AppCtx.locale('Submit')}
              </Button>
            </Grid>
            <Grid item>
              <Button disabled={pristine || submitting} type="button" variant="raised" onClick={form.reset}>
                {AppCtx.locale('Clear')}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Card>
    </Paper>
  </section>
);


function Profile(props) {
  const {
    classes,
    user,
    userList,
  } = props;
  const initialValues = {};
  const possibleUserTypes = getCreatableUserTypes(user.roles);
  if (possibleUserTypes.length) initialValues.userType = possibleUserTypes[0].value;
  return (
    <section className={classes.sectionViewProfile}>
      <Form
        onSubmit={userList.addUser}
        validate={(values) => {
          const errors = {};
          if (values.username && !values.username.replace(/_/g, '').replace(/-/g, '').replace('+91', '')) {
            errors.username = <AppCtx.locale text="Required" />;
          }
          return errors;
        }}
        getInviteCode={userList.getInviteCode}
        req
        render={profileForm}
        classes={classes}
        initialValues={initialValues}
        owner={user}
        agriId={userList.selectedUser}
        inviteLink={userList.inviteLink}
      />
    </section>
  );
}

export default withStyles(styles)(Profile);
