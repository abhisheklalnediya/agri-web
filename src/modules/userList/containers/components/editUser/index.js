import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography, FormLabel, Grid, Card,
  FormGroup, FormControl, Paper,
  Button,
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import styles from './styles';
import { getCreatableUserTypes, isDeletable } from '../../../userTypes';
import RequestInvite from '../requestinvite';
import CallBtn from '../callSMS';
// import DeleteUser from '../deleteUser';

const { required } = AppCtx.formUtils;
const { TextField, Radio, Select } = AppCtx.components;

const profileForm = ({
  handleSubmit,
  onDelete,
  pristine,
  classes,
  form,
  owner,
  submitSucceeded,
  submitting,
  getInviteCode,
  inviteLink,
  submitErrors,
  values,
  user,
  disabled,
}) => (
  <section className={classes.sectionViewProfile}>
    <Paper className={classes.paper}>
      <Card className={classes.nodata}>
        <Typography variant="headline" component="h3" className="heading">
          {disabled ? AppCtx.locale('View User') : AppCtx.locale('Edit User')}
        </Typography>
        <form onSubmit={handleSubmit} className={classes.formViewProfile}>
          <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
              {(disabled && <Field name="role" label={AppCtx.locale('role')} component={TextField} type="text" placeholder="" validate={required} disabled={disabled} />)
              || <Field name="role" menuItems={getCreatableUserTypes(owner.roles)} label={AppCtx.locale('role')} component={Select} type="text" placeholder="" validate={required} />}
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="username" label={AppCtx.locale('mobileNumber')} component={TextField} type="text" placeholder="" validate={required} mask="phone" disabled />
              <CallBtn phonenumber={values.username} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="firstname" label={AppCtx.locale('firstname')} component={TextField} type="text" placeholder="" validate={required} disabled={disabled} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="lastname" label={AppCtx.locale('lastname')} component={TextField} type="text" placeholder="" validate={required} disabled={disabled} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">
                  {AppCtx.locale('gender')}
                </FormLabel>
                <FormGroup row>
                  <Field
                    name="gender"
                    component={Radio}
                    type="radio"
                    value="male"
                    label={AppCtx.locale('male')}
                    disabled={disabled}
                  />
                  <Field
                    name="gender"
                    component={Radio}
                    type="radio"
                    value="female"
                    label={AppCtx.locale('female')}
                    disabled={disabled}
                  />
                </FormGroup>
              </FormControl>
            </Grid>
            { !disabled && (
              <Grid item xs={12} sm={6}>
                <RequestInvite onRequest={getInviteCode} inviteLink={inviteLink} />
              </Grid>
            )}
          </Grid>
          {submitSucceeded && navigator.onLine && (
            <Grid container alignItems="flex-end" direction="row" spacing={8}>
              <Grid item xs={12}>
                <AppCtx.components.SnackBar message={AppCtx.locale('User Updated')} type="success" />
              </Grid>
            </Grid>
          )}
          {submitSucceeded && !navigator.onLine && (
            <Grid container alignItems="flex-end" direction="row" spacing={8}>
              <Grid item xs={12}>
                <AppCtx.components.SnackBar message={AppCtx.locale('User Updated Offline')} type="success" />
              </Grid>
            </Grid>
          )}
          {submitErrors && submitErrors.FORM_ERROR && (
            <Grid container alignItems="flex-end" direction="row" spacing={8}>
              <Grid item xs={12}>
                <AppCtx.components.SnackBar message={AppCtx.locale('Failed to add user')} type="error" />
              </Grid>
            </Grid>
          )}
          {!disabled
          && (
            <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
              <Grid item>
                <Button type="submit" variant="raised" color="primary" disabled={pristine || submitting || submitSucceeded}>
                  {AppCtx.locale('Submit')}
                </Button>
              </Grid>
              <Grid item>
                <Button disabled={pristine || submitting} type="button" variant="raised" onClick={form.reset}>
                  {AppCtx.locale('Clear')}
                </Button>
              </Grid>
              {user && user.agriId !== owner.agriId && isDeletable(owner.roles, user.roles)
              && (
                <Grid item>
                  <Button type="button" variant="raised" color="primary" onClick={onDelete} disabled={submitting}>
                    {AppCtx.locale('Delete')}
                  </Button>
                </Grid>
              )}
            </Grid>
          )
          }
        </form>
      </Card>
    </Paper>
  </section>
);

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    const { agriId } = this.props.match.params;
    this.props.userList.selectUser(parseInt(agriId, 10));
  }

  componentWillUnmount() {
    this.props.userList.selectUser(null);
  }

  onDelete() {
    const { userList, app } = this.props;
    const user = userList.userList.find(x => x.selected);
    app.conformationBox({
      title: 'Delete User?',
      body: `Are you sure you want to delete ${user.firstname} ${user.lastname}?`,
      buttons: [
        {
          text: 'Yes',
          onClick: () => userList.deleteUser(user.agriId).then(() => (AppCtx.history.replace('/users/'))),
          buttonProps: {
            variant: 'raised',
            color: 'secondary',
          },
        },
        {
          text: 'No',
          onClick: () => Promise.resolve(),
          buttonProps: {
            variant: 'raised',
            color: 'default',
          },
        },
      ],
    });
  }

  render() {
    const {
      classes,
      auth,
      userList,
    } = this.props;
    const user = userList.userList.find(x => x.selected);
    let disabled = false;
    if (user) {
      [user.role] = user.roles;
      disabled = user.roles.includes('SUPERVISOR');
    }
    return (
      <section className={classes.sectionViewProfile}>
        <Form
          onSubmit={userList.editUser}
          onDelete={this.onDelete}
          getInviteCode={userList.getInviteCode}
          req
          render={profileForm}
          classes={classes}
          initialValues={user}
          owner={auth}
          inviteLink={userList.inviteLink}
          disabled={disabled}
          user={user}
        />
      </section>
    );
  }
}


export default withStyles(styles)(Profile);
