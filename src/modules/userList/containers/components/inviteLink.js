import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppCtx from 'AppContext';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import orange from '@material-ui/core/colors/orange';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  link: {
    color: orange[800],
  },
});

class InviteLink extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteLink: this.props.inviteLink,
      utlShorting: true,
    };
  }

  componentDidMount() {
    const { inviteLink } = this.state;
    if (inviteLink) {
      console.log(inviteLink);
      AppCtx.urlShortner(inviteLink).then(u => this.setState({ inviteLink: u.url, utlShorting: false }), () => this.setState({ utlShorting: false }));
    }
  }

  render() {
    const { classes } = this.props;
    const { inviteLink, utlShorting } = this.state;
    const onShare = () => {
      navigator.share({ title: 'Invitation to Agribuddy', text: 'Welcome to Agribuddy, Click here to join', url: inviteLink });
    };

    return (
      <div>
        <Paper className={classes.root} elevation={1}>
          <Typography variant="subheading" component="h3">
            {AppCtx.locale('InviteLink')}
            {navigator.share && (
              <IconButton color="primary" aria-label="Share" onClick={onShare}>
                <AppCtx.icons.share />
              </IconButton>
            )}
          </Typography>
          {utlShorting && <LinearProgress color="primary" />}
          <Typography component="span" className={classes.link}>
            {inviteLink}
          </Typography>

        </Paper>
      </div>
    );
  }
}

InviteLink.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InviteLink);
