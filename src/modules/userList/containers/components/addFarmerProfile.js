import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AppCtx from 'AppContext';


class RequestProfile extends React.Component {
  render() {
    return (
      <Link to="/users/farmerprofile">
        <Button type="button" variant="raised" color="primary">
          <AppCtx.locale text="Add More Details" />
        </Button>
      </Link>
    );
  }
}
export default (RequestProfile);
