import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import Create from '@material-ui/icons/Create';
import View from '@material-ui/icons/Forward';
import IconButton from '@material-ui/core/IconButton';
import {
  AccountCircle as UserIcon,
  Mail as EamilIcon,
  Phone as PhoneIcon,
  Search as SearchIcon,
} from '@material-ui/icons';
import AppCtx from 'AppContext';
import {
  Card, CardContent, Paper, Typography, Grid, TextField,
} from '@material-ui/core';
import styles from './styles';
import { getUserType } from '../../../userTypes';

const { phoneFormatter } = AppCtx.formUtils;
class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  componentDidMount() {
    // this.props.userList.updateUserList();
  }

  render() {
    const { classes, userList, filter } = this.props;
    const userListToRender = _.sortBy(userList.userList, 'agriId').filter((u) => {
      if (!filter) return true;
      const fTerms = {
        mentors: 'MENTOR', buddies: 'BUDDY', farmers: 'FARMER', supervisors: 'SUPERVISOR',
      };
      return u.roles.includes(fTerms[filter]);
    });
    return (
      <Paper className={classes.paper}>
        <Card className={classes.base}>
          <CardContent>
            <Typography variant="headline" component="h3">
              {AppCtx.locale('User List')}
            </Typography>
            <Grid container spacing={8} alignItems="flex-end">
              <Grid item>
                <SearchIcon />
              </Grid>
              <Grid item>
                <TextField onChange={(e) => { userList.filter(e.target.value); }} label={<AppCtx.locale text="Search" />} />
              </Grid>
            </Grid>
            <Grid container spacing={8}>
              {userListToRender.map(u => (
                <Grid item className={classes.usercard} key={u.agriId}>
                  <Card key={`user-${u.agriId}`}>
                    <CardContent className={classes.content}>
                      <Grid wrap="nowrap" container spacing={8}>
                        <Grid item>
                          <UserIcon style={{ fontSize: 50, color: '#666' }} />
                        </Grid>
                        <Grid item style={{ flexGrow: 1 }}>
                          <Typography variant="subheading" component="h3">
                            {`${u.firstname} ${u.lastname}`}
                          </Typography>
                          <Typography variant="caption">
                            {`(${u.agriId || 'Pending'}) [${getUserType(u.roles)}]`}
                          </Typography>
                          <Typography variant="body1" component="p">
                            <EamilIcon style={{ fontSize: 18, color: '#666' }} />
                            {` ${u.email || ''}`}
                          </Typography>
                          <Typography variant="body1" component="p">
                            <PhoneIcon style={{ fontSize: 18, color: '#666' }} />
                            {` ${phoneFormatter(u.username)}`}
                          </Typography>

                        </Grid>
                        {u.agriId && (
                          <Grid item>
                            <Link to={`/users/${u.agriId}`}>
                              <IconButton>
                                {u.roles.includes('SUPERVISOR') ? <View /> : <Create />}

                              </IconButton>
                            </Link>
                          </Grid>
                        )}
                      </Grid>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
              { userList.userList === null
            && (
              <Grid className="emptyMessage" container spacing={8}>
                <Grid item>
                  <AppCtx.icons.loading />
                </Grid>
              </Grid>
            )}
              { userList.userList && (!userListToRender.length)
            && (
              <Grid className="emptyMessage" container spacing={8}>
                <Grid item>
                  <AppCtx.icons.clear />
                </Grid>
                <Grid item>
                  <Typography variant="subheading" component="h3">{AppCtx.locale('No Users')}</Typography>
                </Grid>
              </Grid>
            )}
            </Grid>
          </CardContent>
        </Card>
      </Paper>
    );
  }
}

export default withStyles(styles)(UserList);
