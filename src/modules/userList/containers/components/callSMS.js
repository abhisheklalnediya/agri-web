import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppCtx from 'AppContext';
import IconButton from '@material-ui/core/IconButton';
import orange from '@material-ui/core/colors/orange';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  link: {
    color: orange[800],
  },
});

class CallBtn extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { phonenumber } = this.props;
    const onCall = () => {
      window.open(`tel:+91${phonenumber}`);
    };
    const onSms = () => {
      window.open(`sms:+91${phonenumber}`);
    };

    return (
      <React.Fragment>
        {navigator.share && (phonenumber && !Number.isNaN(Number(phonenumber))) && (
          <React.Fragment>
            <IconButton color="primary" aria-label="Share" onClick={onCall}>
              <AppCtx.icons.phone />
            </IconButton>
            <IconButton color="primary" aria-label="Share" onClick={onSms}>
              <AppCtx.icons.sms />
            </IconButton>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(CallBtn);
