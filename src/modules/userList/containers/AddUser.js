import React from 'react';
import AppCtx from 'AppContext';
import AddUser from './components/addUser';

export default () => (
  <AppCtx.Context.user.Consumer>
    { auth => (
      <AppCtx.Context.userList.Consumer>
        { userList => <AddUser user={auth} userList={userList} /> }
      </AppCtx.Context.userList.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
