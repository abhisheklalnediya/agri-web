import React from 'react';
import AppCtx from 'AppContext';
import UserList from './components/userList';

export default ({ match }) => (
  <AppCtx.Context.user.Consumer>
    { auth => (
      <AppCtx.Context.userList.Consumer>
        { userList => (
          <UserList filter={match.params.filter} user={auth} userList={userList} />
        ) }
      </AppCtx.Context.userList.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
