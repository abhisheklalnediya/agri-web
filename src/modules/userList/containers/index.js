import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AppCtx from 'AppContext';
import AddUser from './AddUser';
import EditUser from './EditUser';
import UserList from './UserList';
import FarmerProfile from '../../user/containers/FarmerProfile';

function ProfileContainer({ match }) {
  return (
    <AppCtx.Context.userList.Provider>
      <Switch>
        <Route
          path={`${match.url}/add`}
          exact
          render={rprops => (
            <UserList {...rprops} />
          )}
        />
        <Route
          path={`${match.url}/new`}
          exact
          render={rprops => (
            <AddUser {...rprops} />
          )}
        />
        <Route
          path={`${match.url}/farmerprofile`}
          exact
          render={rprops => (
            <FarmerProfile {...rprops} />
          )}
        />
        <Route
          path={`${match.url}/:agriId`}
          exact
          render={rprops => (
            <EditUser {...rprops} />
          )}
        />
        <Route
          path={`${match.url}/filter/:filter`}
          exact
          render={rprops => (
            <UserList {...rprops} />
          )}
        />
        <Route
          path={`${match.url}`}
          exact
          render={rprops => (
            <UserList {...rprops} />
          )}
        />
      </Switch>
    </AppCtx.Context.userList.Provider>
  );
}

export default ProfileContainer;
