import React from 'react';
import AppCtx from 'AppContext';
import RequestProfile from './components/requestprofile';

export default () => (
  <AppCtx.Context.user.Consumer>
    { auth => (
      <AppCtx.Context.locale.Consumer>
        {locale => (
          <RequestProfile user={auth} locale={locale} />
        )}
      </AppCtx.Context.locale.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
