import React from 'react';
import AppCtx from 'AppContext';
import RequestInvite from './components/requestinvite';

export default () => (
  <AppCtx.Context.user.Consumer>
    { auth => (
      <AppCtx.Context.locale.Consumer>
        {({ strings }) => (
          <AppCtx.Context.userList.Consumer>
            { userList => <RequestInvite user={auth} userList={userList} strings={strings} /> }
          </AppCtx.Context.userList.Consumer>
        )}
      </AppCtx.Context.locale.Consumer>
    )}
  </AppCtx.Context.user.Consumer>
);
