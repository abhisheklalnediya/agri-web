import React from 'react';
import AppCtx from 'AppContext';
import EditUser from './components/editUser';

export default props => (
  <div>
    <AppCtx.Context.app.Consumer>
      { app => (
        <AppCtx.Context.user.Consumer>
          { auth => (
            <AppCtx.Context.userList.Consumer>
              { userList => <EditUser auth={auth} app={app} userList={userList} {...props} /> }
            </AppCtx.Context.userList.Consumer>
          )}
        </AppCtx.Context.user.Consumer>
      )}
    </AppCtx.Context.app.Consumer>
  </div>
);
