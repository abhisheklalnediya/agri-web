import _ from 'lodash';
import AppCtx from 'AppContext';

export const UserTypes = [
  {
    rank: 1,
    value: 'ADMIN',
    title: 'Admin',
    creatableBy: ['ADMIN'],
    deletableBy: ['ADMIN'],
  },
  {
    rank: 2,
    value: 'COUNTRY_HEAD',
    title: 'Country head',
    creatableBy: ['ADMIN'],
    deletableBy: ['ADMIN'],
  },
  {
    rank: 3,
    value: 'REGIONAL_MANAGER',
    title: 'Regional manager',
    creatableBy: ['ADMIN', 'COUNTRY_HEAD'],
    deletableBy: ['ADMIN', 'COUNTRY_HEAD'],
  },
  {
    rank: 4,
    value: 'MENTOR',
    title: 'Mentor',
    creatableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
    deletableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
  },
  {
    rank: 5,
    value: 'BUDDY',
    title: 'Buddy',
    creatableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR'],
    deletableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
  },
  {
    rank: 6,
    value: 'FARMER',
    title: 'Farmer',
    creatableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR', 'BUDDY'],
    deletableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
  },
  {
    rank: 5,
    value: 'SUPPLIER',
    title: 'Supplier',
    creatableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER', 'MENTOR'],
    deletableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
  },
  {
    rank: 5,
    value: 'BUYER',
    title: 'Buyer',
    creatableBy: ['ADMIN', 'COUNTRY_HEAD'],
    deletableBy: ['ADMIN', 'COUNTRY_HEAD', 'REGIONAL_MANAGER'],
  },
  {
    rank: 5,
    value: 'CSR',
    title: 'Customer Service Reps',
    creatableBy: ['ADMIN'],
    deletableBy: ['ADMIN'],
  },
];

export function getUserType(roles) {
  const titles = UserTypes.filter(ut => roles.includes(ut.value)).map(x => AppCtx.locale(x.title));
  return titles.join();
}

export function getCreatableUserTypes(roles) {
  return _.filter(UserTypes, o => roles.filter(r => o.creatableBy.indexOf(r.toUpperCase()) !== -1).length).map(x => ({ ...x, title: AppCtx.locale(x.title) }));
}

export function isDeletable(myRoles, roles) {
  const highestRole = roles.map(x => (_.find(UserTypes, { value: x }))).reduce((a, c) => ((a.rank < c.rank) ? a : c));
  const { deletableBy } = highestRole;
  return myRoles.filter(x => (deletableBy.indexOf(x) > -1)).length;
}
