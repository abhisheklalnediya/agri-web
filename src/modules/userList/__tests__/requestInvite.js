import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TestComponent from '../containers/components/requestinvite';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing Noprops', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MemoryRouter>
      <TestComponent />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing  all props', () => {
  const div = document.createElement('div');
  const props = {
    inviteLink: 'asdasdasdasd',
    onRequest: () => null,
  };
  ReactDOM.render(
    <MemoryRouter>
      <TestComponent {...props} />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

describe('Test Request Invite Button component', () => {
  it('Test click event', () => {
    const mockCallBack = jest.fn();
    mockCallBack.mockReturnValue(Promise.resolve({ body: { code: 11 } }));
    const props = {
      inviteLink: null,
      onRequest: mockCallBack,
    };
    const button = shallow((<TestComponent {...props} />));
    button.simulate('click');
    expect(mockCallBack.mock.calls.length).toEqual(1);
  });
});
