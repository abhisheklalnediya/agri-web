import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import TestComponent from '../containers/components/addUser';


it('renders without crashing', () => {
  const div = document.createElement('div');
  const props = {
    classes: {},
    user: {
      roles: ['ADMIN'],
    },
    userList: { addUser: () => null },
  };

  ReactDOM.render(
    <MemoryRouter>
      <TestComponent {...props} />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});
