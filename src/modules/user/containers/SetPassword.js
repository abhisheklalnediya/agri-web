import React from 'react';
import AppCtx from 'AppContext';
import SetPassword from './components/setpassword';

export default () => (
  <AppCtx.Context.user.Consumer>
  { auth => (
    <AppCtx.Context.locale.Consumer>
      {locale => (
        <SetPassword user={auth} locale={locale} />
      )}
    </AppCtx.Context.locale.Consumer>
  )}
</AppCtx.Context.user.Consumer>
);

