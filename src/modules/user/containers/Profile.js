import React from 'react';
import AppCtx from 'AppContext';
import Profile from './components/profile';

class ProfileContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <AppCtx.Context.user.Consumer>
        { user => (
          <AppCtx.Context.locale.Consumer>
            {locale => (
              <Profile user={user} locale={locale} history={this.props.history}/>
            )}
          </AppCtx.Context.locale.Consumer>
        )}
      </AppCtx.Context.user.Consumer>

    );
  }
}
export default ProfileContainer;
