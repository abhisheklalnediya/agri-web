import React from 'react';
import AppCtx from 'AppContext';
import ChangePassword from './components/changepassword';

export default () => (
  <AppCtx.Context.user.Consumer>
    { auth => <ChangePassword user={auth} /> }
  </AppCtx.Context.user.Consumer>
);
