import React from 'react';
import AppCtx from 'AppContext';
import FarmerProfile from './components/farmerprofile';

class FarmerProfileContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <AppCtx.Context.user.Consumer>
        { user => (
          <AppCtx.Context.locale.Consumer>
            {locale => (
              <FarmerProfile user={user} locale={locale} />
            )}
          </AppCtx.Context.locale.Consumer>
        )}
      </AppCtx.Context.user.Consumer>

    );
  }
}
export default FarmerProfileContainer;
