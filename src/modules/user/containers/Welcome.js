import React, { Component } from 'react';
import AppCtx from 'AppContext';

import Welcome from './components/welcome';

class WelcomeContainer extends Component {
  render() {
    return (
      <AppCtx.Context.user.Consumer>
        { auth => (
          <AppCtx.Context.locale.Consumer>
            {locale => (
              <Welcome auth={auth} locale={locale} />
            )}
          </AppCtx.Context.locale.Consumer>
        )}
      </AppCtx.Context.user.Consumer>
    );
  }
}

export default WelcomeContainer;
