import React from 'react';
import AppCtx from 'AppContext';
import MentorStatus from './components/mentorstatus';

export default () => (
  <div>
    <AppCtx.Context.user.Consumer>
      { auth => (
        <h2>
          {`${AppCtx.locale('welcome')} ${auth.firstname} ${auth.lastname} `}
        </h2>
      )}
    </AppCtx.Context.user.Consumer>
    <AppCtx.Context.user.Consumer>
      { user => (
        <AppCtx.Context.userList.Consumer>
          { userList => (
            user.roles.includes('MENTOR') ? <MentorStatus user={user} userList={userList} /> : ''
          )
          }
        </AppCtx.Context.userList.Consumer>
      )
      }
    </AppCtx.Context.user.Consumer>
  </div>
);
