import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppCxt from 'AppContext';
import styles from './setpassword.styles';

class CheckAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  componentDidMount() {
    const {
      user,
    } = this.props;
    user.checkAccount();
  }

  render() {
    const {
      classes,
      user,
      next,
    } = this.props;
    if (!user.forcePasswordReset) {
      if (user.profile === 404) {
        next();
      } else if (user.profile) {
        next(100);
      }
    }
    return (
      <section className={classes.welcome}>
        <div className={classes.container}>
          <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
          <AppCxt.components.Spinner />
        </div>
      </section>
    );
  }
}

export default withStyles(styles)(CheckAccount);
