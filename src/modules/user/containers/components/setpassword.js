import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Typography, Grid,
  FormGroup, Paper,
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import styles from './setpassword.styles';


const { required } = AppCtx.formUtils;
const { TextField } = AppCtx.components;

const ShowSuccess = withStyles(styles)(({ classes, next }) => {
  function eNext(e) {
    e.preventDefault();
    next(1);
  }
  return (
    <div className={classes.subcontainer}>
      <Typography variant="headline" component="h3">
        <AppCtx.locale text="done" />
      </Typography>
      <Button onClick={eNext} type="button" variant="raised" color="primary">
        <AppCtx.locale text="next" />
      </Button>
    </div>
  );
});

const newpasswordForm = ({
  handleSubmit,
  classes,
  submitting,
  submitSucceeded,
  hasValidationErrors,
  submitErrors,
  next,
}) => (
  <form onSubmit={handleSubmit} className={classes.loginForm}>
    <div className={classes.container}>
      <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
      <Paper className={classes.paper}>
        {(submitSucceeded && <ShowSuccess next={next} />)
        || (
          <div>
            <Typography variant="headline" component="h3" className="heading">
              <AppCtx.locale text="SetPassword" />
            </Typography>

            <FormGroup row>
              <Field
                name="password"
                component={TextField}
                type="password"
                label={<AppCtx.locale text="NewPassword" />}
                validate={required}
              />
              <Field
                name="newpassword"
                component={TextField}
                type="password"
                label={<AppCtx.locale text="NewPasswordAgain" />}
                validate={required}
              />
              {submitErrors && (
                <Grid container alignItems="flex-end" direction="row" spacing={8}>
                  <Grid item xs={12}>
                    <AppCtx.components.SnackBar message={AppCtx.locale('Failed to set password')} type="error" />
                  </Grid>
                </Grid>
              )}
            </FormGroup>

            <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
              <Grid item>
                <Button type="submit" variant="raised" color="primary" disabled={submitting || hasValidationErrors}>
                  <AppCtx.locale text="Submit" />
                </Button>
              </Grid>
            </Grid>
          </div>
        )}
      </Paper>
    </div>
  </form>
);


function SetPassword(props) {
  const {
    classes,
    user,
    next,
  } = props;
  return (
    <section className={classes.sectionViewProfile}>
      <Form
        onSubmit={user.setPassword}
        validate={(values) => {
          const errors = {};
          if (!values.password) {
            errors.password = <AppCtx.locale text="Required" />;
          }
          if (!values.newpassword) {
            errors.newpassword = <AppCtx.locale text="Required" />;
          } else if (values.newpassword !== values.password) {
            errors.newpassword = <AppCtx.locale text="PasswordMismatched" />;
          }
          if (values.newpassword !== values.password) {
            errors.newpassword = <AppCtx.locale text="Passwords are not same" />;
          }
          return errors;
        }}
        render={newpasswordForm}
        classes={classes}
        initialValues={user}
        next={next}
      />
    </section>
  );
}

export default withStyles(styles)(SetPassword);
