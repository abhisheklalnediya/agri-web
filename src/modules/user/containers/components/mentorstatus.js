import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import AppCtx from 'AppContext';
import styles from './styles';

function MentorStatus(props) {
  const { classes } = props;

  return (
    <div className={classes.mentorstatus}>
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <Typography className={classes.title} gutterBottom>
            {AppCtx.locale('BuddyCount')}
          </Typography>
          <Typography component="h2">
           20
          </Typography>
        </CardContent>
      </Card>
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <Typography className={classes.title} gutterBottom>
            {AppCtx.locale('FarmersCount')}
          </Typography>
          <Typography component="h2">
           20
          </Typography>
        </CardContent>
      </Card>
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <Typography className={classes.title} gutterBottom>
            {AppCtx.locale('FarmsCount')}
          </Typography>
          <Typography component="h2">
           20
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}

MentorStatus.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MentorStatus);
