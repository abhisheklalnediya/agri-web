export default () => ({
  sectionViewProfile: {
    '& .heading': {
      marginBottom: 20,
      marginTop: 10,
    },
  },
  dropZone: {
    // alignItems: 'flex-end',
    // backgroundColor: '#666',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    border: '1px solid #BBB',
    boxShadow: '0px 0px 20px #DDD',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: 5,
    minHeight: 200,
    minWidth: 200,
    cursor: 'pointer',
  },
  buttonContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  previewContainer: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
  },
  nodata: {
    padding: 20,
  },
  paper: {
    display: 'block',
    margin: 10,
  },
  select: {
    width: '100%',
    marginTop: 9,
  },
  progress: {
    background: 'rgba(255, 255, 255, 0.54);',
    borderRadius: '50%',
    padding: 2,
  },
  input: {
    display: 'none',
  },
  radiobuttons: {
    listStyleType: 'none',
  },
  uploadbutton: {
    listStyleType: 'none',
  },
  photostatus: {
    display: 'flex',
  },
  button: {
    marginBottom: 15,
    width: '100%',
    minWidth: '30%',
  },
  buttonupload: {
    maxWidth: '50%',
  },
  preview: {
    display: 'inline-block',
    height: '82px',
    width: '82px',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundColor: '#DDD',
    borderRadius: '4px',
    border: '1px solid #a7a7a7',
    margin: '3px',
  },
});
