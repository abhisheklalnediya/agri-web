import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Paper,
  Button,
} from '@material-ui/core';
import AppCtx from 'AppContext';
import styles from './setpassword.styles';
import SelectLanguage from './selectLanguage';

function SetPassword(props) {
  const {
    classes,
    user,
    next,
    locale,
  } = props;
  return (
    <section className={classes.welcome}>
      <div className={classes.container}>
        <div className={classes.select}>
          <SelectLanguage locale={locale} />
        </div>
        <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
        <Paper className={classes.paper}>
          <Typography variant="headline" component="h3" className="heading">
            <AppCtx.locale text="welcome" />
            {` ${user.firstname} ${user.lastname}`}
          </Typography>
          <Button type="button" variant="raised" color="primary" onClick={() => { next(1); }}>
            <AppCtx.locale text="next" />
          </Button>
        </Paper>
      </div>
    </section>
  );
}

export default withStyles(styles)(SetPassword);
