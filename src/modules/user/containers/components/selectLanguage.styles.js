export default () => ({
  langSelect: {
    top: '72px',
    float: 'right',
    width: '100%',
    display: 'flex',
    position: 'absolute',
    maxWidth: '120px',
    right: '10px',
  },
});
