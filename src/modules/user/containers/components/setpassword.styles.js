export default theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // minHeight: 540,
    height: window.innerHeight,
    background: theme.palette.common.primary,
    flexDirection: 'column',
  },
  subcontainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  logo: {
    width: 270,
    marginBottom: 20,
  },
  otp: {
    marginTop: 5,
    marginLeft: 4,
  },
  otpfield: {
    width: '100%',
    marginLeft: 0,
  },
  welcome: {
    textAlign: 'center',
  },
  select: {
    top: 38,
    position: 'absolute',
    right: 32,
  },
  paper: {
    position: 'relative',
    padding: 20,
    overflow: 'auto',
    [theme.breakpoints.up('md')]: {
      minWidth: 300,
      maxWidth: '50%',
    },
    [theme.breakpoints.down('md')]: {
      boxSizing: 'border-box',
      width: '90%',
      // minHeight: 250,
    },
    '& .heading': {
      marginBottom: 20,
      '& img': {
        width: '100%',
        height: 'auto',
        marginBottom: 10,
      },
    },
  },
  loginForm: {
    '& .actions': {
      display: 'flex',
      flexDirection: 'column-reverse',
    },
    '& .buttons': {
      flexGrow: 1,
      alignItems: 'center',
      height: 48,
      display: 'block',
      margin: '0 auto',
    },
    '& button': {
      marginTop: 10,
    },
  },
});
