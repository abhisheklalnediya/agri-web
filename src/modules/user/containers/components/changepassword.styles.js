export default theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 540,
    height: window.innerHeight,
    background: theme.palette.common.primary,
    flexDirection: 'column',
  },
  logo: {
    width: 270,
    marginBottom: 20,
  },
  version: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    marginBottom: 0,
    fontSize: '75%',
    color: '#ccc',
  },
  paper: {
    position: 'relative',
    padding: 20,
    overflow: 'auto',
    [theme.breakpoints.up('md')]: {
      minWidth: 300,
      maxWidth: '50%',
    },
    [theme.breakpoints.down('md')]: {
      boxSizing: 'border-box',
      width: '90%',
      minHeight: 250,
    },
    '& .heading': {
      marginBottom: 20,
      '& img': {
        width: '100%',
        height: 'auto',
        marginBottom: 10,
      },
    },
  },
  loginForm: {
    '& >div': {
      marginBottom: 10,
    },
    '& >div >div': {
      height: 65,
      display: 'flex',
    },

    '& .actions': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      textTransform: 'initial',
      textDecoration: 'underline',
    },
    '& .buttons': {
      flexGrow: 1,
      alignItems: 'center',
      height: 48,
    },
  },
});
