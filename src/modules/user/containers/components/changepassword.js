import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Typography, Grid,
  Paper,
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import styles from './changepassword.styles';

const { required, passwordFormatter } = AppCtx.formUtils;
const { TextField } = AppCtx.components;

const updatepassword = ({
  handleSubmit,
  pristine,
  invalid,
  classes,
  form,
}) => (
  <form className={classes.loginForm} onSubmit={handleSubmit}>
    <div>
      <Field
        name="currentpassword"
        component={TextField}
        type="password"
        label={<AppCtx.locale text="CurrentPassword" />}
        validate={required}
      />
    </div>
    <div>
      <Field
        name="newpassword"
        component={TextField}
        type="password"
        label={<AppCtx.locale text="NewPassword" />}
        validate={passwordFormatter}
      />
    </div>
    <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
      <Grid item>
        <Button type="submit" variant="raised" color="primary" disabled={pristine || invalid}>
          <AppCtx.locale text="Submit" />
        </Button>
      </Grid>
      <Grid item>
        <Button type="button" variant="raised" onClick={form.reset}>
          <AppCtx.locale text="Clear" />
        </Button>
      </Grid>
    </Grid>
  </form>
);

function ChangePassword(props) {
  const {
    classes,
    user,
  } = props;
  return (
    <div className="login-container">
      <div className={classes.container}>
        <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
        <Paper className={classes.paper}>
          <div>
            <Typography variant="headline" component="h3" className="heading">
              <AppCtx.locale text="ChangePassword" />
            </Typography>
            <Form
              onSubmit={user.updateProfile}
              validate={(values) => {
                const errors = {};
                if (!values.newpassword) {
                  errors.newpassword = <AppCtx.locale text="Required" />;
                }
                if (!values.currentpassword) {
                  errors.currentpassword = <AppCtx.locale text="Required" />;
                } else if (values.currentpassword !== values.newpassword) {
                  errors.newpassword = <AppCtx.locale text="PasswordMismatched" />;
                }
                return errors;
              }}
              render={updatepassword}
              classes={classes}
            />
          </div>
        </Paper>
      </div>
    </div>
  );
}
export default withStyles(styles)(ChangePassword);
