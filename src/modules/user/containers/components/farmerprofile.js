import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Typography, FormLabel, Grid,
  FormGroup, FormControl, Paper, Card,
  IconButton, CircularProgress,
} from '@material-ui/core';

import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import EditPP from '@material-ui/icons/InsertPhoto';
import SavePP from '@material-ui/icons/Check';
import ClearPP from '@material-ui/icons/Close';
import SelectLanguage from './selectLanguage';
import styles from './styles';


const { TextField, Radio } = AppCtx.components;
const {
  dateFormatter, dateParser, required,
} = AppCtx.formUtils;


const farmerprofileForm = ({
  handleSubmit,
  invalid,
  classes,
  form,
  locale,
  submitSucceeded,
  user,
  values,
}) => (
  <form onSubmit={handleSubmit} className={classes.formViewProfile}>
    <Typography variant="subheading" component="h3" className="heading">
      <AppCtx.locale text="Basic Details" />
    </Typography>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={8}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <SelectLanguage locale={locale} />
          </Grid>
          <Grid item xs={4}>
            <Field name="buddy name" label={<AppCtx.locale text="buddy name" />} component={TextField} type="text" placeholder="" required />
          </Grid>
          <Grid item xs={4}>
            <Field name="username" label={<AppCtx.locale text="buddy phone" />} component={TextField} type="text" placeholder="" required mask={(!Number.isNaN(Number(values.username))) ? 'phone' : ''} disabled />
          </Grid>
          <Grid item xs={4}>
            <Field name="buddy id" label={<AppCtx.locale text="buddy id" />} component={TextField} type="text" placeholder="" required />
          </Grid>
          <Grid item xs={4}>
            <Field name="farmer name" label={<AppCtx.locale text="farmer name in khmer" />} component={TextField} type="text" placeholder="" validate={required} />
          </Grid>
          <Grid item xs={4}>
            <Field name="farmername" label={<AppCtx.locale text="farmer name in english" />} component={TextField} type="text" placeholder="" validate={required} />
          </Grid>
          <Grid item xs={4}>
            <Field name="mobilephone" label={<AppCtx.locale text="farmer phone" />} component={TextField} type="text" placeholder="" required mask={(!Number.isNaN(Number(values.username))) ? 'phone' : ''} />
          </Grid>
          <Grid item xs={4}>
            <Field name="Family Number" label={<AppCtx.locale text="Family Number" />} component={TextField} type="number" placeholder="" validate={required} />
          </Grid>
          <Grid item xs={4}>
            <Field name="MaleNumber" label={<AppCtx.locale text="Male Number" />} component={TextField} type="number" placeholder="" validate={required} />
          </Grid>
          <Grid item xs={4}>
            <Field name="Female Number" label={<AppCtx.locale text="Female Number" />} component={TextField} type="number" placeholder="" validate={required} />
          </Grid>

          <Grid item xs={4}>
            <Field name="villagename" label={<AppCtx.locale text="Current Village Name" />} component={TextField} type="text" placeholder="" validate={required} />
          </Grid>
          <Grid item xs={4}>
            <Field name="District" label={<AppCtx.locale text="District" />} component={TextField} type="text" placeholder="" />
          </Grid>
          <Grid item xs={4}>
            <Field name="Province" label={<AppCtx.locale text="Province" />} component={TextField} type="text" placeholder="" required />
          </Grid>
          <Grid item xs={4}>
            <Field name="startDate" label={<AppCtx.locale text="Date of start living" />} component={TextField} format={dateFormatter} parse={dateParser} placeholder="" mask="date" validate={required} />
          </Grid>
          <Grid item xs={4}>
            <Field name="appDate" label={<AppCtx.locale text="Date of App" />} component={TextField} format={dateFormatter} parse={dateParser} placeholder="" mask="date" validate={required} />
          </Grid>
          {/* <Grid item xs={12}>
            <Field name="farms" label={<AppCtx.locale text="farms" />} component={TextField} type="number" placeholder="" />
          </Grid> */}
          <Grid item xs={12}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">
                <AppCtx.locale text="gender" />
              </FormLabel>
              <FormGroup row>
                <Field
                  name="gender"
                  component={Radio}
                  type="radio"
                  value="male"
                  label={<AppCtx.locale text="male" />}
                />
                <Field
                  name="gender"
                  component={Radio}
                  type="radio"
                  value="female"
                  label={<AppCtx.locale text="female" />}
                />
              </FormGroup>
            </FormControl>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={4}>
        <Grid container alignItems="center" direction="column" spacing={24}>
          <Grid item xs={12}>
            <ProfilePic classes={classes} user={user} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
    <Typography variant="subheading" component="h3" className="heading">
      <AppCtx.locale text="Photo Verification Status" />
    </Typography>

    <Grid container spacing={16} className={classes.photostatus}>
      <Grid item xs={12} md={6} className={classes.status}>
        <div>
          <ul className={classes.radiobuttons}>
            <li>
              <div className={classes.inputcontainer}>
                <div className={classes.label}>
                  <Field
                    name="status"
                    component={Radio}
                    type="radio"
                    value="aadhar"
                    label={<AppCtx.locale text="aadhar" />}
                  />
                </div>
                <div>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                  />
                  <FormLabel htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.buttonupload}>
                      <AppCtx.locale text="upload" />
                    </Button>
                  </FormLabel>
                </div>
              </div>
            </li>
            <li>
              <div className={classes.inputcontainer}>
                <div className={classes.label}>
                  <Field
                    name="status"
                    component={Radio}
                    type="radio"
                    value="land record"
                    label={<AppCtx.locale text="land record" />}
                  />
                </div>
                <div>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                  />
                  <FormLabel htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.buttonupload}>
                      <AppCtx.locale text="upload" />
                    </Button>
                  </FormLabel>
                </div>
              </div>
            </li>
            <li>
              <div className={classes.inputcontainer}>
                <div className={classes.label}>
                  <Field
                    name="status"
                    component={Radio}
                    type="radio"
                    value="health card"
                    label={<AppCtx.locale text="soil health card" />}
                  />
                </div>
                <div>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                  />
                  <FormLabel htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.buttonupload}>
                      <AppCtx.locale text="upload" />
                    </Button>
                  </FormLabel>
                </div>
              </div>
            </li>
            <li>
              <div className={classes.inputcontainer}>
                <div className={classes.label}>
                  <Field
                    name="status"
                    component={Radio}
                    type="radio"
                    value="pan card"
                    label={<AppCtx.locale text="pan" />}
                  />
                </div>
                <div>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                  />
                  <FormLabel htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.buttonupload}>
                      <AppCtx.locale text="upload" />
                    </Button>
                  </FormLabel>
                </div>
              </div>
            </li>
            <li>
              <div className={classes.inputcontainer}>
                <div className={classes.label}>
                  <Field
                    name="status"
                    component={Radio}
                    type="radio"
                    value="farmer picture"
                    label={<AppCtx.locale text="farmer picture" />}
                  />
                </div>
                <div>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                  />
                  <FormLabel htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.buttonupload}>
                      <AppCtx.locale text="upload" />
                    </Button>
                  </FormLabel>
                </div>
              </div>
            </li>
            <li>
              <div className={classes.inputcontainer}>
                <div className={classes.label}>
                  <Field
                    name="status"
                    component={Radio}
                    type="radio"
                    value="other picture"
                    label={<AppCtx.locale text="other picture" />}
                  />
                </div>
                <div>
                  <input
                    accept="image/*"
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                  />
                  <FormLabel htmlFor="contained-button-file">
                    <Button variant="contained" component="span" className={classes.buttonupload}>
                      <AppCtx.locale text="upload" />
                    </Button>
                  </FormLabel>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </Grid>
    </Grid>
    {submitSucceeded && navigator.onLine && (
      <Grid container alignItems="flex-end" direction="row" spacing={8}>
        <Grid item xs={12}>
          <AppCtx.components.SnackBar message={AppCtx.locale('User Updated')} type="success" />
        </Grid>
      </Grid>
    )}
    <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
      <Grid item>
        <Button type="submit" variant="raised" color="primary" disabled={invalid}>
          <AppCtx.locale text="update" />
        </Button>
      </Grid>
    </Grid>
  </form>
);

function validate(values, user) {
  console.log(values, user);
  const errors = {};
  if (values.mobilephone && values.mobilephone.includes('_')) errors.mobilephone = <AppCtx.locale text="phonenumber is invalid" />;
  if (values.startDate && values.startDate.includes('_')) errors.startDate = <AppCtx.locale text="startdate is invalid" />;
  if (values.appDate && values.appDate.includes('_')) errors.appDate = <AppCtx.locale text="appdate is invalid" />;
  if (!values.profilePic) errors.profilePic = <AppCtx.locale text="please upload profile picture" />;
  if (_.isEmpty(errors) && values.pincode) {
    return new Promise((resolve) => {
      user.setAddressFromPin(values).then(() => {
        resolve(null);
      }, () => {
        errors.pincode = <AppCtx.locale text="pincode is invalid" />;
        resolve(errors);
      });
    });
  }
  return errors;
}

class ProfilePic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newPP: null,
    };
    this.uploadPic = this.uploadPic.bind(this);
  }

  uploadPic() {
    const { user } = this.props;
    this.setState({ progress: true }, () => user.uploadProfilePic(this.state.newPP).then(
      () => this.setState({ progress: false, newPP: null }),
      () => this.setState({ progress: false }),
    ));
  }

  render() {
    const { user, classes } = this.props;
    const { newPP, progress } = this.state;
    let dropzoneRef;
    const { profilePic } = user;
    const proPicUrl = (newPP && newPP[0].preview)
      || (profilePic && encodeURI(`/api/documents/${profilePic.container}/download/${profilePic.filename}`))
      || '/static/images/user-dummy.jpg';

    const onDrop = (acceptedFiles) => {
      this.setState({ newPP: acceptedFiles });
    };
    return (
      <Dropzone accept="image/jpeg, image/png" style={{ backgroundImage: `url(${proPicUrl})` }} className={classes.profilePic} disableClick multiple={false} maxSize={10000000} ref={(node) => { dropzoneRef = node; }} onDrop={onDrop}>
        {!newPP && (
          <IconButton onClick={() => { dropzoneRef.open(); }}>
            <EditPP />
          </IconButton>
        )}
        {!progress && newPP && (
          <IconButton onClick={() => this.setState({ newPP: null })}>
            <ClearPP />
          </IconButton>
        )}
        {!progress && newPP && (
          <IconButton onClick={this.uploadPic}>
            <SavePP />
          </IconButton>
        )}
        {progress && <CircularProgress className={classes.progress} />}
      </Dropzone>
    );
  }
}


const FarmerProfile = (props) => {
  const {
    classes,
    user,
    locale,
  } = props;

  return (
    <section className={classes.sectionViewProfile}>
      <Paper className={classes.paper}>
        <Card className={classes.nodata}>
          <Typography variant="headline" component="h3" className="heading">
            <AppCtx.locale text="farmerprofile" />
          </Typography>
          <Form
            validate={v => validate(v, user)}
            onSubmit={user.updateProfile}
            render={farmerprofileForm}
            classes={classes}
            initialValues={user}
            locale={locale}
            setAddressFromPin={user.setAddressFromPin}
            user={user}
          />
        </Card>
      </Paper>
    </section>
  );
};

export default withStyles(styles)(FarmerProfile);
