import React from 'react';
import {
  Select, MenuItem,
} from '@material-ui/core';


const langItems = [
  { title: 'Hindi', value: 'hi', lang: 'Hindi' },
  { title: 'English', value: 'en', lang: 'English' },
];

const LangForm = ({
  locale,
}) => {
  function handleChange(event) {
    locale.setLanguage(event.target.value);
  }
  return (
    <Select
      value={locale.lang}
      onChange={handleChange}
    >
      {langItems.map(category => (
        <MenuItem key={category.value} value={category.value}>
          {category.title}
        </MenuItem>
      ))}
    </Select>
  );
};
export default (LangForm);
