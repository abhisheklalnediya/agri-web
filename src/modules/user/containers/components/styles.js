export default theme => ({
  sectionViewProfile: {

  },
  singleContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 540,
    marginTop: 10,
    background: theme.palette.common.primary,
    flexDirection: 'column',
  },
  profilePic: {
    alignItems: 'flex-end',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    border: '1px solid #BBB',
    boxShadow: '0px 0px 20px #DDD',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 5,
    height: 200,
    width: 200,
    borderRadius: '50%',
    '& button': {
      background: 'rgba(255, 255, 255, 0.54);',
    },
    '& button:hover': {
      background: 'rgba(255, 255, 255, 0.74);',
    },
  },
  nodata: {
    padding: 20,
  },
  subheading: {
    marginBottom: 20,
    marginTop: 10,
  },
  paper: {
    display: 'block',
    margin: 10,
  },
  select: {
    width: '100%',
    marginTop: 9,
  },
  progress: {
    background: 'rgba(255, 255, 255, 0.54);',
    borderRadius: '50%',
    padding: 2,
  },
  input: {
    display: 'none',
  },
  radiobuttons: {
    listStyleType: 'none',
  },
  uploadbutton: {
    listStyleType: 'none',
  },
  photostatus: {
    display: 'flex',
  },
  button: {
    marginBottom: 15,
    width: '100%',
    minWidth: '30%',
  },
  status: {
    maxWidth: '50%',
  },
  buttonupload: {
    maxWidth: '50%',
  },
  card: {
    width: '40%',
    marginBottom: 26,
    marginLeft: 12,
  },
  title: {
    fontSize: 15,
  },
  content: {
    background: '#f0f0f0',
  },
  mentorstatus: {
    display: 'flex',
    marginRight: 12,
  },
  label: {
    minWidth: 175,
  },
  inputcontainer: {
    display: 'flex',
  },
});
