import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Button, Typography, FormLabel, Grid,
  FormGroup, FormControl, Paper, Card,
  IconButton, CircularProgress,
} from '@material-ui/core';

import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import EditPP from '@material-ui/icons/InsertPhoto';
import SavePP from '@material-ui/icons/Check';
import ClearPP from '@material-ui/icons/Close';
import SelectLanguage from './selectLanguage';
import styles from './styles';

const { TextField, Radio } = AppCtx.components;
const {
  dateFormatter, dateParser, required, nameFormatter,
} = AppCtx.formUtils;

const profileForm = ({
  handleSubmit,
  invalid,
  classes,
  form,
  locale,
  submitSucceeded,
  user,
  values,
  submitting,
  ...rest
}) => (
  <form onSubmit={handleSubmit} className={classes.formViewProfile}>
    {console.log(rest)}
    <Typography variant="subheading" component="h3" className={classes.subheading}>
      <AppCtx.locale text="Basic Details" />
    </Typography>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={8}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <SelectLanguage locale={locale} />
          </Grid>
          <Grid item xs={12}>
            <Field name="username" label={<AppCtx.locale text="mobileNumber" />} component={TextField} type="text" placeholder="" required mask={(!Number.isNaN(Number(values.username))) ? 'phone' : ''} disabled />
          </Grid>
          <Grid item xs={12}>
            <Field name="firstname" label={<AppCtx.locale text="firstname" />} component={TextField} type="text" placeholder="" required validate={nameFormatter} />
          </Grid>
          <Grid item xs={12}>
            <Field name="lastname" label={<AppCtx.locale text="lastname" />} component={TextField} type="text" placeholder="" required validate={nameFormatter} />
          </Grid>
          <Grid item xs={12}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">
                <AppCtx.locale text="gender" />
              </FormLabel>
              <FormGroup row>
                <Field
                  name="gender"
                  component={Radio}
                  type="radio"
                  value="male"
                  label={<AppCtx.locale text="male" />}
                  required
                />
                <Field
                  name="gender"
                  component={Radio}
                  type="radio"
                  value="female"
                  label={<AppCtx.locale text="female" />}
                />
              </FormGroup>
            </FormControl>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={4}>
        <Grid container alignItems="center" direction="column" spacing={24}>
          <Grid item xs={12}>
            <ProfilePic classes={classes} user={user} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
    <Typography variant="subheading" component="h3" className={classes.subheading}>
      <AppCtx.locale text="Personal Details" />
    </Typography>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={6}>
        <Field name="nationalId" label={<AppCtx.locale text="National ID" />} component={TextField} type="tel" placeholder="" mask="aadhar" validate={required} />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="panNumber" label={<AppCtx.locale text="Permanent Account Number(PAN)" />} format={x => (x ? x.toUpperCase() : '')} parse={x => (x ? x.toUpperCase() : '')} component={TextField} type="text" placeholder="" mask="pan" />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="dateOfBirth" label={<AppCtx.locale text="Date of Birth" />} component={TextField} format={dateFormatter} parse={dateParser} type="tel" placeholder="" mask="date" validate={required} />
      </Grid>
    </Grid>
    <Typography variant="subheading" component="h3" className={classes.subheading}>
      <AppCtx.locale text="Address Details" />
    </Typography>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={6}>
        <Field name="housename" label={<AppCtx.locale text="House Name" />} component={TextField} type="text" placeholder="" />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="locality" label={<AppCtx.locale text="Locality" />} component={TextField} type="text" placeholder="" />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="pincode" label={<AppCtx.locale text="Pincode" />} component={TextField} type="tel" placeholder="" validate={required} mask="pincode" />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="postoffice" label={<AppCtx.locale text="Post Office" />} component={TextField} type="text" placeholder="" disabled />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="district" label={<AppCtx.locale text="District" />} component={TextField} type="text" placeholder="" disabled />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="state" label={<AppCtx.locale text="State" />} component={TextField} type="text" placeholder="" disabled />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="country" label={<AppCtx.locale text="Country" />} component={TextField} type="text" placeholder="" disabled />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="localityId" component="input" type="hidden" />
      </Grid>
    </Grid>
    <Typography variant="subheading" component="h3" className={classes.subheading}>
      <AppCtx.locale text="Bank Details" />
    </Typography>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={6}>
        <Field name="bankIFSC" label={<AppCtx.locale text="IFSC" />} component={TextField} type="text" placeholder="" validate={required} />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field name="bankAccountNo" label={<AppCtx.locale text="Account Number" />} component={TextField} type="text" placeholder="" validate={required} />
      </Grid>
    </Grid>
    {submitSucceeded && navigator.onLine && (
      <Grid container alignItems="flex-end" direction="row" spacing={8}>
        <Grid item xs={12}>
          <AppCtx.components.SnackBar message={AppCtx.locale('User Updated')} type="success" />
        </Grid>
      </Grid>
    )}
    <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
      <Grid item>
        <Button type="submit" variant="raised" color="primary" disabled={submitting}>
          <AppCtx.locale text="update" />
        </Button>
      </Grid>
    </Grid>
  </form>
);

function validate(values, user) {
  const errors = {};
  if (values.username && values.username.includes('_')) errors.username = <AppCtx.locale text="phonenumber is invalid" />;
  if (values.nationalId && values.nationalId.includes('_')) errors.nationalId = <AppCtx.locale text="nationalId is invalid" />;
  if (values.dateOfBirth && values.dateOfBirth.includes('_')) errors.dateOfBirth = <AppCtx.locale text="dateOfBirth is invalid" />;
  if (values.panNumber && values.panNumber.includes('_')) errors.panNumber = <AppCtx.locale text="panNumber is invalid" />;
  if (values.pincode && String(values.pincode).includes('_')) errors.pincode = <AppCtx.locale text="pincode is invalid" />;
  if (values.newpassword !== values.currentpassword) errors.newpassword = <AppCtx.locale text="PasswordMismatched" />;
  if (_.isEmpty(errors) && values.pincode) {
    return new Promise((resolve) => {
      user.setAddressFromPin(values).then(() => {
        resolve(null);
      }, () => {
        errors.pincode = <AppCtx.locale text="pincode is invalid" />;
        resolve(errors);
      });
    });
  }
  return errors;
}

class ProfilePic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newPP: null,
    };
    this.uploadPic = this.uploadPic.bind(this);
  }

  uploadPic() {
    const { user } = this.props;
    this.setState({ progress: true }, () => user.uploadProfilePic(this.state.newPP).then(
      () => this.setState({ progress: false, newPP: null }),
      () => this.setState({ progress: false }),
    ));
  }

  render() {
    const { user, classes } = this.props;
    const { newPP, progress } = this.state;
    let dropzoneRef;
    const { profilePic } = user;
    const proPicUrl = (newPP && newPP[0].preview)
      || (profilePic && encodeURI(`/api/documents/${profilePic.container}/download/${profilePic.filename}`))
      || '/static/images/user-dummy.jpg';

    const onDrop = (acceptedFiles) => {
      this.setState({ newPP: acceptedFiles });
    };
    return (
      <Dropzone accept="image/jpeg, image/png" style={{ backgroundImage: `url(${proPicUrl})` }} className={classes.profilePic} disableClick multiple={false} maxSize={10000000} ref={(node) => { dropzoneRef = node; }} onDrop={onDrop}>
        {!newPP && (
          <IconButton onClick={() => { dropzoneRef.open(); }}>
            <EditPP />
          </IconButton>
        )}
        {!progress && newPP && (
          <IconButton onClick={() => this.setState({ newPP: null })}>
            <ClearPP />
          </IconButton>
        )}
        {!progress && newPP && (
          <IconButton onClick={this.uploadPic}>
            <SavePP />
          </IconButton>
        )}
        {progress && <CircularProgress className={classes.progress} />}
      </Dropzone>
    );
  }
}


const Profile = (props) => {
  const {
    classes,
    user,
    locale,
  } = props;

  function handleSubmit(d) {
    return user.updateProfile(d);
  }
  const fullScreen = (user.profile) === 404 && user.roles.indexOf('ADMIN') === -1;
  return (
    <section className={fullScreen ? classes.singleContainer : classes.sectionViewProfile}>
      {fullScreen && <AppCtx.components.Logo />}
      <Paper className={classes.paper}>
        <Card className={classes.nodata}>
          <Typography variant="headline" component="h3" className={classes.subheading}>
            <AppCtx.locale text="profile" />
          </Typography>
          <Form
            validate={v => validate(v, user)}
            onSubmit={handleSubmit}
            render={profileForm}
            classes={classes}
            initialValues={user}
            locale={locale}
            setAddressFromPin={user.setAddressFromPin}
            user={user}
          />
        </Card>
      </Paper>
    </section>
  );
};

export default withStyles(styles)(Profile);
