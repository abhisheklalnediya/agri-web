import React from 'react';
import {
  Button, Typography, Paper,
  FormGroup, Grid,
} from '@material-ui/core';
import { Form, Field } from 'react-final-form';
import AppCtx from 'AppContext';
import { withStyles } from '@material-ui/core/styles';
import styles from './setpassword.styles';


const { required } = AppCtx.formUtils;
const { TextField } = AppCtx.components;

const ShowSuccess = withStyles(styles)(({ classes, next }) => {
  function eNext(e) {
    e.preventDefault();
    next(1);
  }
  return (
    <div className={classes.subcontainer}>
      <Typography variant="headline" component="h3">
        <AppCtx.locale text="done" />
      </Typography>
      <Button onClick={eNext} type="button" variant="raised" color="primary">
        <AppCtx.locale text="next" />
      </Button>
    </div>
  );
});

const OtpForm = ({
  handleSubmit,
  classes,
  submitting,
  submitSucceeded,
  hasValidationErrors,
  next,
  submitErrors,
  loader,
}) => (

  <form onSubmit={handleSubmit} className={classes.loginForm}>

    {(submitSucceeded && <ShowSuccess next={next} />)
        || (
          <div>
            <Typography variant="headline" component="h3" className="heading">
              <AppCtx.locale text="Verfiy Phone Number" />
            </Typography>
            <Typography variant="body1" className="heading">
              <AppCtx.locale text="A one time password(OTP) is sent to to your phone as sms. Enter the number below to verify your phone" />
            </Typography>
            <FormGroup row className={classes.otpfield}>
              <Field
                name="token"
                component={TextField}
                label="OTP"
                validate={required}
                type="text"
                inputProps={{autofocus: 'true'}}
                mask="otp"
              />
              {submitErrors && (
                <Grid container alignItems="flex-end" direction="row" className={classes.otp}>
                  <Grid item xs={12}>
                    <AppCtx.components.SnackBar message={AppCtx.locale('Incorrect OTP')} type="error" />
                  </Grid>
                </Grid>
              )}
            </FormGroup>
            <Grid container alignItems="flex-end" direction="row-reverse" spacing={8}>
              <Grid item>
                <Button type="submit" variant="raised" color="primary" disabled={submitting || hasValidationErrors || loader}>
                  <AppCtx.locale text="Submit" />
                </Button>
              </Grid>
            </Grid>
          </div>
        )}

  </form>
);

class OtpLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
    };
  }

  componentDidMount() {
    const { user } = this.props;
    user.verifyPhone().then(() => this.setState({ loader: false }));
  }

  // onClick={() => { next(user.forcePasswordReset ? 1 : 2);
  render() {
    const {
      user, classes, next,
    } = this.props;
    const { loader } = this.state;
    return (
      <section className={classes.sectionViewProfile}>
        <div className={classes.container}>
          <img src="/static/images/logo-white.png" alt="Agribuddy" className={classes.logo} />
          <Paper className={classes.paper}>
            <Form
              onSubmit={user.confirmPhone}
              render={OtpForm}
              classes={classes}
              initialValues={user}
              next={next}
              user={user}
              loader={loader}
            />
          </Paper>
        </div>
      </section>
    );
  }
}

export default withStyles(styles)(OtpLogin);
