import React, { Component } from 'react';
import queryString from 'qs';
import SetPassword from './setpassword';
import Welcome from './welcomeMessage';
import Profile from './profile';
import OtpLogin from './phoneVerifyOTP';

class WelcomeContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 0,
    };
    this.next = this.next.bind(this);
  }

  componentWillMount() {
    const search = queryString.parse(window.location.search, { ignoreQueryPrefix: true });
    if ('ns' in search) this.setState({ step: parseInt(search.ns, 10) });
  }

  next() {
    const { auth } = this.props;
    const { step } = this.state;
    let nextstep = 0;
    switch (step) {
    case 0:
      if (!auth.mobileVerified) {
        nextstep = 1;
      } else if (auth.forcePasswordReset) {
        nextstep = 2;
      } else if (auth.profile === 404) {
        nextstep = 3;
      } else nextstep = 100;
      break;
    case 1:
      if (auth.forcePasswordReset) {
        nextstep = 2;
      } else if (auth.profile === 404) {
        nextstep = 3;
      } else nextstep = 100;
      break;
    case 2:
      if (auth.profile === 404) {
        nextstep = 3;
      } else nextstep = 100;
      break;
    case 3:
      nextstep = 100;
      break;
    default:
    }
    this.setState({ step: nextstep });
  }

  render() {
    const { auth, locale } = this.props;
    const { step } = this.state;

    switch (step) {
    case 0:
      window.history.pushState({}, null, '/');
      return (
        <Welcome user={auth} locale={locale} next={this.next} />
      );
    case 1:
      return (
        <OtpLogin user={auth} next={this.next} />
      );
    case 2:
      return (
        <SetPassword user={auth} next={this.next} />
      );
    case 3:
      return (<Profile user={auth} locale={locale} next={this.next} />);
    default:

      return (
        <Welcome user={auth} />
      );
    }
  }
}

export default WelcomeContainer;
