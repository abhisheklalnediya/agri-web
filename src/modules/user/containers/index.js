import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Profile from './Profile';
import Dashboard from './Dashboard';
import ChangePassword from './ChangePassword';
import SetPassword from './SetPassword';
import Welcome from './Welcome';

function UserContainer({ match }) {
  return (
    <Switch>
      <Route
        path={`${match.url}/profile`}
        exact
        render={rprops => (
          <Profile {...rprops} />
        )}
      />
      <Route
        path="/user/changepassword"
        render={() => (<ChangePassword />
        )}
      />
      <Route
        path="/user/setpassword"
        render={() => (<SetPassword />
        )}
      />
      <Route
        path="/"
        render={() => (<Dashboard />
        )}
      />
    </Switch>

  );
}

export default {
  UserContainer, SetPassword, Welcome,
};
