
import React from 'react';
import AppCtx from 'AppContext';
import OtpLogin from './components/otp';

class OtpGenerator extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <AppCtx.Context.user.Consumer>
        { user => (
          <AppCtx.Context.locale.Consumer>
            {locale => (
              <OtpLogin auth={user} locale={locale} />
            )}
          </AppCtx.Context.locale.Consumer>
        )}
      </AppCtx.Context.user.Consumer>

    );
  }
}
export default OtpGenerator;
