import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChangePassword from '../containers/components/changepassword';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing Noprops', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MemoryRouter>
      <ChangePassword />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing  all props', () => {
  const div = document.createElement('div');
  const props = {
    classes: {},
    user: {
      roles: ['ADMIN'],
    },
    updateProfile: () => null,
  };

  ReactDOM.render(
    <MemoryRouter>
      <ChangePassword {...props} />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});


describe('Test Click Button component', () => {
  xit('Test click event', () => {
    const mockCallBack = jest.fn();
    const props = {
      classes: {},
      user: {
        roles: ['ADMIN'],
      },
      updateProfile: mockCallBack,
    };
    const button = shallow((<ChangePassword {...props} />));
    button.simulate('click');
    expect(mockCallBack.mock.calls.length).toEqual(1);
  });
});
