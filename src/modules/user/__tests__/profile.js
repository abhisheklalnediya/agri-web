import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Profile from '../containers/components/profile';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing  all props', () => {
  const div = document.createElement('div');
  const props = {
    classes: {},
    user: {
      roles: ['ADMIN'],
      updateProfile: () => null,
    },
    locale: { lang: 'en' },
  };
  ReactDOM.render(
    <MemoryRouter>
      <Profile {...props} />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

// describe('Test Click Button component', () => {
//   it('Test click event', () => {
//     const mockCallBack = jest.fn();
//     const props = {
//       classes: {},
//       user: {
//         roles: ['ADMIN'],
//         updateProfile: mockCallBack,
//       },
//       locale: { lang: 'en' },
//     };
//     const button = shallow((<Profile {...props} />));
//     button.simulate('click');
//     expect(mockCallBack.mock.calls.length).toEqual(1);
//   });
// });
