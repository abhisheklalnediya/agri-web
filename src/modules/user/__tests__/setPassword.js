import React from 'react';
import ReactDOM from 'react-dom';
import SetPassword from '../containers/components/setpassword';

xit('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SetPassword />, div);
  ReactDOM.unmountComponentAtNode(div);
});
