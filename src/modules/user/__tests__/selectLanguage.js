import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SelectLanguage from '../containers/components/selectLanguage';

Enzyme.configure({ adapter: new Adapter() });

xit('renders without crashing', () => {
  const div = document.createElement('div');
  const locale = {
    classes: {},
    lang: 'en',
    setLanguage: () => {},
    handleChange: () => null,
  };
  ReactDOM.render(
    <MemoryRouter>
      <SelectLanguage locale={locale} />
    </MemoryRouter>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

xit('should pass a selected value to the onChange handler', () => {
  const locale = {
    classes: {},
    lang: 'en',
  };

  const onChange = jest.fn();
  const wrapper = shallow(
    <SelectLanguage locale={locale} onChange={onChange} />,
  );

  expect(wrapper).toMatchSnapshot();

  wrapper.find('select').simulate('change', {
    target: { locale },
  });

  expect(onChange).toBeCalledWith(locale);
});
