import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import injectTapEventPlugin from 'react-tap-event-plugin';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'font-awesome/css/font-awesome.css';
import 'flexboxgrid/css/flexboxgrid.css';
import * as Sentry from '@sentry/browser';
import theme from './theme';
import App from './App';
import './fb';

if (window.location.hostname !== 'localhost') {
  Sentry.init({
    dsn: 'https://e5d5950de6f641be9d8f7b48397f7968@sentry.io/1313674',
  });
}

injectTapEventPlugin();
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <App />
  </MuiThemeProvider>,
  document.getElementById('root'),
);
