export default {
  disableKeyCompression: true,
  version: 9,
  title: 'Auth Schema',
  type: 'object',
  properties: {
    agriId: {
      type: 'number',
    },
    username: {
      type: 'string',
      primary: true,
    },
    supervisor: {
      type: 'string',
    },
    password: {
      type: 'string',
    },
    firstname: {
      type: 'string',
    },
    lastname: {
      type: 'string',
    },
    email: {
      type: 'string',
    },
    gender: {
      type: 'string',
    },
    roles: {
      type: 'array',
      item: {
        type: 'string',
      },
    },
    locked: {
      type: 'boolean',
    },
    forcePasswordReset: {
      type: 'boolean',
    },
    profilePic: {
      type: 'object',
    },
    createdOn: {
      type: 'string',
    },
    updatedOn: {
      type: 'string',
    },
    createdBy: {
      type: 'number',
    },
    updatedBy: {
      type: 'number',
    },
  },
};
