export default {
  disableKeyCompression: true,
  version: 2,
  title: 'Auth Schema',
  type: 'object',
  properties: {
    id: {
      type: 'string',
      primary: true,
    },
    farmerId: {
      type: 'number',
    },
    name: {
      type: 'string',
    },
    crops: {
      type: 'array',
      item: {
        type: 'string',
      },
    },
    cordinates: {
      type: 'array',
      item: {
        type: 'object',
      },
    },
    farmPhotos: {
      type: 'array',
      item: {
        type: 'object',
      },
    },
    area: {
      type: 'number',
    },
  },
};
