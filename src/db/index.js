import RxDB from 'rxdb';
import memorydb from 'pouchdb-adapter-memory';
import idbdb from 'pouchdb-adapter-idb';
import AuthSchema from './auth';
import AppSchema from './app';
import UsersSchema from './users';
import PincodeSchema from './pincodes';
import FarmSchema from './farms';

const clear = () => null;

const DB = new Promise((resolve) => {
  if (process.env.NODE_ENV === 'test') {
    RxDB.plugin(memorydb);
  } else {
    RxDB.plugin(idbdb);
  }
  RxDB.create({
    name: 'agridb',
    adapter: 'idb',
    multiInstance: false,
  }).then((db) => {
    Promise.all([

      db.collection({
        name: 'auth',
        schema: AuthSchema,
        migrationStrategies: {
          1: clear, 2: clear, 3: clear, 4: clear, 5: clear, 6: clear, 7: clear, 8: clear, 9: clear,
        },
      }),

      db.collection({
        name: 'app',
        schema: AppSchema,
        migrationStrategies: {
          1: clear, 2: clear, 3: clear, 4: clear,
        },
      }),

      db.collection({
        name: 'users',
        schema: UsersSchema,
        migrationStrategies: {
          1: clear, 2: clear, 3: clear, 4: clear, 5: clear, 6: clear, 7: clear, 8: clear, 9: clear,
        },
      }),

      db.collection({
        name: 'pincodes',
        schema: PincodeSchema,
        migrationStrategies: {
          1: clear,
        },
      }),

      db.collection({
        name: 'farms',
        schema: FarmSchema,
        migrationStrategies: {
          1: clear, 2: clear,
        },
      }),
    ]).then(() => {
      resolve(db);
    });
  }, e => console.log('rx error'));
});

export default DB;
