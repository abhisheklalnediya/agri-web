export default {
  disableKeyCompression: true,
  version: 9,
  title: 'Auth Schema',
  type: 'object',
  properties: {
    agriId: {
      type: 'number',
      final: true,
    },
    username: {
      type: 'string',
      primary: true,
    },
    firstname: {
      type: 'string',
    },
    lastname: {
      type: 'string',
    },
    gender: {
      type: 'string',
    },
    roles: {
      type: 'array',
      item: {
        type: 'string',
      },
    },
    access_token: {
      type: 'string',
    },
    forcePasswordReset: {
      type: 'boolean',
    },
    profilePic: {
      type: 'object',
    },
    createdOn: {
      type: 'string',
    },
    updatedOn: {
      type: 'string',
    },
    createdBy: {
      type: 'number',
    },
    updatedBy: {
      type: 'number',
    },
  },
};
