export default {
  disableKeyCompression: true,
  version: 4,
  title: 'App Schema',
  type: 'object',
  properties: {
    lang: {
      type: 'string',
      default: 'en',
    },
  },
};
