export default {
  disableKeyCompression: true,
  version: 1,
  title: 'Pincode Schema',
  type: 'object',
  properties: {
    'id': {
      'type': 'number',
      'required': true,
      'id': true,
    },
    'pincode': {
      'type': 'number',
      'required': true,
      'index': true,
    },
    'postoffice': {
      'type': 'string',
      'required': true,
    },
    'taluk': {
      'type': 'string',
      'required': true,
    },
    'district': {
      'type': 'string',
      'required': true,
      'index': true,
    },
    'state': {
      'type': 'string',
      'required': true,
    },
    'country': {
      'type': 'string',
      'default': 'India',
    },
  },
};
