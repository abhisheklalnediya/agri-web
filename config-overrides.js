const { rewireWorkboxInject, defaultInjectConfig } = require('react-app-rewire-workbox');
const path = require('path');
const wbConfg = require('./workbox-config');

module.exports = function override(config, env) {
  if (env === 'production') {
    console.log('Production build - Adding Workbox for PWAs');
    // Extend the default injection config with required swSrc
    const workboxConfig = {
      ...defaultInjectConfig,
      ...wbConfg,
      swSrc: path.join('public', 'sw.js'),
    };
    return rewireWorkboxInject(workboxConfig)(config, env);
  }

  return config;
};
